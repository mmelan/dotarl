#ifndef COMPONENTMAPPER_H
#define COMPONENTMAPPER_H

#include "Entity.h"
#include "EntityManager.h"
#include "World.h"

namespace artemis {
	
  /**
   * High performance component retrieval from entities. Use this wherever you
   * need to retrieve components from entities often and fast.
   */
  template<typename c>
  class ComponentMapper {
    
  private:
    
    Bag<Component*>* bag;
    
  public:
    
    ~ComponentMapper() {
        bag = NULL;
    }
    
    void init(World& world){
      ComponentType type = ComponentTypeManager::getTypeFor<c>();
      world.getEntityManager()->initBag(type);
      bag = world.getEntityManager()->getComponentsByType().get(type.getId());
    }
    
    /**
     *Returns the component mapped to the Entity.
     *If there is no such component associated with the entity
     *NULL is returned.
     */
    c * get(Entity & e) {
      if(bag != NULL && e.getId() < bag->getCapacity())
        return (c*)bag->get(e.getId());
      return NULL;
    }
    
  };
};
#endif // $(Guard token)
