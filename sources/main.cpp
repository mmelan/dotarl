#include "iostream"
#include "SDL.h"
#include "SDL_ttf.h"

#include "Artemis/Entity.h"
#include "Artemis/SystemManager.h"
#include "Artemis/World.h"

#include "core/AppearanceSystem.h"
#include "core/ControleSystem.h"
#include "core/EnergySystem.h"
#include "core/StatsSystem.h"
#include "core/EventInterpreter.h"
#include "core/Renderer.h"
#include "core/VirtualFileSystem.h"
#include "core/VisualMovementSystem.h"
#include "core/MapDiscoverySystem.h"
#include "core/ExperienceSystem.h"
#include "core/RenderConstantes.h"
#include "core/EntityCreation.h"
#include "core/dungeon.h"
#include "core/DungeonRenderer.h"
#include "core/FeedBackSystem.h"
#include "core/PlayerInterface.h"
#include "core/PositionSystem.h"
#include "core/TagList.h"
#include "core/Console.h"
#include "core/StartScreen.h"
#include "core/EndScreen.h"

const float FRAME_MIN_DURATION = 1.0f / 120.0f; // 60 FPS cap

int main(int argc, char *argv[])
{
    if (SDL_Init(SDL_INIT_EVERYTHING) == -1)
    {
        std::cout << SDL_GetError() << std::endl;
        return 1;
    }

    TTF_Init();

    VirtualFileSystem::InitFileSystem(argc, argv);

    bool quit_asked = false;
    while(!quit_asked)
    {
    //initialisation de la map
    DotaRL::Dungeon::Level level(130, 130);

    artemis::World world;
    artemis::SystemManager * sm = world.getSystemManager();

    //renderer
    DotaRL::Renderer::CreateInstance();
    SDL_assert(DotaRL::Renderer::HasInstance());
    DotaRL::Renderer::Instance().Initialise();

    DotaRL::Console::CreateInstance();
    DotaRL::AddLog("Hello and welcome fellow friend, Hope you'll have fun playing this!", DotaRL::LogSource::Info);
    DotaRL::AddLog("Have some hint:", DotaRL::LogSource::Info);
    DotaRL::AddLog("Use the NumPad to move!", DotaRL::LogSource::Info);
    DotaRL::AddLog("5 in the numpad skip your turn. Maintain to rest faster!", DotaRL::LogSource::Info);
    DotaRL::AddLog("Pressing f/space will allow you to target with ranged char.", DotaRL::LogSource::Info);
    DotaRL::AddLog("Pressing f/space again will launch ranged attack", DotaRL::LogSource::Info);
    DotaRL::AddLog("Pressing s will make you rest until full health or being hit", DotaRL::LogSource::Info);
    DotaRL::AddLog("others :  g=loot / alt + shortcut=drop items / c=open console / f=target", DotaRL::LogSource::Info);

    //systems
    auto energySystem = new DotaRL::GameObject::EnergySystem; sm->setSystem(energySystem);
    auto controlesys = new DotaRL::GameObject::ControleSystem(energySystem); sm->setSystem(controlesys);
    auto visualMovementsys = new DotaRL::GameObject::VisualMovementSystem; sm->setSystem(visualMovementsys);
    auto appearanceSys = new DotaRL::GameObject::AppearanceSystem; sm->setSystem(appearanceSys);
    auto mapDiscoverySys = new DotaRL::GameObject::MapDiscoverySystem; sm->setSystem(mapDiscoverySys);
    auto statSystem = new DotaRL::GameObject::StatsSystem(energySystem); sm->setSystem(statSystem);
    auto feedbackSys = new DotaRL::GameObject::FeedbackSystem(); sm->setSystem(feedbackSys);
    auto levelSystem = new DotaRL::GameObject::PositionSystem(&level); sm->setSystem(levelSystem);
    auto buffSystem = new DotaRL::GameObject::BuffSystem(energySystem); sm->setSystem(buffSystem);
    auto buffListSystem = new DotaRL::GameObject::BuffListSystem; sm->setSystem(buffListSystem);
    auto experienceSystem = new DotaRL::GameObject::ExperienceSystem(); sm->setSystem(experienceSystem);
    artemis::EntityManager * em = world.getEntityManager();
    sm->initializeAll();

    //key handler
    DotaRL::Event::Interpreter::CreateInstance();
    SDL_assert(DotaRL::Event::Interpreter::HasInstance());

    //Player interface
    DotaRL::PlayerInterfaceData playerInterfaceData;
    DotaRL::PlayerInterfaceRenderer playerInterfaceRenderer;
    DotaRL::PlayerInterfaceController playerInterfaceController;
    playerInterfaceController.Initialise(world);

    auto starting_point = DotaRL::Dungeon::MakeRandomLevel(level, DotaRL::Dungeon::StartingSide::Radiant, world);

    //dungeon renderer
    DotaRL::Dungeon::LevelRenderer dungeonRenderer;
    dungeonRenderer.ReinitTextures(level);
    level.mLevelRenderer = &dungeonRenderer;

    appearanceSys->SetDungeon(&level);
    feedbackSys->SetDungeon(&level);
    mapDiscoverySys->SetDungeonLevel(&level);

    //creations de trucs
    DotaRL::Renderer& mainRenderer = DotaRL::Renderer::Instance();
    DotaRL::Event::Interpreter& eventInterpreter = DotaRL::Event::Interpreter::Instance();

    auto player_start = level.FindEmptyPosition(starting_point, DotaRL::Dungeon::EntityLayer::Creature);
    SDL_assert(player_start.x > 0);
    DotaRL::Character selectedChar = DotaRL::StartScreen();
    if(selectedChar == DotaRL::Character::None)
    {
        quit_asked = true;
    }
    else
    {
        switch(selectedChar)
        {
        case DotaRL::Character::Ursa:
            DotaRL::CreateUrsa(world, player_start.x, player_start.y, &level);
            break;
        case DotaRL::Character::Venomancer:
            DotaRL::CreateVenomancer(world, player_start.x, player_start.y, &level);
            break;
        case DotaRL::Character::QoP:
            DotaRL::CreateQoP(world, player_start.x, player_start.y, &level);
            break;
        };

        DotaRL::MobTemplate roshanTemplate = DotaRL::GetRoshanTemplate();
        DotaRL::CreateMob(world, level.SizeX()/2, level.SizeY()/2, &level, roshanTemplate);

        //on va mettre un truc par terre pour tester
        std::vector<DotaRL::GameObject::ItemTemplate> itemList = DotaRL::GameObject::GetItemTable();
        DotaRL::CreateItemOnGround(world, player_start.x, player_start.y, &level, itemList[1]);
    }
    
    Uint64 last_tick = SDL_GetPerformanceCounter();
    bool gameEnded = false;
    bool victory = false;
    while(!quit_asked && !gameEnded)
    {
        Uint64 current_tick = SDL_GetPerformanceCounter();
        float remaining_time;
        while ((remaining_time = static_cast<float>(current_tick - last_tick) / SDL_GetPerformanceFrequency() - FRAME_MIN_DURATION) < 0)
        {
            SDL_Delay(static_cast<uint32_t>(-remaining_time*1000.0f));
            current_tick = SDL_GetPerformanceCounter();
        }

        {
            world.loopStart();
            world.setDelta(static_cast<float>(current_tick - last_tick) / SDL_GetPerformanceFrequency());
            last_tick = current_tick;

            eventInterpreter.Process();
            
            artemis::Entity* entityActing = energySystem->EntityActing();
            if(entityActing)
            {
                bool reset = controlesys->ProcessEntity(*entityActing);
                if(reset)
                    energySystem->ResetEntityActing();
            }

            level.loopStart();
            mapDiscoverySys->process();
            visualMovementsys->process();

            mainRenderer.LoopStart(world, level);
            if(mainRenderer.ResolutionChanged())
                dungeonRenderer.ReinitTextures(level);
            dungeonRenderer.Render(level);

            appearanceSys->ResetTextures() = mainRenderer.ResolutionChanged();
            for(int i = 0; i < DotaRL::Layer::Length; ++i)
            {
                appearanceSys->SetLayer((DotaRL::Layer::Type)i);
                appearanceSys->process();
            }
            feedbackSys->process();
            playerInterfaceController.Process(playerInterfaceData, world);
            playerInterfaceRenderer.Process(playerInterfaceData, energySystem->CurrentDate());
            controlesys->DoRenderTargeting(&playerInterfaceRenderer);
            DotaRL::Console::Instance().Process();
            mainRenderer.Process();
        }
        
        while (!quit_asked && !gameEnded && !visualMovementsys->MovementInProgress() && energySystem->EntityActing() == nullptr)
        {
            world.loopStart();
            world.setDelta(0.f);
            eventInterpreter.Process();
            buffListSystem->process();
            energySystem->process();
            buffSystem->process();
            experienceSystem->process();
            statSystem->process();
            levelSystem->process();
            auto tagManager = world.getTagManager();
            if (!tagManager->isSubscribed(TagList::PLayer))
            {
                gameEnded = true;
            }
            else if(!tagManager->isSubscribed(TagList::Rushan))
            {
                gameEnded = true;
                victory = true;
            }
            else
            {
                artemis::Entity* entityActing = energySystem->EntityActing();
                auto playerId = tagManager->getEntity(TagList::PLayer).getId();
                if (entityActing && entityActing->getId() != playerId)
                {
                    bool reset = controlesys->ProcessEntity(*entityActing);
                    if(reset)
                        energySystem->ResetEntityActing();
                    SDL_assert(reset); // pas de boucle de rendu entre deux actions de mobs sauf s'ils font un mouvement bien sur
                }
            }
        }

        quit_asked |= eventInterpreter.QuitAsked();
    }
    if(!quit_asked)
        quit_asked |= DotaRL::EndScreen(victory);
    DotaRL::Renderer::DestroyInstance();
    DotaRL::Console::DestroyInstance();
    DotaRL::Event::Interpreter::DestroyInstance();
    }
    SDL_Quit();
    return EXIT_SUCCESS;
}
