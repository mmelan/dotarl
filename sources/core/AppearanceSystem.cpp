#include "AppearanceSystem.h"
#include "SDL.h"
#include "RenderConstantes.h"
#include "Renderer.h"

namespace DotaRL { namespace GameObject {
	AppearanceSystem::AppearanceSystem()
        : mDungeonLvl(nullptr)
	{
        addComponentType<AppearanceComponent>();
        addComponentType<PositionComponent>();
	}

	void AppearanceSystem::initialize()
	{
        mAppearanceMapper.init(*world);
        mPositionMapper.init(*world);
	}

    void AppearanceSystem::SetLayer(Layer::Type parLayer)
    {
        mLayer = parLayer;
    }

	void AppearanceSystem::processEntity(artemis::Entity &e)
	{
        AppearanceComponent* appearanceComponent = mAppearanceMapper.get(e);
        if( appearanceComponent->mAppearance_Ground.mSurface && 
            appearanceComponent->mAppearance_Ground.mLayer == mLayer)
        {
            PositionComponent* positionComponent = mPositionMapper.get(e);
            if(mDungeonLvl && mDungeonLvl->GetVisibility(positionComponent->mGridPosX, positionComponent->mGridPosY))
            {
                SDL_Renderer* sdlRenderer = DotaRL::Renderer::Instance().SDLRenderer();
                SDL_assert(sdlRenderer);
                if(mResetTextures || !appearanceComponent->mAppearance_Ground.mTexture)
                {
                    if(appearanceComponent->mAppearance_Ground.mTexture)
                        SDL_DestroyTexture(appearanceComponent->mAppearance_Ground.mTexture);
                    appearanceComponent->mAppearance_Ground.mTexture = SDL_CreateTextureFromSurface(sdlRenderer, appearanceComponent->mAppearance_Ground.mSurface);
                }
                SDL_assert(appearanceComponent->mAppearance_Ground.mTexture);
	            SDL_Rect position;
                position.x = (int)appearanceComponent->mAppearance_Ground.mPosX + (Dungeon::GetTileSizeX() - appearanceComponent->mAppearance_Ground.mSurface->w) / 2; //centr� sur la case
	            position.y = (int)appearanceComponent->mAppearance_Ground.mPosY + (Dungeon::GetTileSizeY() - appearanceComponent->mAppearance_Ground.mSurface->h) / 2 - (appearanceComponent->mAppearance_Ground.mSurface->h - Dungeon::GetTileSizeY())/2;
                position.w = appearanceComponent->mAppearance_Ground.mSurface->w;
	            position.h = appearanceComponent->mAppearance_Ground.mSurface->h;
                DotaRL::Renderer::Instance().Render(appearanceComponent->mAppearance_Ground.mTexture, nullptr, &position, true);
            }
        }
	}
}}