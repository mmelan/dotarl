#pragma once

#include <cstdint>

#include "Artemis/EntityProcessingSystem.h"
#include "Artemis/ComponentMapper.h"

#include "EnergyComponent.h"

namespace DotaRL
{
    namespace GameObject
    {
        class EnergySystem : public artemis::EntityProcessingSystem
        {
        public:
            EnergySystem();

            void initialize() override;
            void processEntity(artemis::Entity &e) override;

            void begin() override;
            void end() override;

            artemis::Entity* EntityActing() const;
            void ResetEntityActing();

            int64_t CurrentDate() const;
        private:
            artemis::ComponentMapper<EnergyComponent> mEnergyMapper;
            
            artemis::Entity* mEntityActing;
            int64_t mDateNextAction;
            int64_t mCurrentDate;
        };
    }
}
