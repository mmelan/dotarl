#include "Buffs.h"

#include "SDL_assert.h"
#include "SDL_log.h"
#include "StatsComponent.h"
#include "RandomGenerator.h"
#include "EnergySystem.h"
#include "DamageRules.h"

#include "Artemis/World.h"

namespace DotaRL { namespace GameObject
{
    AttackRollSummary::AttackRollSummary()
    {
        CritBonus = 0.f;
    }

    Buff::Buff(BuffId id) :
        mId(id),
        Creator(-1),
        Carrier(-1),
        EntityId(-1),
        Duration(-1),
        MaxStacks(-1),
        Charges(-1),
        PeriodicEffect(-1),
        StackMode(BuffStackMode::Independent)
    {
    }

    void Buff::ApplyGreenCombatStatBonuses(CombatStatsComponent* combatComponent, const HealthManaComponent* healthComponent) {}
    void Buff::ApplyBaseHealthBonuses(HealthManaComponent* healthComponent) {}
    void Buff::ApplyFinalHealthBonuses(HealthManaComponent* healthComponent) {}
    void Buff::AttackLaunched(std::vector<OrbEffectId>& orbs, AttackRollSummary& attackRollSummary, artemis::Entity& target) {}
    void Buff::AttackLanded(artemis::World& world, artemis::Entity& target, int64_t currentDate) {}
    void Buff::ApplyBuffBonusDamage(int* damage, BuffListComponent* targetBuffs) {}
    void Buff::OnPeriodicEffect(artemis::World& world) {}
    void Buff::ApplyStatBonuses(CoreStatsComponent* coreStats) {}

    Buff* MakeBuffFromId(BuffId id, int level);

    bool EntityHasBuff(artemis::Entity& e, BuffId id)
    {
        auto buffList = static_cast<BuffListComponent*>(e.getComponent<BuffListComponent>());
        if (buffList)
        {
            for (auto b : *buffList)
                if (b->GetId() == id)
                    return true;
        }
        return false;
    }

    int AddBuffToEntity(artemis::World& world, artemis::Entity& carrier, artemis::Entity& creator, BuffId id, int level, int64_t currentDate)
    {
        auto& r = world.createEntity();
        auto buff = MakeBuffFromId(id, level);
        if (!buff)
            return -1;
        auto buffList = static_cast<BuffListComponent*>(carrier.getComponent<BuffListComponent>());
        int64_t dateEndBuff = buff->Duration>0?currentDate+buff->Duration:-1;
        int nbStacks = 0;
        for (auto b : *buffList)
            if (b->GetId() == id)
                nbStacks++;

        if (buff->MaxStacks > 0 && buff->MaxStacks < nbStacks)
        {
            auto& buffEntity = world.getEntity(buffList->Buffs[0]->EntityId);
            buffEntity.remove();
            SDL_assert(nbStacks-1 == buff->MaxStacks);
        }

        if (buff->StackMode == BuffStackMode::Refresh)
        {
            SDL_assert(dateEndBuff != -1); // pas de sens de refresh la duree d'un buff infini
            for (auto b : *buffList)
                if (b->GetId() == id)
                {
                    auto& buffEntity = world.getEntity(b->EntityId);
                    auto buffComponent = static_cast<BuffComponent*>(buffEntity.getComponent<BuffComponent>());
                    buffComponent->DateExpire = dateEndBuff;
                }
        }

        buff->Carrier = carrier.getId();
        buff->Creator = creator.getId();
        buff->EntityId = r.getId();
        SDL_assert(buffList);
        if (buffList)
        {
            buffList->Buffs.push_back(buff);
        }
        auto buffComponent = new BuffComponent(buff, -1);
        if (buff->PeriodicEffect > 0)
        {
            buffComponent->DateNextWakeUp = currentDate + buff->PeriodicEffect;
        }

        r.addComponent(buffComponent);
        r.refresh();
        return buff->EntityId;
    }

    BuffComponent::BuffComponent(Buff* buff, int64_t dateExpire) :
        BuffInstance(buff),
        DateExpire(dateExpire),
        DateNextWakeUp(-1),
        ChargesRemaining(buff->Charges)
    {
    }

    BuffSystem::BuffSystem(const EnergySystem* energySystem) :
        mEnergySystem(energySystem)
    {
        addComponentType<BuffComponent>();
    }

    void BuffSystem::initialize()
    {
        mBuffMapper.init(*world);
        mBuffListMapper.init(*world);
    }

    void BuffSystem::processEntity(artemis::Entity& e)
    {
        auto buff = mBuffMapper.get(e);
        if (buff->ChargesRemaining == 0)
            e.remove();
        else if (buff->DateExpire >= 0 && buff->DateExpire > mEnergySystem->CurrentDate())
            e.remove();
        else if (buff->DateNextWakeUp < mEnergySystem->CurrentDate())
        {
            buff->DateNextWakeUp += buff->BuffInstance->PeriodicEffect;
            buff->BuffInstance->OnPeriodicEffect(*world);
        }
    }

    void BuffSystem::removed(artemis::Entity& e)
    {
        auto buff = mBuffMapper.get(e);
        SDL_assert(buff); // do not remove the component from the entity

        auto carrier = &world->getEntity(buff->BuffInstance->Carrier);
        if (carrier)
        {
            auto bufflist = mBuffListMapper.get(*carrier);
            auto buffiter = std::find(bufflist->Buffs.begin(), bufflist->Buffs.end(), buff->BuffInstance);
            SDL_assert(buffiter != bufflist->Buffs.end());
            bufflist->Buffs.erase(buffiter);
        }
        delete buff->BuffInstance;
    }

    BuffListSystem::BuffListSystem()
    {
        addComponentType<BuffListComponent>();
    }

    void BuffListSystem::initialize()
    {
        mBuffListMapper.init(*world);
    }

    void BuffListSystem::processEntities(artemis::ImmutableBag<artemis::Entity*> & bag)
    {
    }

    bool BuffListSystem::checkProcessing()
    {
        return false;
    }

    void BuffListSystem::removed(artemis::Entity& e)
    {
        auto buffList = mBuffListMapper.get(e);
        for (auto b : *buffList)
        {
            auto& buffEntity = world->getEntity(b->EntityId);
            if (&buffEntity)
            {
                buffEntity.remove();
            }
        }
    }

    class FurySwipesPassive : public Buff
    {
    public:
        FurySwipesPassive(int level) :
            Buff(BuffId::FurySwipesPassive),
            mLevel(level)
        {
        }

        void AttackLaunched(std::vector<OrbEffectId>& orbs, AttackRollSummary& attackRollSummary, artemis::Entity& target) override
        {
            if (orbs.empty())
                orbs.push_back(OrbEffectId::FurySwipes);
        }

        void ApplyBuffBonusDamage(int* damage, BuffListComponent* targetBuffs) override
        {
            int target_stacks = 0;
            for (auto b : *targetBuffs)
                if (b->GetId() == BuffId::FurySwipesDebuff)
                    target_stacks++;

            *damage += target_stacks*(10+5*mLevel);
        }
    private:
        int mLevel;
    };

    class FurySwipesDebuff : public Buff
    {
    public:
        FurySwipesDebuff() :
            Buff(BuffId::FurySwipesDebuff)
        {
            Duration = 15000;
            StackMode = BuffStackMode::Refresh;
        }
    };

    class EarthshockDebuff : public Buff
    {
    public:
        EarthshockDebuff(int level) :
            Buff(BuffId::EarthshockDebuff),
            mLevel(level)
        {
            MaxStacks = 1;
            Duration = 4000;
        }
        void ApplyGreenCombatStatBonuses(CombatStatsComponent* combatComponent, const HealthManaComponent* healthComponent) override
        {
            combatComponent->MoveSpeed -= (0.15f + 0.1f*mLevel) * combatComponent->BaseMoveSpeed;
        }
    private:
        int mLevel;
    };

    class OverpowerBuff : public Buff
    {
    public:
        OverpowerBuff(int level) :
            Buff(BuffId::OverpowerBuff),
            mLevel(level)
        {
            MaxStacks = 1;
            Duration = 15000;
            Charges = 2+level;
        }

        void AttackLaunched(std::vector<OrbEffectId>& orbs, AttackRollSummary& attackRollSummary, artemis::Entity& target) override
        {
            Charges--;
        }

        void ApplyGreenCombatStatBonuses(CombatStatsComponent* combatComponent, const HealthManaComponent* healthComponent) override
        {
            if (Charges)
                combatComponent->IAS = 10000;
        }
    private:
        int mLevel;
    };

    class EnrageBuff : public Buff
    {
    public:
        EnrageBuff(int level) :
            Buff(BuffId::Enrage)
        {
            MaxStacks = 1;
            Duration = 15000;
            mBonus = 0.04f + 0.01f*level;
        }

        void ApplyGreenCombatStatBonuses(CombatStatsComponent* combatComponent, const HealthManaComponent* healthComponent) override
        {
            int bonusDamage = static_cast<int>(healthComponent->MaxHealth * mBonus);
            combatComponent->MinDamage += bonusDamage;
            combatComponent->MaxDamage += bonusDamage;
        }
    private:
        float mBonus;
    };

    class VenomousGaleDebuff : public Buff
    {
    public:
        VenomousGaleDebuff(int level) :
            Buff(BuffId::VenomousGale),
            mLevel(level)
        {
            MaxStacks = 1;
            Duration = 15000;
            if (mLevel > 1)
                PeriodicEffect = 3000;
        }
        void OnPeriodicEffect(artemis::World& world) override
        {
            static float damage[] = {0, 30, 60, 90};
            auto& attacker = world.getEntity(Creator);
            auto& target = world.getEntity(Carrier);
            InflictDamage(world, attacker, target, damage[mLevel-1], DamageType::Magic);
        }
        void ApplyGreenCombatStatBonuses(CombatStatsComponent* combatComponent, const HealthManaComponent* healthComponent) override
        {
            combatComponent->MoveSpeed -= 0.5f * combatComponent->BaseMoveSpeed;
        }
    private:
        int mLevel;
    };

    class PoisonStingPassive : public Buff
    {
    public:
        PoisonStingPassive(int level) :
            Buff(BuffId::PoisonStingPassive),
            mLevel(level)
        {
            MaxStacks = 1;
        }
        void AttackLanded(artemis::World& world, artemis::Entity& target, int64_t currentDate) override
        {
            auto& creator = world.getEntity(Creator);
            AddBuffToEntity(world, target, creator, BuffId::PoisonStingDebuff, mLevel, currentDate);
        }
    private:
        int mLevel;
    };

    class PoisonStingDebuff : public Buff
    {
    public:
        PoisonStingDebuff(int level) :
            Buff(BuffId::PoisonStingDebuff),
            mLevel(level)
        {
            MaxStacks = 1;
            Duration = 3000+3000*level;
            PeriodicEffect = 1000;
        }
        void OnPeriodicEffect(artemis::World& world) override
        {
            auto& attacker = world.getEntity(Creator);
            auto& target = world.getEntity(Carrier);
            InflictDamage(world, attacker, target, mLevel*5.f, DamageType::Magic);
        }
        void ApplyGreenCombatStatBonuses(CombatStatsComponent* combatComponent, const HealthManaComponent* healthComponent) override
        {
            combatComponent->MoveSpeed -= (0.1f + 0.01f*mLevel) * combatComponent->BaseMoveSpeed;
        }
    private:
        int mLevel;
    };

    class PoisonNovaDebuff : public Buff
    {
    public:
        PoisonNovaDebuff(int level) :
            Buff(BuffId::PoisonNova),
            mLevel(level)
        {
            MaxStacks = 1;
            static int durations[] = {12, 14, 15, 16};
            Duration = durations[level-1]*1000;
            PeriodicEffect = 1000;
        }
        void OnPeriodicEffect(artemis::World& world) override
        {
            static float damage[] = {36, 58, 81, 108};
            auto& attacker = world.getEntity(Creator);
            auto& target = world.getEntity(Carrier);
            InflictDamage(world, attacker, target, damage[mLevel-1], DamageType::Magic);
        }
    private:
        int mLevel;
    };

    class ShadowStrikeDebuff : public Buff
    {
    public:
        ShadowStrikeDebuff(int level) :
            Buff(BuffId::ShadowStrike),
            mLevel(level)
        {
            MaxStacks = 1;
            Duration = 15100;
            PeriodicEffect = 3000;
        }
        void OnPeriodicEffect(artemis::World& world) override
        {
            auto& attacker = world.getEntity(Creator);
            auto& target = world.getEntity(Carrier);
            InflictDamage(world, attacker, target, 20.f+10.f*mLevel, DamageType::Magic);
        }
        void ApplyGreenCombatStatBonuses(CombatStatsComponent* combatComponent, const HealthManaComponent* healthComponent) override
        {
            combatComponent->MoveSpeed -= (0.1f + 0.1f*mLevel) * combatComponent->BaseMoveSpeed;
        }
    private:
        int mLevel;
    };

    class IAmRoshanBuff : public Buff
    {
    public:
        IAmRoshanBuff() :
            Buff(BuffId::IAmRoshan)
        {
            PeriodicEffect = 1000000;
        }
        void OnPeriodicEffect(artemis::World& world) override
        {
            auto& attacker = world.getEntity(Creator);
            AddBuffToEntity(world, attacker, attacker, BuffId::IGrowStronger, 0, 0);
        }
    };

    class IGrowStrongerBuff : public Buff
    {
    public:
        IGrowStrongerBuff() :
            Buff(BuffId::IGrowStronger)
        {
        }
        void ApplyGreenCombatStatBonuses(CombatStatsComponent* combatComponent, const HealthManaComponent* healthComponent) override
        {
            combatComponent->MinDamage += 10.f;
            combatComponent->MaxDamage += 10.f;
            combatComponent->Armor += 1.f;
        }
        void ApplyBaseHealthBonuses(HealthManaComponent* healthComponent) override
        {
            healthComponent->MaxHealth += 100.f;
        }
    };

    class RingOfHealthBuff : public Buff
    {
    public:
        RingOfHealthBuff() :
            Buff(BuffId::RingOfHealth)
        {
        }

        void ApplyBaseHealthBonuses(HealthManaComponent* healthComponent) override
        {
            healthComponent->HealthRegen += 5.f;
        }
    };

    class VoidStoneBuff : public Buff
    {
    public:
        VoidStoneBuff() :
            Buff(BuffId::VoidStone)
        {
        }

        void ApplyBaseHealthBonuses(HealthManaComponent* healthComponent) override
        {
            healthComponent->ManaRegen += healthComponent->BaseManaRegen;
        }
    };

    class PerseveranceBuff : public Buff
    {
    public:
        PerseveranceBuff() :
            Buff(BuffId::Perseverance)
        {
        }

        void ApplyBaseHealthBonuses(HealthManaComponent* healthComponent) override
        {
            healthComponent->HealthRegen += 5.f;
            healthComponent->ManaRegen += healthComponent->BaseManaRegen * 1.25f;
        }
        void ApplyGreenCombatStatBonuses(CombatStatsComponent* combatComponent, const HealthManaComponent* healthComponent) override
        {
            combatComponent->MinDamage += 10.f;
            combatComponent->MaxDamage += 10.f;
        }
    };

    class DaedalusBuff : public Buff
    {
    public:
        DaedalusBuff() :
            Buff(BuffId::Daedalus)
        {
        }
        void AttackLaunched(std::vector<OrbEffectId>& orbs, AttackRollSummary& attackRollSummary, artemis::Entity& target) override
        {
            if (Random::d100() < 25)
                attackRollSummary.CritBonus = std::max(attackRollSummary.CritBonus, 1.4f);
        }
        void ApplyGreenCombatStatBonuses(CombatStatsComponent* combatComponent, const HealthManaComponent* healthComponent) override
        {
            combatComponent->MinDamage += 81.f;
            combatComponent->MaxDamage += 81.f;
        }
    };

    class DesolatorBuff : public Buff
    {
    public:
        DesolatorBuff() :
            Buff(BuffId::Desolator)
        {
        }
        void AttackLaunched(std::vector<OrbEffectId>& orbs, AttackRollSummary& attackRollSummary, artemis::Entity& target) override
        {
            if (orbs.empty())
                orbs.push_back(OrbEffectId::Desolator);
        }
        void ApplyGreenCombatStatBonuses(CombatStatsComponent* combatComponent, const HealthManaComponent* healthComponent) override
        {
            combatComponent->MinDamage += 60.f;
            combatComponent->MaxDamage += 60.f;
        }
    };

    class DesolatorDebuff : public Buff
    {
    public:
        DesolatorDebuff() :
            Buff(BuffId::DesolatorDebuff)
        {
            Duration = 15000;
            StackMode = BuffStackMode::Refresh;
            MaxStacks = 1;
        }
        void ApplyGreenCombatStatBonuses(CombatStatsComponent* combatComponent, const HealthManaComponent* healthComponent) override
        {
            combatComponent->Armor -= 7.f;
        }
    };

    class SangeBuff : public Buff
    {
    public:
        SangeBuff() :
            Buff(BuffId::Sange)
        {
        }
        void AttackLaunched(std::vector<OrbEffectId>& orbs, AttackRollSummary& attackRollSummary, artemis::Entity& target) override
        {
            if (Random::d100() < 15)
                attackRollSummary.BuffToPlace.push_back(BuffId::LesserMaim);
        }
        void ApplyStatBonuses(CoreStatsComponent* coreStats)
        {
            coreStats->Str += 16;
        }
        void ApplyGreenCombatStatBonuses(CombatStatsComponent* combatComponent, const HealthManaComponent* healthComponent) override
        {
            combatComponent->MinDamage += 10.f;
            combatComponent->MaxDamage += 10.f;
        }
    };

    class YashaBuff : public Buff
    {
    public:
        YashaBuff() :
            Buff(BuffId::Yasha)
        {
        }
        void ApplyStatBonuses(CoreStatsComponent* coreStats)
        {
            coreStats->Agi += 16;
        }
        void ApplyGreenCombatStatBonuses(CombatStatsComponent* combatComponent, const HealthManaComponent* healthComponent) override
        {
            combatComponent->IAS += 10.f;
            combatComponent->MoveSpeed += combatComponent->BaseMoveSpeed * .1f;
        }
    };

    class SangeYashaBuff : public Buff
    {
    public:
        SangeYashaBuff() :
            Buff(BuffId::SangeYasha)
        {
        }
        void AttackLaunched(std::vector<OrbEffectId>& orbs, AttackRollSummary& attackRollSummary, artemis::Entity& target) override
        {
            if (Random::d100() < 16)
                attackRollSummary.BuffToPlace.push_back(BuffId::GreaterMaim);
        }
        void ApplyStatBonuses(CoreStatsComponent* coreStats)
        {
            coreStats->Str += 16;
            coreStats->Agi += 16;
        }
        void ApplyGreenCombatStatBonuses(CombatStatsComponent* combatComponent, const HealthManaComponent* healthComponent) override
        {
            combatComponent->MinDamage += 16.f;
            combatComponent->MaxDamage += 16.f;
            combatComponent->IAS += 16.f;
            combatComponent->MoveSpeed += combatComponent->BaseMoveSpeed * .16f;
        }
    };

    class LesserMaimDebuff : public Buff
    {
    public:
        LesserMaimDebuff() :
            Buff(BuffId::LesserMaim)
        {
            Duration = 5000;
        }
        void ApplyGreenCombatStatBonuses(CombatStatsComponent* combatComponent, const HealthManaComponent* healthComponent) override
        {
            combatComponent->IAS -= 20.f;
            combatComponent->MoveSpeed -= combatComponent->BaseMoveSpeed * .20f;
        }
    };

    class GreaterMaimDebuff : public Buff
    {
    public:
        GreaterMaimDebuff() :
            Buff(BuffId::GreaterMaim)
        {
            Duration = 5000;
        }
        void ApplyGreenCombatStatBonuses(CombatStatsComponent* combatComponent, const HealthManaComponent* healthComponent) override
        {
            combatComponent->IAS -= 32.f;
            combatComponent->MoveSpeed -= combatComponent->BaseMoveSpeed * .32f;
        }
    };

    class OrchidMalevolenceBuff : public Buff
    {
    public:
        OrchidMalevolenceBuff() :
            Buff(BuffId::OrchidMalevolence)
        {
        }
        void ApplyStatBonuses(CoreStatsComponent* coreStats)
        {
            coreStats->Int += 25;
        }
        void ApplyBaseHealthBonuses(HealthManaComponent* healthComponent) override
        {
            healthComponent->ManaRegen += healthComponent->BaseManaRegen * 1.5f;
        }
        void ApplyGreenCombatStatBonuses(CombatStatsComponent* combatComponent, const HealthManaComponent* healthComponent) override
        {
            combatComponent->MinDamage += 30.f;
            combatComponent->MaxDamage += 30.f;
            combatComponent->IAS += 30.f;
        }
    };

    Buff* MakeBuffFromId(BuffId id, int level)
    {
        switch (id)
        {
        case BuffId::FurySwipesPassive:
            return new FurySwipesPassive(level);
        case BuffId::FurySwipesDebuff:
            return new FurySwipesDebuff;
        case BuffId::Enrage:
            return new EnrageBuff(level);
        case BuffId::EarthshockDebuff:
            return new EarthshockDebuff(level);
        case BuffId::OverpowerBuff:
            return new OverpowerBuff(level);

        case BuffId::VenomousGale:
            return new VenomousGaleDebuff(level);
        case BuffId::PoisonStingPassive:
            return new PoisonStingPassive(level);
        case BuffId::PoisonStingDebuff:
            return new PoisonStingDebuff(level);
        case BuffId::PoisonNova:
            return new PoisonNovaDebuff(level);

        case BuffId::ShadowStrike:
            return new ShadowStrikeDebuff(level);

        case BuffId::IAmRoshan:
            return new IAmRoshanBuff;
        case BuffId::IGrowStronger:
            return new IGrowStrongerBuff;

        case BuffId::RingOfHealth:
            return new RingOfHealthBuff;
        case BuffId::VoidStone:
            return new VoidStoneBuff;
        case BuffId::Perseverance:
            return new PerseveranceBuff;
        case BuffId::Daedalus:
            return new DaedalusBuff;
        case BuffId::Desolator:
            return new DesolatorBuff;
        case BuffId::DesolatorDebuff:
            return new DesolatorDebuff;
        case BuffId::Sange:
            return new SangeBuff;
        case BuffId::Yasha:
            return new YashaBuff;
        case BuffId::SangeYasha:
            return new SangeYashaBuff;
        case BuffId::GreaterMaim:
            return new GreaterMaimDebuff;
        case BuffId::LesserMaim:
            return new LesserMaimDebuff;
        case BuffId::OrchidMalevolence:
            return new OrchidMalevolenceBuff;
        default:
            return nullptr;
        }
    }

    BuffId GetOrbDebuff(OrbEffectId orbId)
    {
        switch (orbId)
        {
        default:
            return BuffId::Undefined;
        case OrbEffectId::FurySwipes:
            return BuffId::FurySwipesDebuff;
        case OrbEffectId::Desolator:
            return BuffId::DesolatorDebuff;
        }
    }
}
}
