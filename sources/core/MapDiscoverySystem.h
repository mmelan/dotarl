#pragma once

#include "Artemis/EntityProcessingSystem.h"
#include "Artemis/ComponentMapper.h"
#include "MapDiscoveryComponent.h"
#include "PositionComponent.h"
#include "VisibilityComponent.h"
#include "dungeon.h"
#include "fov.h"

namespace DotaRL { namespace GameObject
{
    class MapDiscoverySystem : public artemis::EntityProcessingSystem
    {

    public:
        MapDiscoverySystem();
        ~MapDiscoverySystem() override;

        virtual void initialize();
        virtual void processEntity(artemis::Entity &e);

        void SetDungeonLevel(Dungeon::Level* dungeonLevel) { mDungeonLvl = dungeonLevel; }

    private:
        artemis::ComponentMapper<MapDiscoveryComponent> mMapDiscoveryMapper;
        artemis::ComponentMapper<PositionComponent> mPositionMapper;
        artemis::ComponentMapper<VisibilityComponent> mVisibilityMapper;
        Dungeon::Level* mDungeonLvl;

        fov_settings_type mFovSettings;
    };
}
}
