#pragma once

#include <string>
#include <vector>
#include "AIBehaviour.h"
#include "FactionComponent.h"
#include "SkillDescription.h"

namespace DotaRL{
    template<class T>
    struct range{
        range() {}
        range(T a, T b) : min(a), max(b) {}
        range(T a) : min(a), max(a) {}
        T min;
        T max;
    };
    typedef range<float> range_f;
    typedef range<int> range_i;
    struct MobTemplate
    {
        range_f mBaseHealthRegen;
        range_f mBaseManaRegen;
        range_f mMaxHealth;
        range_f mMaxMana;
        range_f mMoveSpeed;
        range_f mMinDamage;
        range_f mDamageRange;
        range_f mArmor;
        range_f mXpGain;
        range_i mNumberToSpawn;
        int     mRange;
        GameObject::Faction mFaction;
        int mVisibility;
        int mBaseLevel;
        std::string mName;
        std::string mAppearancePath;
        std::vector<AIActions> mPossibleActions;
        std::vector<GameObject::SkillDescription> mSkillSet;
        bool mIsHero;
        bool operator==(const MobTemplate& other)const 
        {
            return mName == other.mName;
        }
    };

    std::vector<MobTemplate> GetMobTable();
    MobTemplate GetRoshanTemplate();
}
