#include "VisualMovementSystem.h"
#include "RenderConstantes.h"
#include "Artemis/Entity.h"

#define sqr(A) (A)*(A)
namespace DotaRL
{
    namespace GameObject
    {
        const float MaxVisualSpeed = 8.f;
        VisualMovementSystem::VisualMovementSystem() :
            mMovementInProgress(false)
        {
            addComponentType<PositionComponent>();
            addComponentType<AppearanceComponent>();
        }

        void VisualMovementSystem::initialize()
        {
            mPositionMapper.init(*world);
            mAppearanceMapper.init(*world);
        }

        void VisualMovementSystem::processEntity(artemis::Entity &e)
        {
            PositionComponent* positionComponent = mPositionMapper.get(e);
            AppearanceComponent* appearanceComponent = mAppearanceMapper.get(e);
            float destinationX = (float)positionComponent->mGridPosX * Dungeon::GetTileSizeX();
            float destinationY = (float)positionComponent->mGridPosY * Dungeon::GetTileSizeY();
            if(positionComponent->AttackedTile.x >= 0)
            {
                mMovementInProgress = true;
                float atkDestinationX = (positionComponent->AttackedTile.x + (float)positionComponent->mGridPosX) * ((float)Dungeon::GetTileSizeX()) / 2.f;
                float atkDestinationY = (positionComponent->AttackedTile.y + (float)positionComponent->mGridPosY) * ((float)Dungeon::GetTileSizeY()) / 2.f;
                
                if( (std::abs(appearanceComponent->mAppearance_Ground.mPosX-atkDestinationX) < 0.001f) &&
                    (std::abs(appearanceComponent->mAppearance_Ground.mPosY-atkDestinationY) < 0.001f) )
                {
                    positionComponent->AttackedTile.x = -1;
                    positionComponent->AttackedTile.y = -1;
                }
                else
                {
                    destinationX = atkDestinationX;
                    destinationY = atkDestinationY;
                }
            }

            float distanceX = destinationX - appearanceComponent->mAppearance_Ground.mPosX;
            float distanceY = destinationY - appearanceComponent->mAppearance_Ground.mPosY;

            float distanceToDest = sqrt(sqr(distanceX) + sqr(distanceY));
            if (distanceToDest > 0.001)
            {
                float speedX = distanceX;
                float speedY = distanceY;
                if(distanceToDest > MaxVisualSpeed && !positionComponent->mNextMoveIsTeleport)
                {
                    speedX *= MaxVisualSpeed / distanceToDest;
                    speedY *= MaxVisualSpeed / distanceToDest;
                }
                appearanceComponent->mAppearance_Ground.mPosX += speedX;
                appearanceComponent->mAppearance_Ground.mPosY += speedY;
                mMovementInProgress = true;
            }
            else if(appearanceComponent->mDiesAtDestination)
            {
                e.remove();
            }
            positionComponent->mNextMoveIsTeleport = false;
        }

        bool VisualMovementSystem::MovementInProgress()
        {
            bool result = mMovementInProgress;
            mMovementInProgress = false;
            return result;
        }
    }
}