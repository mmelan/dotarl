#include "PositionComponent.h"

#include "SDL_assert.h"

namespace DotaRL { namespace GameObject
{
    PositionComponent::PositionComponent(int gridPosX, int gridPosY, Dungeon::Level* level, PositionLayer layer)
        : mGridPosX(gridPosX)
        , mGridPosY(gridPosY)
        , mLevel(level)
        , mNextMoveIsTeleport(true)
        , mLayer(layer)
    {
        AttackedTile.x = -1;
        AttackedTile.y = -1;
        SDL_assert(level);
    }
}
}