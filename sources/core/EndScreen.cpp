#include "EndScreen.h"
#include "SDL_Assert.h"
#include "SDL_Helper.h"
#include "PlayerInterface.h"
#include "Renderer.h"
#include "RenderConstantes.h"
#include "SDL.h"
#include <vector>

namespace DotaRL
{
const float FRAME_MIN_DURATION = 1.0f / 60.0f; // 60 FPS cap
    bool EndScreen(bool victory)
    {
        bool quit_asked = false;
        bool validated = false;
        Uint64 last_tick = SDL_GetPerformanceCounter();
        Renderer& renderer = Renderer::Instance();

        SDL_Surface* bgSurface = SDL_CreateRGBSurface(0, Window::LogicSizeX, Window::LogicSizeY, 32, 0, 0, 0, 0);
        SDL_FillRect(bgSurface, NULL, SDL_MapRGBA(bgSurface->format, 50, 50, 50, 255));
        SDL_Texture* bgTexture = SDL_CreateTextureFromSurface(renderer.SDLRenderer(), bgSurface);
        SDL_FreeSurface(bgSurface);
        
        SDL_Texture* textTexture;
        int text_w, text_h;
        if(victory)
        {
            SDL_Surface* s = SDL_LoadImage_VFS("data/victorious.png");
            text_w = s->w;
            text_h = s->h;
            textTexture = SDL_CreateTextureFromSurface(renderer.SDLRenderer(), s);
            SDL_FreeSurface(s);
        }
        else
        {
            SDL_Surface* s = SDL_LoadImage_VFS("data/defeated.png");
            text_w = s->w;
            text_h = s->h;
            textTexture = SDL_CreateTextureFromSurface(renderer.SDLRenderer(), s);
            SDL_FreeSurface(s);
        }
        
        SDL_Texture* imgTexture;
        int img_w, img_h;
        if(victory)
        {
            SDL_Surface* s = SDL_LoadImage_VFS("data/aegis.jpg");
            img_w = s->w;
            img_h = s->h;
            imgTexture = SDL_CreateTextureFromSurface(renderer.SDLRenderer(), s);
            SDL_FreeSurface(s);
        }
        else
        {
            SDL_Surface* s = SDL_LoadImage_VFS("data/tomb.png");
            img_w = s->w;
            img_h = s->h-100;
            imgTexture = SDL_CreateTextureFromSurface(renderer.SDLRenderer(), s);
            SDL_FreeSurface(s);
        }

        int keyListeningTimer = 60;
        while (!quit_asked && !validated)
        {
            //cap des frames
            renderer.LoopStart();
            Uint64 current_tick = SDL_GetPerformanceCounter();
            float remaining_time;
            while ((remaining_time = static_cast<float>(current_tick - last_tick) / SDL_GetPerformanceFrequency() - FRAME_MIN_DURATION) < 0)
            {
                SDL_Delay(static_cast<uint32_t>(-remaining_time*1000.0f));
                current_tick = SDL_GetPerformanceCounter();
            }
            
            if(keyListeningTimer > 0)
            {
                keyListeningTimer--;
            }
            else
            {
                SDL_Event event;

                while (SDL_PollEvent(&event))
                {
                    //Boucle d'interpretation des evennement
                    switch(event.type)
                    {
                    case SDL_QUIT:
                        quit_asked = true;
                        break;
                    case SDL_KEYDOWN:
                        if (event.key.keysym.sym == SDLK_ESCAPE)
                            quit_asked = true;
                        else if (event.key.keysym.sym == SDLK_F4 && (event.key.keysym.mod & KMOD_ALT))
                            quit_asked = true;
                        else if (event.key.keysym.sym == SDLK_q && (event.key.keysym.mod & KMOD_CTRL))
                            quit_asked = true;
                        else
                        {
                            validated = true;
                        }
                        break;
                    default:
                        break;
                    };
                }
            }
            {
	            SDL_Rect position;
                position.x = 0;
                position.y = 0;
                position.w = Window::LogicSizeX;
                position.h = Window::LogicSizeY;
                renderer.Render(bgTexture, nullptr, &position, false);
            }
            {
	            SDL_Rect position;
                position.x = (Window::LogicSizeX - img_w ) /2;
                position.y = Window::LogicSizeY / 4-50;
                position.w = img_w;
                position.h = img_h;
                renderer.Render(imgTexture, nullptr, &position, false);
            }
            {
	            SDL_Rect position;
                position.x = (Window::LogicSizeX - text_w ) /2;
                position.y = (Window::LogicSizeY  / 3) + img_h - 100;
                position.w = text_w;
                position.h = text_h;
                renderer.Render(textTexture, nullptr, &position, false);
            }
            renderer.Process();
        }
        
        SDL_DestroyTexture(imgTexture);
        SDL_DestroyTexture(textTexture);
        SDL_DestroyTexture(bgTexture);
        return quit_asked;
    }

}