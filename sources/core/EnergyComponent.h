#pragma once

#include <cstdint>

#include "Artemis/Component.h"

namespace DotaRL { namespace GameObject
{
    class EnergyComponent : public artemis::Component
    {
    public:
        EnergyComponent();

        bool Paused;
        int64_t DateNextAction;
    };
}
}
