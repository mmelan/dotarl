#pragma once

#include "Artemis/Component.h"
#include <vector>
#include "AIBehaviour.h"

namespace DotaRL
{
    namespace GameObject
    {
        enum class Control
        {
            keyboard,
            IA
        };
        class ControllableComponent : public artemis::Component
        {
        public:
            ControllableComponent(Control controleType);
            Control mControleType;
            std::vector<AIActions> mPossibleActions;
        };
    }
}
