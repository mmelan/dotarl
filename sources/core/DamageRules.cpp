#include "DamageRules.h"

#include "SDL_assert.h"
#include "SDL_log.h"
#include "StatsComponent.h"
#include "ExperienceComponent.h"
#include "AppearanceComponent.h"
#include "RandomGenerator.h"
#include "ItemTemplate.h"
#include "Buffs.h"
#include "Console.h"

namespace DotaRL { namespace GameObject
{
    bool ArmorAffected[] = {
        true,
        true,
        false,
        false,
        true,
        false
    };

    bool PhysicalImmuneAffected[] = {
        true,
        true,
        false,
        false,
        true,
        false,
    };

    bool MagicResistanceAffected[] = {
        false,
        false,
        true,
        false,
        true,
        true
    };

    bool MagicImmuneAffected[] = {
        false,
        false,
        true,
        true,
        false,
        false
    };

    void ApplyNormalAttack(artemis::World& world,  artemis::Entity& attacker, artemis::Entity& target, int64_t dateInitiated)
    {
        // etapes
        auto attackerStats = static_cast<CombatStatsComponent*>(attacker.getComponent<CombatStatsComponent>());
        auto attackerBuffList = static_cast<BuffListComponent*>(attacker.getComponent<BuffListComponent>());
        auto targetBuffList = static_cast<BuffListComponent*>(target.getComponent<BuffListComponent>());
        SDL_assert(attackerStats);
        SDL_assert(attackerBuffList);

        // application des orb et autres procs sur le lancement du skill
        AttackRollSummary attackRolls;
        std::vector<OrbEffectId> orbs;
        for(auto b : *attackerBuffList)
            b->AttackLaunched(orbs, attackRolls, target);

        auto min_damage = attackerStats->MinDamage * (1.f + attackRolls.CritBonus);
        auto max_damage = attackerStats->MaxDamage * (1.f + attackRolls.CritBonus);

        // evade eventuel

        bool hackDesolator = EntityHasBuff(target, BuffId::DesolatorDebuff);
        for (auto orb : orbs)
        {
            auto orbDebuffId = GetOrbDebuff(orb);
            if (orbDebuffId != BuffId::Undefined)
                AddBuffToEntity(world, target, attacker, orbDebuffId, 1, dateInitiated);
        }

        for (auto buffId : attackRolls.BuffToPlace)
        {
            AddBuffToEntity(world, target, attacker, buffId, 1, dateInitiated);
        }

        if (!hackDesolator && EntityHasBuff(target, BuffId::DesolatorDebuff))
        {
            auto targetStats = static_cast<CombatStatsComponent*>(target.getComponent<CombatStatsComponent>());
            targetStats->Armor -= 7.f;
        }

        // roll de la plage de degats
        auto rolled_damage = Random::rand_range(static_cast<int>(min_damage), static_cast<int>(max_damage+1));

        for (auto b : *attackerBuffList)
            b->ApplyBuffBonusDamage(&rolled_damage, targetBuffList);

        // application du cleave

        // application des debuffs portes par l'attaque (desolator, les mana burn etc...)

        // calcul amplification des degats situationelle (quelling blade quoi)

        // calcul des resistances et block de la cible

        for (auto b : *attackerBuffList)
            b->AttackLanded(world, target, dateInitiated);

        if (attackRolls.CritBonus > 0.f)
            AddLog("Critical strike!", LogSource::Damage);
        InflictDamage(world, attacker, target, static_cast<float>(rolled_damage), DamageType::Physical);
    }

    void InflictDamage(artemis::World& world,  artemis::Entity& attacker, artemis::Entity& target, float damage, DamageType type)
    {
        auto targetStats = (CombatStatsComponent*)target.getComponent<CombatStatsComponent>();
        auto attackerStats = (CombatStatsComponent*)attacker.getComponent<CombatStatsComponent>();
        auto targetHealth = (HealthManaComponent*)target.getComponent<HealthManaComponent>();

        SDL_assert(targetStats);
        SDL_assert(targetHealth);

        if (MagicImmuneAffected[static_cast<int>(type)] && IsMagicImmune(target))
            return;

        if (PhysicalImmuneAffected[static_cast<int>(type)] && IsPhysicalImmune(target))
            return;

        if (ArmorAffected[static_cast<int>(type)])
        {
            if (type != DamageType::Cleave)
            {
                float block = RollDamageBlock(target);
                damage = std::max(0.f, damage - block);
            }
            damage *= GetArmorVulnerability(static_cast<int>(targetStats->Armor));
        }

        if (MagicResistanceAffected[static_cast<int>(type)])
            damage *= GetMagicVulnerability(target);

        // TODO : check last minute buffs for perfect reduction like Shallow Grave, Dispersion or Bristleback

        targetHealth->CurrentHealth -= damage;
        targetHealth->LastEntityToDamage = attacker.getId();
        auto appearanceComponent = (AppearanceComponent*)attacker.getComponent<AppearanceComponent>();
        auto targetComponent = (AppearanceComponent*)target.getComponent<AppearanceComponent>();
        {
            char text[50];
            sprintf_s(text, "%s hit %s for %d damage", appearanceComponent->mName.c_str(), targetComponent->mName.c_str(), (int)damage);
            AddLog(text, LogSource::Damage);
        }
        // TODO : apply on damage event buffs

        if (targetHealth->CurrentHealth < 0)
        {
            auto faction = (FactionComponent*)target.getComponent<FactionComponent>();
            if(faction->mFaction != Faction::GoodGuys)
            {
                if (attacker.getId() != target.getId())
                {
                    auto attackerExp = (ExperienceComponent*)attacker.getComponent<ExperienceComponent>();
                    if(attackerExp)
                    {
                        int experienceGain = 10 + (targetStats->Level-attackerStats->Level)*2;
                        if(experienceGain > 0)
                            attackerExp->mExperience += experienceGain;
                    }
                }
                RandomlyDropLootOrNot(world, target);
            }
            target.remove();
            {
                char text[50];
                sprintf_s(text, "%s killed %s", appearanceComponent->mName.c_str(), targetComponent->mName.c_str());
                AddLog(text, LogSource::Death);
            }
        }
    }

    bool IsMagicImmune(artemis::Entity& target)
    {
        // TODO
        return false;
    }

    bool IsPhysicalImmune(artemis::Entity& target)
    {
        // TODO
        return false;
    }

    float GetMagicVulnerability(artemis::Entity& target)
    {
        return 1.f;
    }

    float GetArmorVulnerability(int armor)
    {
        if (armor > 0)
            return 1.f - ((0.06f*armor) / (1.f + 0.06f*armor));
        else if (armor < 0)
            return 1.f + ((0.06f*(-armor)) / (1.f + 0.06f*(-armor)));
        else return 1.f;
    }

    float RollDamageBlock(artemis::Entity& target)
    {
        return 0.f;
    }

    void UnitTestArmor(int armor)
    {
        SDL_Log("Damage for %d armor : %.2f%%", armor, 100-GetArmorVulnerability(armor)*100);
    }

    void UnitTestDamageRules()
    {
        UnitTestArmor(0);
        UnitTestArmor(1);
        UnitTestArmor(2);
        UnitTestArmor(3);
        UnitTestArmor(4);
        UnitTestArmor(5);
        UnitTestArmor(10);
        UnitTestArmor(25);
        UnitTestArmor(40);
        UnitTestArmor(-1);
        UnitTestArmor(-2);
        UnitTestArmor(-10);
        UnitTestArmor(-20);
        UnitTestArmor(-40);
    }
}
}
