#pragma once

#include "Artemis/EntityProcessingSystem.h"
#include "Artemis/ComponentMapper.h"
#include "ControllableComponent.h"
#include "PositionComponent.h"
#include "EnergyComponent.h"
#include "VisibilityComponent.h"
#include "dungeon.h"

namespace DotaRL { namespace Dungeon
{
    class Level;
}
}

namespace DotaRL { namespace GameObject
{
    class PositionComponent;

    class PositionSystem : public artemis::EntityProcessingSystem
    {
    public:
        PositionSystem(Dungeon::Level* level);

        void initialize() override;
        void processEntity(artemis::Entity &e) override;
        void removed(artemis::Entity&) override;
        void added(artemis::Entity&) override;
    private:
        Dungeon::Level* mLevel;
        artemis::ComponentMapper<PositionComponent> mPositionMapper;
    };

}
}
