#pragma once

#include <cstdint>

#include "Artemis/Entity.h"

namespace DotaRL { namespace GameObject {
    class ControleSystem;
    struct SkillState;
    bool TryLaunchSkill(artemis::World& world,  artemis::Entity& e, SkillState& skillState, int targetX, int targetY, int64_t dateInitiated);
}
}
