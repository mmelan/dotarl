#include "StatsComponent.h"

namespace DotaRL { namespace GameObject
{
    HealthManaComponent::HealthManaComponent(float baseHealthRegen, float baseManaRegen) :
        IntrinsicHealthRegen(baseHealthRegen),
        IntrinsicManaRegen(baseManaRegen),
        LastEntityToDamage(-1),
        MaxHealth(1.f),
        MaxMana(1.f),
        LastMaxHealth(1.f),
        LastMaxMana(1.f),
        CurrentHealth(1.f),
        CurrentMana(1.f),
        BaseHealthRegen(0.f),
        BaseManaRegen(0.f),
        HealthRegen(0.f),
        ManaRegen(0.f),
        LastHealth(0.f),
        Resting(false)
    {
    }

    HealthManaComponent::HealthManaComponent(float baseHealthRegen, float baseManaRegen, float maxHealth, float maxMana) :
        IntrinsicHealthRegen(baseHealthRegen),
        IntrinsicManaRegen(baseManaRegen),
        LastEntityToDamage(-1),
        MaxHealth(maxHealth),
        MaxMana(maxMana),
        LastMaxHealth(maxHealth),
        LastMaxMana(maxMana),
        CurrentHealth(maxHealth),
        CurrentMana(maxMana),
        BaseHealthRegen(0.f),
        BaseManaRegen(0.f),
        HealthRegen(0.f),
        ManaRegen(0.f),
        LastHealth(0.f),
        Resting(false)
    {
    }

    CombatStatsComponent::CombatStatsComponent(float moveSpeed, float minDamage, float maxDamage, float armor, float baseAttackTime, int range, int level) :
        IntrinsicMoveSpeed(moveSpeed),
        IntrinsicMinDamage(minDamage),
        IntrinsicMaxDamage(maxDamage),
        IntrinsicArmor(armor),
        Range(range),
        BaseMoveSpeed(0.f),
        BaseMinDamage(1.f),
        BaseMaxDamage(1.f),
        BaseAttackTime(baseAttackTime),
        BaseIAS(0.f),
        BaseArmor(0.f),
        MoveSpeed(0.f),
        MinDamage(0.f),
        MaxDamage(0.f),
        IAS(0.f),
        Armor(0.f),
        Level(level)
    {
    };

    CoreStatsComponent::CoreStatsComponent(CoreMainStat mainStat, int baseStr, float strPerLevel, int baseAgi, float agiPerLevel, int baseInt, float intPerLevel) :
        MainStat(mainStat),
        IntrinsicStr(baseStr),
        IntrinsicAgi(baseAgi),
        IntrinsicInt(baseInt),
        BaseStr(baseStr),
        BaseAgi(baseAgi),
        BaseInt(baseInt),
        Str(baseStr),
        Agi(baseAgi),
        Int(baseInt),
        StrPerLevel(strPerLevel),
        AgiPerLevel(agiPerLevel),
        IntPerLevel(intPerLevel)
    {
    }

    int ComputeCoreStatAttackBonus(const CoreStatsComponent& coreStats)
    {
        switch (coreStats.MainStat)
        {
        case CoreMainStat::Str:
            return coreStats.Str;
        case CoreMainStat::Agi:
            return coreStats.Agi;
        case CoreMainStat::Int:
            return coreStats.Int;
        default:
            SDL_assert("Corrupted data in CoreStatsComponent");
        };
        return 0;
    }
}
}