#pragma once

namespace DotaRL
{
    enum class AIActions
    {
        nothing,
        attack,
        flee
    };
}