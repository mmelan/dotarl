#include "dungeon.h"

#include "SDL_assert.h"
#include "SDL_log.h"
#include "SDL_rect.h"

#include <iostream>
#include "RandomGenerator.h"
#include <algorithm>
#include <ctime>
#include "MobTemplate.h"
#include "EntityCreation.h"
#include "PositionComponent.h"

#include "Artemis/Entity.h"

namespace DotaRL { namespace Dungeon
{
    const auto MaxSearchRadius = 100;

    namespace
    {
        struct Room
        {
            int posX, posY;
            int sizeX, sizeY;
        };

        SDL_Point CenterRoom(const Room& r)
        {
            SDL_Point result;
            result.x = r.posX + r.sizeX / 2;
            result.y = r.posY + r.sizeY / 2;
            return result;
        }

        bool HasWaterAround(const Level& level, int x, int y)
        {
            SDL_assert(x > 0);
            SDL_assert(y > 0);
            SDL_assert(x < level.SizeX() - 1);
            SDL_assert(y < level.SizeY() - 1);
            if (level.GetFeature(x, y, Layers::Ground) == Feature::Water)
                return false;
            return
                level.GetFeature(x+1, y+1, Layers::Ground) == Feature::Water ||
                level.GetFeature(x+1, y  , Layers::Ground) == Feature::Water ||
                level.GetFeature(x+1, y-1, Layers::Ground) == Feature::Water ||
                level.GetFeature(x  , y+1, Layers::Ground) == Feature::Water ||
                level.GetFeature(x  , y-1, Layers::Ground) == Feature::Water ||
                level.GetFeature(x-1, y+1, Layers::Ground) == Feature::Water ||
                level.GetFeature(x-1, y  , Layers::Ground) == Feature::Water ||
                level.GetFeature(x-1, y-1, Layers::Ground) == Feature::Water;
        }

        void AddRandomRiver(Level& level)
        {
            SDL_assert(level.SizeX() > 20);
            SDL_assert(level.SizeY() > 20);

            int y = Random::rand_range(10, level.SizeY() - 10);
            int x = level.SizeX() - 1;
            while (x >= 0)
            {
                level.GetFeature(x, y, Layers::Ground) = Feature::Water;
                level.GetFeature(x, y, Layers::Collisionnable) = Feature::Nothing;
                int river_vertical_movement = std::max(Random::rand_int(10) - 7, 0);
                int river_direction;
                if (y < level.SizeY() / 4)
                    river_direction = 1;
                else if (y > (3*level.SizeY()) / 4)
                    river_direction = -1;
                else
                    river_direction = Random::rand_int(2) != 0?1:-1;

                for(int i = 0; i < river_vertical_movement; i++)
                {
                    y += river_direction;
                    level.GetFeature(x, y, Layers::Ground) = Feature::Water;
                    level.GetFeature(x, y, Layers::Collisionnable) = Feature::Nothing;
                }
                x--;
            }

            // Put walls around river
            for (int x = 1; x < level.SizeX() - 1; x++)
            {
                for (int y = 1; y < level.SizeY() - 1; y++)
                {
                    if (HasWaterAround(level, x, y))
                        level.GetFeature(x, y, Layers::Collisionnable) = Feature::Mountain;
                }
            }
        }

        void DrawHorizontalCorridor(Level& level, int startx, int endx, int y)
        {
            if (startx > endx)
                std::swap(startx, endx);
            for(int x = startx; x <= endx; x++)
                if (level.GetFeature(x, y, Layers::Ground) != Feature::Water)
                    level.GetFeature(x, y, Layers::Collisionnable) = Feature::Nothing;
        }

        void DrawVerticalCorridor(Level& level, int starty, int endy, int x)
        {
            if (starty > endy)
                std::swap(starty, endy);
            for(int y = starty; y <= endy; y++)
                if (level.GetFeature(x, y, Layers::Ground) != Feature::Water)
                    level.GetFeature(x, y, Layers::Collisionnable) = Feature::Nothing;
        }

        void DrawCorridor(Level& level, SDL_Point start, SDL_Point end)
        {
            if (start.x == end.x)
            {
                DrawVerticalCorridor(level, start.y, end.y, start.x);
            }
            else if (start.y == end.y)
            {
                DrawHorizontalCorridor(level, start.x, end.x, start.y);
            }
            else
            {
                bool corridor_model = Random::rand_int(2) != 0;
                if (corridor_model)
                {
                    int split_x;
                    if (start.x > end.x)
                        split_x = Random::rand_range(end.x, start.x);
                    else
                        split_x = Random::rand_range(start.x, end.x);
                    DrawVerticalCorridor(level, start.y, end.y, split_x);
                    DrawHorizontalCorridor(level, start.x, split_x, start.y);
                    DrawHorizontalCorridor(level, end.x, split_x, end.y);
                }
                else
                {
                    int split_y;
                    if (start.y > end.y)
                        split_y = Random::rand_range(end.y, start.y);
                    else
                        split_y = Random::rand_range(start.y, end.y);
                    DrawVerticalCorridor(level, start.y, split_y, start.x);
                    DrawVerticalCorridor(level, end.y, split_y, end.x);
                    DrawHorizontalCorridor(level, start.x, end.x, split_y);
                }
            }
        }

        void RecolorDireSide(Level& level)
        {
            for(int x=0; x < level.SizeX(); x++)
            {
                for(int y=0; y < level.SizeY(); y++)
                {

                    Feature& f = level.GetFeature(x, y, Layers::Ground);
                    if (f == Feature::RadiantGrass)
                    {
                        f = Feature::DireGrass;
                        Feature& fc = level.GetFeature(x, y, Layers::Collisionnable);
                        if (fc == Feature::RadiantTree)
                            fc = Feature::DireTree;
                    }
                    else
                        break;
                }
            }
        }
    };

    SDL_Point MakeRandomLevel(Level& level, StartingSide side, artemis::World& world)
    {
        SDL_assert(level.SizeX() > 20);
        SDL_assert(level.SizeY() > 20);

        const int room_cell_size = 16;
        const int min_room_size = 4;
        const int max_room_size = 12;
        const int nb_cells_x = (level.SizeX() - 2) / room_cell_size;
        const int nb_cells_y = (level.SizeY() - 2) / room_cell_size;

        level.FillFeature(Feature::RadiantGrass, Layers::Ground);
        level.FillFeature(Feature::RadiantTree, Layers::Collisionnable);
        for(int i = 0; i < level.SizeX(); i++)
        {
            level.GetFeature(i, 0, Layers::Collisionnable) = Feature::HighMountain;
            level.GetFeature(i, level.SizeY() - 1, Layers::Collisionnable) = Feature::HighMountain;
        }
        for(int i = 0; i < level.SizeY(); i++)
        {
            level.GetFeature(0, i, Layers::Collisionnable) = Feature::HighMountain;
            level.GetFeature(level.SizeX() - 1, i, Layers::Collisionnable) = Feature::HighMountain;
        }

        // Make some holes in the trees
        for(int x = 1; x < level.SizeX() - 1; x++)
        {
            for(int y = 1; y < level.SizeY() - 1; y++)
            {
                if (Random::d100() < 5)
                    level.GetFeature(x, y, Layers::Collisionnable) = Feature::Nothing;
            }
        }

        // Make a river
        AddRandomRiver(level);

        std::vector<MobTemplate> mobTable = GetMobTable();
        std::vector<Room> rooms;
        rooms.resize(nb_cells_x*nb_cells_y);
        for(int x = 0; x < nb_cells_x; x++)
        {
            for (int y = 0; y < nb_cells_y; y++)
            {
                Room& room = rooms[x + y*nb_cells_x];

                const int minx = x * room_cell_size + 1;
                const int maxx = minx + room_cell_size - 1;
                const int miny = y * room_cell_size + 1;
                const int maxy = miny + room_cell_size - 1;

                const int sizex = Random::rand_range(min_room_size, max_room_size);
                const int sizey = Random::rand_range(min_room_size, max_room_size);

                room.posX = Random::rand_range(minx, maxx - sizex);
                room.posY = Random::rand_range(miny, maxy - sizey);
                room.sizeX = sizex;
                room.sizeY = sizey;

                level.ReplaceFeatureRect(Feature::RadiantTree, Feature::Nothing, room.posX, room.posX + sizex, room.posY, room.posY + sizey, Layers::Collisionnable);
            }
        }
        for(int x = 0; x < nb_cells_x - 1; x++)
        {
            for (int y = 0; y < nb_cells_y - 1; y++)
            {
                Room& room1 = rooms[x   +  y   *nb_cells_x];
                Room& room2 = rooms[x+1 +  y   *nb_cells_x];
                Room& room3 = rooms[x   + (y+1)*nb_cells_x];

                DrawCorridor(level, CenterRoom(room1), CenterRoom(room2));
                DrawCorridor(level, CenterRoom(room1), CenterRoom(room3));
            }
            {
                Room& room1 = rooms[x   + (nb_cells_y-1)*nb_cells_x];
                Room& room2 = rooms[x+1 + (nb_cells_y-1)*nb_cells_x];
                DrawCorridor(level, CenterRoom(room1), CenterRoom(room2));
            }
        }
        for (int y = 0; y < nb_cells_y - 1; y++)
        {
            Room& room1 = rooms[(nb_cells_x-1) +  y   *nb_cells_x];
            Room& room2 = rooms[(nb_cells_x-1) + (y+1)*nb_cells_x];

            DrawCorridor(level, CenterRoom(room1), CenterRoom(room2));
        }

        // Recolor north side of river into dire
        RecolorDireSide(level);

        SDL_Point fountain;
        if (side == StartingSide::Radiant)
        {
            fountain = CenterRoom(rooms[nb_cells_x*(nb_cells_y-1)]);
        }
        else
        {
            fountain = CenterRoom(rooms[nb_cells_x-1]);
        }
        level.GetFeature(fountain.x, fountain.y, Layers::Collisionnable) = Feature::Fountain;

        //spawn des mobs
        for(int x = 0; x < nb_cells_x; x++)
        {
            for (int y = 0; y < nb_cells_y; y++)
            {
                Room& room = rooms[x + y*nb_cells_x];
                int distanceToPlayerSpawn = std::max(std::abs(fountain.x-room.posX), std::abs(fountain.y-room.posY));
                int levelMax = distanceToPlayerSpawn / 10;
                CreateMobInRoom(world, room.posX, room.posX + room.sizeX, room.posY, room.posY + room.sizeY, mobTable, level, levelMax);
            }
        }

        return fountain;
    }

    void DumpLevel(const Level& level)
    {
        SDL_assert(level.SizeX() < 1024);
        for(int y = 0; y < level.SizeY(); y++)
        {
            char buffer[1025];
            for(int x = 0; x < level.SizeX(); x++)
            {
                const char* tile_table = "##**..~";
                buffer[x] = tile_table[static_cast<int>(level.GetFeature(x, y, Layers::Collisionnable))];
            }
            buffer[level.SizeX()] = 0;
            SDL_Log("%s", buffer);
        }
    }

    static bool BlocksVision(Dungeon::Level* level, int x, int y)
    {
        auto f = level->GetFeature(x, y, Dungeon::Layers::Collisionnable);
        switch (f)
        {
        case Dungeon::Feature::Nothing:
        case Dungeon::Feature::Fountain:
        case Dungeon::Feature::Mountain:
        case Dungeon::Feature::DireGrass:
        case Dungeon::Feature::RadiantGrass:
            return false;
        default:
            return true;
        }
    }

    static bool TestMapOpacity(void *map, int x, int y)
    {
        auto level = static_cast<Level*>(map);
        return BlocksVision(level, x, y);
    }

    static void MarkTileAsVisible(void *map, int x, int y, int dx, int dy, void *output)
    {
        auto result = static_cast<Path*>(output);
        result->push_back(std::make_pair(x, y));
    }

    Level::Level(int sizeX, int sizeY) :
        mSizeX(sizeX),
        mSizeY(sizeY)
    {
        int listsSize = sizeX*sizeY;
        mFeatures[Layers::Ground].resize(listsSize);
        mFeatures[Layers::Collisionnable].resize(listsSize);
        mLastSeenTileState[Layers::Ground].resize(listsSize);
        mLastSeenTileState[Layers::Collisionnable].resize(listsSize);
        mDetails.resize(listsSize);
        mTileVisibility.resize(listsSize);
        mCreatures.resize(listsSize);
        mItems.resize(listsSize);
        
        std::fill(mCreatures.begin(), mCreatures.end(), -1);
        std::fill(mItems.begin(), mItems.end(), -1);
        std::fill(mTileVisibility.begin(), mTileVisibility.end(), false);
        std::fill(mLastSeenTileState[Layers::Ground].begin(), mLastSeenTileState[Layers::Ground].end(), Feature::Nothing);
        std::fill(mLastSeenTileState[Layers::Collisionnable].begin(), mLastSeenTileState[Layers::Collisionnable].end(), Feature::Nothing);

        fov_settings_init(&mFovSettings);
        fov_settings_set_shape(&mFovSettings, FOV_SHAPE_CIRCLE_PRECALCULATE);
        fov_settings_set_opaque_apply(&mFovSettings, FOV_OPAQUE_APPLY);
        fov_settings_set_opacity_test_function(&mFovSettings, TestMapOpacity);
        fov_settings_set_apply_lighting_function(&mFovSettings, MarkTileAsVisible);
    }

    Level::~Level()
    {
        fov_settings_free(&mFovSettings);
    }

    void Level::loopStart()
    {
        std::fill(mTileVisibility.begin(), mTileVisibility.end(), false);
    }

    std::pair<int, int> Level::Size() const
    {
        return std::make_pair( mSizeX, mSizeY );
    }

    int Level::SizeX() const
    {
        return mSizeX;
    }

    int Level::SizeY() const
    {
        return mSizeY;
    }

    bool Level::IsInMap(int x, int y) const
    {
        return ( x >= 0 &&
            y >= 0 &&
            x < SizeX() &&
            y < SizeY() );
    }

    size_t Level::GetIndex(int x, int y) const
    {
        SDL_assert(IsInMap(x, y));
        return y*mSizeX+x;
    }

    Feature Level::GetFeature(int x, int y, Layers layer) const
    {
        SDL_assert(x < mSizeX);
        SDL_assert(y < mSizeY);
        if (x < 0 || x >= mSizeX || y < 0 || y > mSizeY)
            return Feature::Nothing;
        return mFeatures.at(layer)[GetIndex(x, y)];
    }

    Feature Level::GetLastKnownFeature(int x, int y, Layers layer) const
    {
        SDL_assert(x < mSizeX);
        SDL_assert(y < mSizeY);
        if (x < 0 || x >= mSizeX || y < 0 || y > mSizeY)
            return Feature::Nothing;
        return mLastSeenTileState.at(layer)[GetIndex(x, y)];
    }

    Detail Level::GetDetail(int x, int y) const
    {
        if (x < 0 || x >= mSizeX || y < 0 || y > mSizeY)
            return Detail::Nothing;
        return mDetails[GetIndex(x, y)];
    }

    bool Level::GetVisibility(int x, int y) const
    {
        if (x < 0 || x >= mSizeX || y < 0 || y > mSizeY)
            return false;
        return mTileVisibility[GetIndex(x, y)];
    }

    void Level::SetVisibility(int x, int y)
    {
        if (x < 0 || x >= mSizeX || y < 0 || y > mSizeY)
            return;
        int index = GetIndex(x, y);
        mTileVisibility[index] = true;
        for(int i = 0; i < (int)Layers::Lenght; ++i)
            mLastSeenTileState[(Layers)i][index] = mFeatures[(Layers)i][index];
    }

    Feature& Level::GetFeature(int x, int y, Layers layer)
    {
        SDL_assert(x < mSizeX);
        SDL_assert(y < mSizeY);
        return mFeatures[layer][GetIndex(x, y)];
    }

    Detail& Level::GetDetail(int x, int y)
    {
        SDL_assert(x < mSizeX);
        SDL_assert(y < mSizeY);
        return mDetails[GetIndex(x, y)];
    }

    void Level::FillFeature(Feature f, Layers layer)
    {
        for(auto& i : mFeatures[layer])
            i = f;
    }

    void Level::FillFeatureRect(Feature f, int minx, int maxx, int miny, int maxy, Layers layer)
    {
        for(int x = minx; x < maxx; x++)
        {
            for(int y = miny; y < maxy; y++)
            {
                GetFeature(x, y, layer) = f;
            }
        }
    }

    void Level::ReplaceFeatureRect(Feature f0, Feature f1, int minx, int maxx, int miny, int maxy, Layers layer)
    {
        for(int x = minx; x < maxx; x++)
        {
            for(int y = miny; y < maxy; y++)
            {
                Feature& f = GetFeature(x, y, layer);
                if (f == f0)
                    f = f1;
            }
        }
    }

    int Level::GetCreature(int x, int y) const
    {
        if (!IsInMap(x, y))
            return -1;
        return mCreatures[GetIndex(x, y)];
    }

    int Level::GetItem(int x, int y) const
    {
        if (!IsInMap(x, y))
            return -1;
        return mItems[GetIndex(x, y)];
    }


    bool IsMoveableFeature(Feature f)
    {
        return f == Feature::Nothing;
    }

    bool Level::IsAccessibleTile(int x, int y, EntityLayer layer) const
    {
        if(!IsInMap(x,y))
            return false;

        auto feature = GetFeature(x, y, Layers::Collisionnable);
        if (!IsMoveableFeature(feature))
            return false;
        int nbNeighborAccessible = 0;
        for(int i = -1; i <= 1; ++i)
        {
            for(int j = -1; j <= 1; ++j)
            {
                int nx = x+i;
                int ny = y+i;
                if(!IsInMap(nx,ny))
                    continue;
                auto feature = GetFeature(nx, ny, Layers::Collisionnable);
                if (IsMoveableFeature(feature))
                    nbNeighborAccessible++;
            }
        }
        if(nbNeighborAccessible < 3)
            return false;
        switch (layer)
        {
        case EntityLayer::None:
            return true;
        case EntityLayer::Creature:
            return GetCreature(x, y) == -1;
        case EntityLayer::Item:
            return GetItem(x, y) == -1;
        default:
            SDL_assert("Corrupted layer parameter");
            return false;
        }
    }

    bool Level::IsMovableTile(int x, int y, EntityLayer layer) const
    {
        if(!IsInMap(x,y))
            return false;

        auto feature = GetFeature(x, y, Layers::Collisionnable);
        if (!IsMoveableFeature(feature))
            return false;
        switch (layer)
        {
        case EntityLayer::None:
            return true;
        case EntityLayer::Creature:
            return GetCreature(x, y) == -1;
        case EntityLayer::Item:
            return GetItem(x, y) == -1;
        default:
            SDL_assert("Corrupted layer parameter");
            return false;
        }
    }

    void Level::PlaceCreatureAt(int entity, int x, int y)
    {
        SDL_assert(mCreatures[GetIndex(x, y)] == -1);
        mCreatures[GetIndex(x, y)] = entity;
    }

    void Level::RemoveCreatureAt(int entity, int x, int y)
    {
        SDL_assert(mCreatures[GetIndex(x, y)] == entity);
        mCreatures[GetIndex(x, y)] = -1;
    }

    void Level::PlaceItemAt(int entity, int x, int y)
    {
        SDL_assert(mItems[GetIndex(x, y)] == -1);
        mItems[GetIndex(x, y)] = entity;
    }

    void Level::RemoveItemAt(int entity, int x, int y)
    {
        SDL_assert(mItems[GetIndex(x, y)] == entity);
        mItems[GetIndex(x, y)] = -1;
    }

    std::vector<int> Level::GetCreatureInArea(const SDL_Point& origin, int radius, EntityLayer layer) const
    {
        std::vector<int> result;
        for(int x = origin.x - radius; x <= origin.x + radius; ++x)
        {
            for(int y = origin.y - radius; y <= origin.y + radius; ++y)
            {
                if(IsInMap(x, y))
                {
                    int entity = GetCreature(x,y);
                    if(entity >= 0)
                        result.push_back(entity);
                }
            }
        }
        return result;
    }

    SDL_Point Level::FindEmptyPosition(const SDL_Point& origin, EntityLayer layer) const
    {
        SDL_Point result;
        if (IsAccessibleTile(origin.x, origin.y, layer))
        {
            return origin;
        }
        for(int radius = 1; radius < MaxSearchRadius; radius++)
        {
            for(int i=0; i < radius*2; i++)
            {
                if (IsAccessibleTile(origin.x+radius, origin.y-radius+i, layer))
                {
                    result.x = origin.x+radius;
                    result.y = origin.y-radius+i;
                    return result;
                }
                if (IsAccessibleTile(origin.x-radius, origin.y-radius+i, layer))
                {
                    result.x = origin.x-radius;
                    result.y = origin.y-radius+i;
                    return result;
                }
                if (IsAccessibleTile(origin.x-radius+i, origin.y+radius, layer))
                {
                    result.x = origin.x-radius+i;
                    result.y = origin.y+radius;
                    return result;
                }
                if (IsAccessibleTile(origin.x-radius+i, origin.y-radius, layer))
                {
                    result.x = origin.x-radius+i;
                    result.y = origin.y-radius;
                    return result;
                }
            }
        }

        SDL_assert(false);
        result.x = -1;
        result.y = -1;
        return result;
    }

    struct SearchPathElement
    {
        SearchPathElement()
            : mDistanceLeftEstimation(FLT_MAX)
            , mDistanceFromStart(FLT_MAX)
            , father(nullptr)
            , mOpened(false)
            , mClosed(false)
        {
        }
        std::pair<int, int> mPosition;
        float mDistanceLeftEstimation;
        float mDistanceFromStart;
        SearchPathElement* father;
        bool mOpened;
        bool mClosed;
    };


    struct PriorityQueue
    {
        std::vector<SearchPathElement*> mQueue;
        void Sort()
        {
            struct {
                bool operator()(const SearchPathElement* a, const SearchPathElement* b)
                {   
                    float aCost = a->mDistanceFromStart + a->mDistanceLeftEstimation;
                    float bCost = b->mDistanceFromStart + b->mDistanceLeftEstimation;
                    return aCost < bCost;
                }   
            } customLess;
            std::sort(mQueue.begin(), mQueue.end(), customLess);
        }
    };

    Path Level::SearchPath(int startX, int startY, int endX, int endY)
    {
        PriorityQueue priorityQueue;
        std::vector<SearchPathElement> mElementList;
        mElementList.resize(mSizeX * mSizeY);
        SearchPathElement* startElem = &mElementList[startY * mSizeX + startX];
        {
            int distX = startX - endX;
            int distY = startY - endY;
            startElem->mPosition.first = startX;
            startElem->mPosition.second = startY;
            startElem->mDistanceLeftEstimation = (float)std::max(std::abs(distX), std::abs(distY));
            startElem->mOpened = true;
            float distanceFromStart = 0;
            startElem->mDistanceFromStart = distanceFromStart;
            startElem->father = nullptr;
        }
        priorityQueue.mQueue.push_back(startElem);
        SearchPathElement* endElem = nullptr;
        while(!endElem && !priorityQueue.mQueue.empty())
        {
            priorityQueue.Sort();
            SearchPathElement* checkedElem = priorityQueue.mQueue[0];
            priorityQueue.mQueue.erase(priorityQueue.mQueue.begin());
            if(checkedElem->mPosition.first == endX &&
                checkedElem->mPosition.second == endY )
            {
                endElem = checkedElem;
            }
            else
            {
                checkedElem->mClosed = true;
                for(int i = -1; i < 2; ++i)
                {
                    for(int j = -1; j < 2; ++j)
                    {
                        int coordX = checkedElem->mPosition.first + i;
                        int coordY = checkedElem->mPosition.second + j;
                        if(IsInMap(coordX, coordY) && GetFeature(coordX, coordY, Layers::Collisionnable) == Feature::Nothing)
                        {
                            SearchPathElement* neighborElem = &mElementList[coordY * mSizeX + coordX];
                            if(!neighborElem->mClosed)
                            {
                                if(!neighborElem->mOpened)
                                {
                                    int distX = coordX - endX;
                                    int distY = coordY - endY;
                                    neighborElem->mPosition.first = coordX;
                                    neighborElem->mPosition.second = coordY;
                                    neighborElem->mDistanceLeftEstimation = std::max(std::abs(distX), std::abs(distY));
                                    if(GetCreature(coordX, coordY) != -1 && (coordX != endX || coordY != endY))
                                        neighborElem->mDistanceLeftEstimation += 6;
                                    neighborElem->mOpened = true;
                                    priorityQueue.mQueue.push_back(neighborElem);
                                }
                                float distanceFromStart = checkedElem->mDistanceFromStart + 1;
                                if(distanceFromStart < neighborElem->mDistanceFromStart)
                                {
                                    neighborElem->mDistanceFromStart = distanceFromStart;
                                    neighborElem->father = checkedElem;
                                }
                            }
                        }
                    }
                }
            }
        }
        Path outPath;
        if(endElem)
        {
            SearchPathElement* elem = endElem;
            int pathLen = 0;
            while(elem)
            {
                pathLen++;
                elem = elem->father;
            }
            outPath.resize(pathLen);
            elem = endElem;
            while(elem)
            {
                --pathLen;
                outPath[pathLen] = (elem->mPosition);
                elem = elem->father;
            }
        }
        return outPath;
    }

    void Level::FindVisibleCircle(int x, int y, int radius, Path& result)
    {
        result.clear();
        result.push_back(std::make_pair(x, y));
        fov_circle(&mFovSettings, this, &result, x, y, radius);
    }

    void Level::FindRayLine(int x0, int y0, int x1, int y1, Path& result)
    {
        result.clear();
        int dx = abs(x1-x0), sx = x0<x1 ? 1 : -1;
        int dy = abs(y1-y0), sy = y0<y1 ? 1 : -1; 
        int err = (dx>dy ? dx : -dy)/2, e2;

        for(;;)
        {
            result.push_back(std::make_pair(x0, y0));
            if (x0==x1 && y0==y1) break;
            e2 = err;
            if (e2 >-dx) { err -= dy; x0 += sx; }
            if (e2 < dy) { err += dx; y0 += sy; }
        }
    }
}
}
