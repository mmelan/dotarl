#pragma once

#include "RenderConstantes.h"
struct SDL_Texture;
struct SDL_Surface;

namespace DotaRL
{
    namespace Rendering
    {
        class Drawable
        {
        public:
            Drawable();
            
            Layer::Type mLayer;
            float mPosX;
            float mPosY;
            SDL_Texture* mTexture;
            SDL_Surface* mSurface;
        };
    }
}
