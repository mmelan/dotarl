#include "Console.h"
#include "PlayerInterface.h"
#include "RenderConstantes.h"
#include "VirtualFileSystem.h"

namespace DotaRL
{
    int newEventPrintDuration = 220;
    Console::Console()
        : mFull(false)
    {
        SDL_RWops* file = VirtualFileSystem::OpenFile("data/font/segoepr.ttf");
        mPolice = TTF_OpenFontRW(file, 1, 10);
    }

    Console::~Console()
    {
    }

    void Console::Process()
    {
        int nbDrawn = 0;
        int startTextDrawY = Interface::StartY;
        for(int i = mLogList.size()-1; i >= std::max(0, (int)mLogList.size()-25); --i)
        {
            if(mLogList[i].Time < newEventPrintDuration || mFull )
            {
                nbDrawn++;
                mLogList[i].Time++;
                startTextDrawY -= 20;
                DrawRect(0, startTextDrawY, Window::LogicSizeY /2, 20, 110, 110, 110, mFull?255:120);
                int r = 0, g = 0, b = 0;
                switch(mLogList[i].Source)
                {
                case LogSource::Damage:
                    break;
                case LogSource::Info:
                    r = 255;
                    g = 255;
                    break;
                case LogSource::Death:
                    r = 255;
                    break;
                case LogSource::Gain:
                    g = 255;
                    break;
                }
                DrawText(mLogList[i].Message.c_str(), 10, startTextDrawY, Window::LogicSizeY /2, 20, r, g, b, 255, mPolice);
            }
        }
        if(nbDrawn == 0)
        {
            startTextDrawY -= 20;
            DrawRect(0, startTextDrawY, Window::LogicSizeY /2, 20, 110, 110, 110, mFull?255:120);
            int r = 255, g = 255, b = 255;
            DrawText("hint : g=loot / alt + shortcut=drop items / c=open console / f=target", 10, startTextDrawY, Window::LogicSizeY /2, 20, r, g, b, 255, mPolice);
        }
    }

    void Console::Toggle()
    {
        mFull = !mFull;
    }

    void AddLog(char* log, LogSource source)
    {
        Console::Instance().mLogList.push_back(LogText(log, source));
    }


}