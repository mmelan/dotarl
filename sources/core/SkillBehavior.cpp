#include "SkillBehavior.h"

#include "SDL_assert.h"
#include "SkillSetComponent.h"
#include "PositionComponent.h"
#include "FactionComponent.h"
#include "DamageRules.h"
#include "StatsComponent.h"
#include "Buffs.h"
#include "EnergyComponent.h"
#include "DungeonRenderer.h"
#include "Console.h"
#include "ControleSystem.h"
#include "EntityCreation.h"

namespace DotaRL { namespace GameObject
{
    static void ApplySkillEffectOnEntity(artemis::World& world, artemis::Entity& attacker, artemis::Entity& target, SkillState& skillState, int64_t dateInitiated)
    {
        auto skillLevel = skillState.mSkillLevel;
        auto buffEntity = AddBuffToEntity(world, target, attacker, skillState.mSkillDescription->mActiveBuff, skillLevel, dateInitiated);
        const Damage& damage = skillState.mSkillDescription->mDamages[skillLevel];
        if (damage.Amount)
            InflictDamage(world, attacker, target, damage.Amount, damage.Type);
    }

    bool LaunchPBAoESkill(artemis::World& world,  artemis::Entity& e, SkillState& skillState, int64_t dateInitiated)
    {
        auto skillLevel = skillState.mSkillLevel;
        auto radius = skillState.mSkillDescription->Range[skillLevel];

        auto positionComponent = (PositionComponent*)e.getComponent<PositionComponent>();
        SDL_Point origin={positionComponent->mGridPosX, positionComponent->mGridPosY};

        std::vector<int> targetList = positionComponent->mLevel->GetCreatureInArea(origin, radius);

        auto faction = (FactionComponent*)e.getComponent<FactionComponent>();
        for(int i = 0; i < targetList.size(); ++i)
        {
            artemis::Entity& target = world.getEntity(targetList[i]);
            FactionComponent* targetFaction =  (FactionComponent*)target.getComponent<FactionComponent>();
            if(targetFaction && targetFaction->mFaction != faction->mFaction)
                ApplySkillEffectOnEntity(world, e, target, skillState, dateInitiated);
        }
        for(int x = -radius; x <= radius; ++x)
        {
            for(int y = -radius; y <= radius; ++y)
            {
                if(positionComponent->mLevel->IsInMap(origin.x + x, origin.y + y))
                {
                    if(x != origin.x || y != origin.y)
                        positionComponent->mLevel->mLevelRenderer->Animate(origin.x + x, origin.y + y, 10+std::abs(x)*2+std::abs(y)*2);
                }
            }
        }
        return true;
    }

    static void ApplyScriptedSkillEffect(artemis::World& world, SkillId skillId, artemis::Entity& attacker, int targetX, int targetY, int skillLevel, int64_t dateInitiated)
    {
        switch (skillId)
        {
        case SkillId::HealingSalve:
            {
                auto healthManaComponent = (HealthManaComponent*)attacker.getComponent<HealthManaComponent>();
                healthManaComponent->CurrentHealth = std::min(healthManaComponent->CurrentHealth + 400, healthManaComponent->MaxHealth);
            }
            break;
        case SkillId::Clarity:
            {
                auto healthManaComponent = (HealthManaComponent*)attacker.getComponent<HealthManaComponent>();
                healthManaComponent->CurrentMana = std::min(healthManaComponent->CurrentMana + 400, healthManaComponent->MaxMana);
            }
            break;
        case SkillId::Blink:
            {
                auto positionComponent = (PositionComponent*)attacker.getComponent<PositionComponent>();
                positionComponent->mLevel->RemoveCreatureAt(attacker.getId(), positionComponent->mGridPosX, positionComponent->mGridPosY);
                positionComponent->mGridPosX = targetX;
                positionComponent->mGridPosY = targetY;
                positionComponent->mLevel->PlaceCreatureAt(attacker.getId(), targetX, targetY);
                positionComponent->mNextMoveIsTeleport = true;
            }
            break;
        case SkillId::Plague_Ward:
            {
                auto positionComponent = (PositionComponent*)attacker.getComponent<PositionComponent>();
                CreatePlagueWard(world, targetX, targetY, positionComponent->mLevel, skillLevel);
            }
            break;
        default:
            break;
        }
    }

    bool TryLaunchSkill(artemis::World& world, artemis::Entity& e, SkillState& skillState, int targetX, int targetY, int64_t dateInitiated)
    {
        if( skillState.mSkillLevel <= 0 ||
            skillState.mDateEndCooldown > dateInitiated ||
            !skillState.mSkillDescription->isActivable)
            return false;

        int manaCost = skillState.mSkillDescription->mManaCost[skillState.mSkillLevel];
        auto healthManaComponent = (HealthManaComponent*)e.getComponent<HealthManaComponent>();
        auto energyComponent = static_cast<EnergyComponent*>(e.getComponent<EnergyComponent>());
        if(healthManaComponent->CurrentMana < manaCost)
        {
            AddLog("Not enough mana", LogSource::Info);
            return false;
        }

        auto& skillDesc = *skillState.mSkillDescription;
        bool success = true;
        switch (skillDesc.Shape)
        {
        case SkillShape::Instant:
            if(skillDesc.mActiveBuff != BuffId::Undefined)
                AddBuffToEntity(world, e, e, skillDesc.mActiveBuff, skillState.mSkillLevel, dateInitiated);
            else
                ApplyScriptedSkillEffect(world, skillDesc.mSkillId, e, targetX, targetY, skillState.mSkillLevel, dateInitiated);
            break;
        case SkillShape::PBAoE:
            LaunchPBAoESkill(world, e, skillState, dateInitiated);
            break;
        case SkillShape::GroundTarget:
            ApplyScriptedSkillEffect(world, skillDesc.mSkillId, e, targetX, targetY, skillState.mSkillLevel, dateInitiated);
            break;
        case SkillShape::Target:
            {
                auto positionComponent = static_cast<PositionComponent*>(e.getComponent<PositionComponent>());
                auto faction = (FactionComponent*)e.getComponent<FactionComponent>();
                auto creature = positionComponent->mLevel->GetCreature(targetX, targetY);
                if (creature <= 0)
                    break;
                auto& targetEntity = world.getEntity(creature);
                auto targetFaction = (FactionComponent*)targetEntity.getComponent<FactionComponent>();
                if (targetFaction && targetFaction->mFaction != faction->mFaction)
                    ApplySkillEffectOnEntity(world, e, targetEntity, skillState, dateInitiated);
            }
            break;
        case SkillShape::Ray:
            {
                auto positionComponent = static_cast<PositionComponent*>(e.getComponent<PositionComponent>());
                auto faction = (FactionComponent*)e.getComponent<FactionComponent>();
                Dungeon::Path path;
                positionComponent->mLevel->FindRayLine(positionComponent->mGridPosX, positionComponent->mGridPosY, targetX, targetY, path);
                for (auto p : path)
                {
                    auto creature = positionComponent->mLevel->GetCreature(p.first, p.second);
                    positionComponent->mLevel->mLevelRenderer->Animate(p.first, p.second, 10);
                    if (creature <= 0)
                        continue;
                    auto& targetEntity = world.getEntity(creature);
                    auto targetFaction = (FactionComponent*)targetEntity.getComponent<FactionComponent>();
                    if (targetFaction && targetFaction->mFaction != faction->mFaction)
                        ApplySkillEffectOnEntity(world, e, targetEntity, skillState, dateInitiated);
                }
            }
            break;
        default:
            success = false;
            // rien ici normalement
            break;
        };

        if(success)
        {
            energyComponent->DateNextAction = dateInitiated + skillState.mSkillDescription->CastTime;
            skillState.mDateEndCooldown = dateInitiated + skillState.mSkillDescription->mCooldown[skillState.mSkillLevel];
            healthManaComponent->CurrentMana -= manaCost;
            return true;
        }
        return false;
    }

}
}
