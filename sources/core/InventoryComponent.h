#pragma once

#include "Artemis/Component.h"
#include "ItemState.h"
#include <vector>

namespace DotaRL { namespace GameObject
{
    class InventoryComponent : public artemis::Component
    {
    public:
        InventoryComponent();

        std::vector<ItemState*> mOwnedItemList;
    };
}
}
