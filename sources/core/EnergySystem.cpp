#include "EnergySystem.h"
#include "Artemis/Entity.h"

namespace DotaRL { namespace GameObject
{
    const int64_t PeriodicEffectDate = 100;

    EnergySystem::EnergySystem() :
        mEntityActing(nullptr),
        mDateNextAction(0),
        mCurrentDate(0)
    {
        addComponentType<EnergyComponent>();
    }

    void EnergySystem::initialize()
    {
        mEnergyMapper.init(*world);
    }

    void EnergySystem::processEntity(artemis::Entity &e)
    {
        EnergyComponent* energyComponent = mEnergyMapper.get(e);
        if (energyComponent->DateNextAction < 0)
            energyComponent->DateNextAction = mCurrentDate;
        if (energyComponent->DateNextAction < mDateNextAction)
        {
            mDateNextAction = energyComponent->DateNextAction;
            mEntityActing = &e;
        }
    }

    void EnergySystem::begin()
    {
        mDateNextAction = ((mCurrentDate + PeriodicEffectDate) / PeriodicEffectDate) * PeriodicEffectDate;
        mEntityActing = nullptr;
    }

    void EnergySystem::end()
    {
        mCurrentDate = mDateNextAction;
    }

    void EnergySystem::ResetEntityActing()
    {
        mEntityActing = nullptr;
    }

    artemis::Entity* EnergySystem::EntityActing() const
    {
        return mEntityActing;
    }

    int64_t EnergySystem::CurrentDate() const
    {
        return mCurrentDate;
    }
}
}