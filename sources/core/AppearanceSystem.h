#pragma once

#include "Artemis/EntityProcessingSystem.h"
#include "Artemis/ComponentMapper.h"

#include "AppearanceComponent.h"
#include "PositionComponent.h"
#include "dungeon.h"

namespace DotaRL
{
	namespace GameObject
	{
		class AppearanceSystem : public artemis::EntityProcessingSystem
		{
		public:
			AppearanceSystem();

			virtual void initialize();
			virtual void processEntity(artemis::Entity &e);
            bool& ResetTextures() { return mResetTextures; }
            void SetLayer(Layer::Type parLayer);
            void SetDungeon(Dungeon::Level* dungeonLvl) { mDungeonLvl = dungeonLvl; }
		private:
            artemis::ComponentMapper<AppearanceComponent> mAppearanceMapper;
            artemis::ComponentMapper<PositionComponent> mPositionMapper;
            bool mResetTextures;
            Layer::Type mLayer;
            Dungeon::Level* mDungeonLvl;
		};
	}
}
