#pragma once

#include "Artemis/Component.h"

namespace DotaRL
{
    namespace GameObject
    {
        class ControllableComponent : public artemis::Component
        {
        public:
            ControllableComponent();
        };
    }
}
