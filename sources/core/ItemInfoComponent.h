#pragma once

#include "Artemis/Component.h"
#include "ItemState.h"

namespace DotaRL
{
    namespace GameObject
    {
        class ItemInfoComponent : public artemis::Component
        {
        public:
            ItemInfoComponent(ItemState* itemState);
            ItemState* mItemState;
        };
    }
}
