#pragma once

#include "Artemis/Component.h"

namespace DotaRL
{
    namespace GameObject
    {
        class VisibilityComponent : public artemis::Component
        {
        public:
            VisibilityComponent(int visionRange);
            int mVisionRange;
        };
    }
}
