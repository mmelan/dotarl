#pragma once

#include "Artemis/Component.h"
#include "Drawable.h"
#include <string>

namespace DotaRL
{
    namespace GameObject
    {
        class AppearanceComponent : public artemis::Component
        {
        public:
            AppearanceComponent();
            Rendering::Drawable mAppearance_Ground;
            Rendering::Drawable mAppearance_Inventory;
            std::string mName;
            bool mDiesAtDestination;
        };
    }
}
