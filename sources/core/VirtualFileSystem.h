#pragma once

struct SDL_RWops;

namespace VirtualFileSystem
{
    void InitFileSystem(int argc, char *argv[]);
    void DeInitFileSystem();
    SDL_RWops* OpenFile(const char*  parFilePath);
}
