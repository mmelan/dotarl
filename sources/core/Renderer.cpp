#include "Renderer.h"

#include "SDL.h"
#include "SDL_render.h"
#include "SDL_image.h"
#include "SDL_Helper.h"
#include "AppearanceComponent.h"
#include "TagList.h"
#include "RenderConstantes.h"
#include "Artemis/Entity.h"
namespace DotaRL
{
    Renderer::Renderer()
        : mWindow(nullptr)
        , mRenderer(nullptr)
        , mCameraMoving(true)
        , mTeleport(true)
    {
        mCameraPosition.x = 0;
        mCameraPosition.y = 0;
    }

    Renderer::~Renderer()
    {
        SDL_DestroyWindow(mWindow);
        SDL_DestroyRenderer(mRenderer);
    }

    void Renderer::Initialise()
    {
        //SDL_Init(SDL_INIT_VIDEO);	
	
	    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");  // make the scaled rendering look smoother.

	    //Icone

	    //init ttf
	    ApplyVideoSettings();
    }
    
    struct VideoParameters
    {
	    bool FullScreen;
	    int WindowSizeX;
	    int WindowSizeY;
	    bool Vsync;
    };

    void Renderer::ApplyVideoSettings()
    {
	    VideoParameters videoParams;
	    videoParams.FullScreen = false;
	    videoParams.WindowSizeX = Window::LogicSizeX;
	    videoParams.WindowSizeY = Window::LogicSizeY;
	    videoParams.Vsync = true;

	    static bool LeaveMeAloneItsTheGameLaunching = true;
	    if(!LeaveMeAloneItsTheGameLaunching)
	    LeaveMeAloneItsTheGameLaunching = false;

	    if(mWindow)
		    SDL_DestroyWindow(mWindow);
	    if(mRenderer)
		    SDL_DestroyRenderer(mRenderer);
	    Uint32 windowflags = 0;
	    if(videoParams.FullScreen)
		    windowflags = SDL_WINDOW_FULLSCREEN_DESKTOP;
	    else
		    windowflags = SDL_WINDOW_RESIZABLE;
	    mWindow = SDL_CreateWindow("Rushan grows stronger!",
                            SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED,
                            videoParams.WindowSizeX, videoParams.WindowSizeY,
                            windowflags);
	    Uint32 rendererflags = 0;
	    if(videoParams.Vsync)
		    rendererflags = SDL_RENDERER_PRESENTVSYNC;
	    mRenderer = SDL_CreateRenderer(mWindow, -1, rendererflags);
	    SDL_RenderSetLogicalSize(mRenderer, Window::LogicSizeX, Window::LogicSizeY);
	    SDL_SetRenderDrawColor(mRenderer, 255, 0, 255, 255);
        SDL_SetWindowIcon(mWindow, SDL_LoadImage_VFS("data/icon.png"));
    }

    void Renderer::LoopStart()
    {
	    //Test de changement de resolution
	    mReinitAll = false;
	    /*const Uint8 *keystate = SDL_GetKeyboardState(NULL);
	    if(keystate[SDL_SCANCODE_ESCAPE])
	    {
		    ApplyVideoSettings();
		    mReinitAll = true;
	    }*/
	    //On efface l'ecran
	    SDL_SetRenderDrawColor(mRenderer, 0, 0, 0, 0);
	    SDL_RenderClear(mRenderer);
    }

    void Renderer::LoopStart(artemis::World& world, const Dungeon::Level& dungeonLevel)
    {
        LoopStart();

        //gestion de la camera
        artemis::TagManager* tagManager = world.getTagManager();
        if(tagManager->isSubscribed(TagList::PLayer))
        {
            artemis::Entity& cameraGuide = tagManager->getEntity(TagList::PLayer);
            GameObject::AppearanceComponent* appearanceComponent = (GameObject::AppearanceComponent*)cameraGuide.getComponent<GameObject::AppearanceComponent>();
            SDL_assert(appearanceComponent);
            float xDist = appearanceComponent->mAppearance_Ground.mPosX - mCameraPosition.x;
            float yDist = appearanceComponent->mAppearance_Ground.mPosY - mCameraPosition.y;
            float distanceToGuide = sqrt((xDist * xDist) + (yDist * yDist));
            if(!mCameraMoving)
            {
                if(distanceToGuide > Camera::MarginDist)
                {
                    mCameraMoving = true;
                }
            }
            if(mCameraMoving && distanceToGuide > 0)
            {
                float moveDistanceFact = 1.f;;
                if(!mTeleport)
                    moveDistanceFact = std::min(Camera::MaxSpeed, distanceToGuide) / distanceToGuide ;
                mCameraPosition.x += (int)xDist * moveDistanceFact;
                mCameraPosition.y += yDist * moveDistanceFact;
            }
            else
                mCameraMoving = false;
            mCameraPosition.x = std::max(Window::LogicSizeX / 2, mCameraPosition.x);
            mCameraPosition.y = std::max(Interface::StartY / 2, mCameraPosition.y);
            mCameraPosition.x = std::min(dungeonLevel.SizeX() * Dungeon::GetTileSizeX() - Window::LogicSizeX / 2, mCameraPosition.x);
            mCameraPosition.y = std::min(dungeonLevel.SizeY()* Dungeon::GetTileSizeY() - Interface::StartY / 2, mCameraPosition.y);
            mTeleport = false;
        }
    }

    void Renderer::Process()
    {
	    //on commande le dessin
	    SDL_RenderPresent(mRenderer);
    }

    void Renderer::Render(SDL_Texture * texture,
                SDL_Rect * srcrect,
                SDL_Rect * dstrect,
                bool cameraRelative)
    {
        SDL_assert(dstrect);
        SDL_Rect transformedDest = *dstrect;
        if(cameraRelative)
        {
            transformedDest.x += Window::LogicSizeX / 2 - mCameraPosition.x ;
            transformedDest.y += Window::LogicSizeY / 2 - mCameraPosition.y;
        }
        if( transformedDest.x > Window::LogicSizeX ||
            transformedDest.y > Window::LogicSizeY ||
            (transformedDest.x + transformedDest.w) < 0||
            (transformedDest.y + transformedDest.h) < 0 )
            return;

        SDL_RenderCopy(mRenderer, texture, srcrect, &transformedDest);
    }

}