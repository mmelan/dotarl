#include "Animation.h"

#include "SDL.h"
#include "SDL_Helper.h"

namespace DotaRL { 
    Animation::Animation()
        : mAnimationState(0)
        , mModified(false)
    {
    }

    void Animation::AddAnim(const char* path)
    {
        mAnimationList.push_back(SDL_LoadImage_VFS(path));
    }

    SDL_Surface* Animation::GetSurface()
    {
        SDL_assert(mAnimationList.size() > mAnimationState);
        return mAnimationList[mAnimationState];
    }

    bool Animation::Modified()
    {
        return mModified;
    }
}