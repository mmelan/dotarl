#include "ItemTemplate.h"
#include "SDL_Helper.h"
#include "SDL_assert.h"
#include "RandomGenerator.h"
#include "PositionComponent.h"
#include "EntityCreation.h"
#include "ItemState.h"

namespace DotaRL { namespace GameObject {

    std::vector<ItemTemplate> GetItemTable()
    {
        std::vector<ItemTemplate> outTable;
        {
            ItemTemplate itemTemplate;
            itemTemplate.AppearancePath_Ground = "data/items/Ring_of_Health_small.png";
            itemTemplate.Appearance_inventory = SDL_LoadImage_VFS("data/items/Ring_of_Health_big.png");
            itemTemplate.Name = "Ring of Health";
            itemTemplate.Rarity = 10;
            itemTemplate.Amount = 1;
            itemTemplate.Passive = BuffId::RingOfHealth;
            itemTemplate.ActiveSkill = SkillId::None;
            itemTemplate.Stackable = false;
            itemTemplate.Id = 1;
            outTable.push_back(itemTemplate);
        }
        {
            ItemTemplate itemTemplate;
            itemTemplate.AppearancePath_Ground = "data/items/Healing_Salve.png";
            itemTemplate.Appearance_inventory = SDL_LoadImage_VFS("data/items/Healing_Salve_icon.png");
            itemTemplate.Rarity = 20;
            itemTemplate.Amount = 10;
            itemTemplate.Name = "Healing Potion";
            itemTemplate.Passive = BuffId::Undefined;
            itemTemplate.ActiveSkill = SkillId::HealingSalve;
            itemTemplate.Stackable = true;
            itemTemplate.Id = 3;
            outTable.push_back(itemTemplate);
        }
        {
            ItemTemplate itemTemplate;
            itemTemplate.AppearancePath_Ground = "data/items/Clarity.png";
            itemTemplate.Appearance_inventory = SDL_LoadImage_VFS("data/items/Clarity_icon.png");
            itemTemplate.Name = "Clarity";
            itemTemplate.Rarity = 10;
            itemTemplate.Amount = 10;
            itemTemplate.Passive = BuffId::Undefined;
            itemTemplate.ActiveSkill = SkillId::Clarity;
            itemTemplate.Stackable = true;
            itemTemplate.Id = 4;
            outTable.push_back(itemTemplate);
        }
        {
            ItemTemplate itemTemplate;
            itemTemplate.AppearancePath_Ground = "data/items/Daedalus.png";
            itemTemplate.Appearance_inventory = SDL_LoadImage_VFS("data/items/Daedalus_icon.png");
            itemTemplate.Name = "Daedalus";
            itemTemplate.Rarity = 1;
            itemTemplate.Amount = 2;
            itemTemplate.Passive = BuffId::Daedalus;
            itemTemplate.ActiveSkill = SkillId::None;
            itemTemplate.Stackable = false;
            itemTemplate.Id = 5;
            outTable.push_back(itemTemplate);
        }
        {
            ItemTemplate itemTemplate;
            itemTemplate.AppearancePath_Ground = "data/items/Desolator.png";
            itemTemplate.Appearance_inventory = SDL_LoadImage_VFS("data/items/Desolator_icon.png");
            itemTemplate.Name = "Desolator";
            itemTemplate.Rarity = 1;
            itemTemplate.Amount = 1;
            itemTemplate.Passive = BuffId::Desolator;
            itemTemplate.ActiveSkill = SkillId::None;
            itemTemplate.Stackable = false;
            itemTemplate.Id = 6;
            outTable.push_back(itemTemplate);
        }
        {
            ItemTemplate itemTemplate;
            itemTemplate.AppearancePath_Ground = "data/items/Orchid_Malevolence.png";
            itemTemplate.Appearance_inventory = SDL_LoadImage_VFS("data/items/Orchid_Malevolence_icon.png");
            itemTemplate.Name = "Orchid Malevolence";
            itemTemplate.Rarity = 1;
            itemTemplate.Amount = 1;
            itemTemplate.Passive = BuffId::OrchidMalevolence;
            itemTemplate.ActiveSkill = SkillId::None;
            itemTemplate.Stackable = false;
            itemTemplate.Id = 7;
            outTable.push_back(itemTemplate);
        }
        {
            ItemTemplate itemTemplate;
            itemTemplate.AppearancePath_Ground = "data/items/Sange.png";
            itemTemplate.Appearance_inventory = SDL_LoadImage_VFS("data/items/Sange_icon.png");
            itemTemplate.Name = "Sange";
            itemTemplate.Rarity = 5;
            itemTemplate.Amount = 1;
            itemTemplate.Passive = BuffId::Sange;
            itemTemplate.ActiveSkill = SkillId::None;
            itemTemplate.Stackable = false;
            itemTemplate.Id = 9;
            outTable.push_back(itemTemplate);
        }
        {
            ItemTemplate itemTemplate;
            itemTemplate.AppearancePath_Ground = "data/items/Void_Stone.png";
            itemTemplate.Appearance_inventory = SDL_LoadImage_VFS("data/items/Void_Stone_icon.png");
            itemTemplate.Name = "Void Stone";
            itemTemplate.Rarity = 10;
            itemTemplate.Amount = 1;
            itemTemplate.Passive = BuffId::VoidStone;
            itemTemplate.ActiveSkill = SkillId::None;
            itemTemplate.Stackable = false;
            itemTemplate.Id = 11;
            outTable.push_back(itemTemplate);
        }
        {
            ItemTemplate itemTemplate;
            itemTemplate.AppearancePath_Ground = "data/items/Yasha.png";
            itemTemplate.Appearance_inventory = SDL_LoadImage_VFS("data/items/Yasha_icon.png");
            itemTemplate.Name = "Yasha";
            itemTemplate.Rarity = 5;
            itemTemplate.Amount = 1;
            itemTemplate.Passive = BuffId::Yasha;
            itemTemplate.ActiveSkill = SkillId::None;
            itemTemplate.Stackable = false;
            itemTemplate.Id = 12;
            outTable.push_back(itemTemplate);
        }
        {
            ItemTemplate itemTemplate;
            itemTemplate.AppearancePath_Ground = "data/items/Town_Portal_Scroll.png";
            itemTemplate.Appearance_inventory = SDL_LoadImage_VFS("data/items/Town_Portal_Scroll_icon.png");
            itemTemplate.Name = "Town Portal Scroll";
            itemTemplate.Rarity = 5;
            itemTemplate.Amount = 50;
            itemTemplate.Passive = BuffId::Undefined;
            itemTemplate.ActiveSkill = SkillId::TPScoll;
            itemTemplate.Stackable = false;
            itemTemplate.Id = 12;
            outTable.push_back(itemTemplate);
        }

        return outTable;
    }
    
    std::vector<ItemTemplate> GetCraftable()
    {
        std::vector<ItemTemplate> outTable;
        {
            ItemTemplate itemTemplate;
            itemTemplate.AppearancePath_Ground = "data/items/Perseverance.png";
            itemTemplate.Appearance_inventory = SDL_LoadImage_VFS("data/items/Perseverance_icon.png");
            itemTemplate.Name = "Perseverance";
            itemTemplate.Rarity = 1;
            itemTemplate.Passive = BuffId::Perseverance;
            itemTemplate.ActiveSkill = SkillId::None;
            itemTemplate.Stackable = false;
            itemTemplate.Id = 8;
            outTable.push_back(itemTemplate);
        }
        {
            ItemTemplate itemTemplate;
            itemTemplate.AppearancePath_Ground = "data/items/Sange_and_Yasha.png";
            itemTemplate.Appearance_inventory = SDL_LoadImage_VFS("data/items/Sange_and_Yasha_icon.png");
            itemTemplate.Name = "Sange and Yasha";
            itemTemplate.Rarity = 1;
            itemTemplate.Passive = BuffId::SangeYasha;
            itemTemplate.ActiveSkill = SkillId::None;
            itemTemplate.Stackable = false;
            itemTemplate.Id = 10;
            outTable.push_back(itemTemplate);
        }

        return outTable;
    }
    ItemState* Craft(int id)
    {
        static std::vector<ItemTemplate> itemTable = GetCraftable();
        GameObject::ItemState* useItemState = new GameObject::ItemState(itemTable[id]);
        if(itemTable[id].ActiveSkill != GameObject::SkillId::None)
        {
            useItemState->mSkill = GetItemSkillState(itemTable[id].ActiveSkill);
        }
        return useItemState;
    }

    const int DropChance = 50;
    void RandomlyDropLootOrNot(artemis::World& world,  artemis::Entity& target)
    {
        int diceRoll = Random::d100();
        if(diceRoll < DropChance)
        {
            static std::vector<ItemTemplate> itemTable = GetItemTable();
            if(!itemTable.empty())
            {
                int totalNbItems = 0;
                for(size_t i = 0; i < itemTable.size(); ++i)
                {
                    if(itemTable[i].Amount > 0)
                        totalNbItems += itemTable[i].Rarity;
                }
                int itemToDrop = Random::rand_int(totalNbItems);
                GameObject::PositionComponent* positionComponent = (GameObject::PositionComponent*)target.getComponent<GameObject::PositionComponent>();

                for(size_t i = 0; i < itemTable.size(); ++i)
                {
                    if(itemTable[i].Amount > 0)
                    {
                        if(itemToDrop < itemTable[i].Rarity)
                        {
                            DotaRL::CreateItemOnGround(world, positionComponent->mGridPosX, positionComponent->mGridPosY, positionComponent->mLevel, itemTable[i]);
                            itemTable[i].Amount--;
                            break;
                        }
                        else
                            itemToDrop -= itemTable[i].Rarity;
                    }
                }
            }
        }
    }
}
}
