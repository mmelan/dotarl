#include "FeedBackSystem.h"

#include "Artemis/Entity.h"
#include "EventInterpreter.h"
#include "SDL.h"
#include "Renderer.h"

namespace DotaRL { namespace GameObject
{
    const int HeatlBarHeigth = 5;
    FeedbackSystem::FeedbackSystem()
    {
        addComponentType<HealthManaComponent>();
        addComponentType<AppearanceComponent>();
        addComponentType<PositionComponent>();
        addComponentType<FactionComponent>();

        mlifeBarSurface_GoodGuys = SDL_CreateRGBSurface(0, 31, HeatlBarHeigth, 32, 0, 0, 0, 0);
        SDL_assert(mlifeBarSurface_GoodGuys);
        SDL_FillRect(mlifeBarSurface_GoodGuys, NULL, SDL_MapRGB(mlifeBarSurface_GoodGuys->format, 0, 220, 30));

        mlifeBarSurfaceEnemy_EvilForeigner = SDL_CreateRGBSurface(0, 31, HeatlBarHeigth, 32, 0, 0, 0, 0);
        SDL_assert(mlifeBarSurfaceEnemy_EvilForeigner);
        SDL_FillRect(mlifeBarSurfaceEnemy_EvilForeigner, NULL, SDL_MapRGB(mlifeBarSurfaceEnemy_EvilForeigner->format, 220, 0, 30));

        mlifeBarSurfaceBg = SDL_CreateRGBSurface(0, 31, HeatlBarHeigth, 32, 0, 0, 0, 0);
        SDL_assert(mlifeBarSurfaceBg);
        SDL_FillRect(mlifeBarSurfaceBg, NULL, SDL_MapRGB(mlifeBarSurfaceBg->format, 10, 10, 20));
    }

    void FeedbackSystem::initialize()
    {
        mAppearanceMapper.init(*world);
        mHealthManaMapper.init(*world);
        mPositionMapper.init(*world);
        mFactionMapper.init(*world);
    }

    void FeedbackSystem::processEntity(artemis::Entity &e)
    {
        HealthManaComponent* healthManaComponent = mHealthManaMapper.get(e);
        AppearanceComponent* appearanceComponent = mAppearanceMapper.get(e);
        PositionComponent* positionComponent = mPositionMapper.get(e);
        if(mDungeonLvl && mDungeonLvl->GetVisibility(positionComponent->mGridPosX, positionComponent->mGridPosY))
        {
            FactionComponent* faction = mFactionMapper.get(e);
            SDL_Renderer* sdlRenderer = DotaRL::Renderer::Instance().SDLRenderer();

            SDL_assert(sdlRenderer);
            SDL_Texture* texture = nullptr;
            switch(faction->mFaction)
            {
            case Faction::GoodGuys:
                texture = SDL_CreateTextureFromSurface(sdlRenderer, mlifeBarSurface_GoodGuys);
                break;
            case Faction::EvilForeigner:
                texture = SDL_CreateTextureFromSurface(sdlRenderer, mlifeBarSurfaceEnemy_EvilForeigner);
                break;
            }
            SDL_Texture* textureBg = SDL_CreateTextureFromSurface(sdlRenderer, mlifeBarSurfaceBg);

	        SDL_Rect position;
            position.x = (int)appearanceComponent->mAppearance_Ground.mPosX;
	        position.y = (int)appearanceComponent->mAppearance_Ground.mPosY - HeatlBarHeigth;
            position.w = mlifeBarSurfaceBg->w;
	        position.h = mlifeBarSurfaceBg->h;
            DotaRL::Renderer::Instance().Render(textureBg, nullptr, &position, true);

            position.w = (int)((float)position.w * (healthManaComponent->CurrentHealth / healthManaComponent->MaxHealth));
            DotaRL::Renderer::Instance().Render(texture, nullptr, &position, true);
            SDL_DestroyTexture(texture);
            SDL_DestroyTexture(textureBg);
        }
    }
}
}