#include "VirtualFileSystem.h"
#include "physfs.h"
#include "assert.h"
#include "stdlib.h"
#include "SDL.h"

namespace VirtualFileSystem
{
    void InitFileSystem(int argc, char *argv[])
    {
        {
            int err = PHYSFS_init(NULL);
            assert(err != 0);
        }
        bool assetsFound = false;
        if (argc > 1)
        {
            int err = PHYSFS_mount(argv[1], "data", 1); 	
            assetsFound |= err!=0;
        }
        else
        {
            {
                int err = PHYSFS_mount("data", "data", 1); 	
                assetsFound |= err!=0;
            }
            if (!assetsFound)
            {
                int err = PHYSFS_mount("../data", "data", 1); 	
                assetsFound |= err!=0;
            }
            if (!assetsFound)
            {
                int err = PHYSFS_mount("data.zip", "data", 1); 	
                assetsFound |= err!=0;
            }
            if (!assetsFound)
            {
                int err = PHYSFS_mount("data.pack", "data", 1); 	
                assetsFound |= err!=0;
            }
        }
        if(!assetsFound)
        {
            SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Unable to open ressource data folder", PHYSFS_getLastError(), nullptr);
            exit(-1);
        }
    }

    void DeInitFileSystem()
    {
        PHYSFS_deinit();
    }

    SDL_RWops* OpenFile(const char* parFilePath)
    {
        if (!PHYSFS_exists(parFilePath))
            __debugbreak();
        PHYSFS_File* myfile = PHYSFS_openRead(parFilePath);
        if (myfile == nullptr)
        {
            SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error opening ressource file", PHYSFS_getLastError(), nullptr);
            return nullptr;
        }
        PHYSFS_sint64 file_size_64 = PHYSFS_fileLength(myfile);
        SDL_assert(file_size_64 > 0);
        SDL_assert(file_size_64 < INT_MAX);
        size_t file_size = static_cast<size_t>(file_size_64);
        char *myBuf = new char[file_size];
        PHYSFS_sint64 length_read = PHYSFS_read(myfile, myBuf, 1, file_size);
        if (file_size != length_read)
        {
            SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error reading ressource file", PHYSFS_getLastError(), nullptr);
            return nullptr;
        }
        SDL_RWops* output = SDL_RWFromMem(myBuf, file_size);
        return output;
    }
}