#include "MobTemplate.h"

namespace DotaRL{

std::vector<MobTemplate> GetMobTable()
{
    std::vector<MobTemplate> outTable;
    {
        MobTemplate mobTemplate;
        mobTemplate.mAppearancePath = "data/creatures/Axe_icon.png";
        mobTemplate.mIsHero = true;
        mobTemplate.mBaseHealthRegen = range_f(2.f, 3.f);
        mobTemplate.mBaseManaRegen = range_f(0.01f, 0.1f);
        mobTemplate.mMaxHealth = range_f(800, 1000);
        mobTemplate.mMaxMana = range_f(100, 250);
        mobTemplate.mMoveSpeed = range_f(300, 350);
        mobTemplate.mMinDamage = range_f(50,80);
        mobTemplate.mDamageRange = range_f(20,50);
        mobTemplate.mArmor = range_f(1, 3);
        mobTemplate.mXpGain = range_f(100, 150);
        mobTemplate.mVisibility = 5;
        mobTemplate.mNumberToSpawn = range_i(1);
        mobTemplate.mBaseLevel = 10;
        mobTemplate.mRange = 0;
        mobTemplate.mName = "Axe";
        mobTemplate.mFaction = GameObject::Faction::EvilForeigner;
        mobTemplate.mPossibleActions.push_back(AIActions::attack);

        outTable.push_back(mobTemplate);
    }
    {
        MobTemplate mobTemplate;
        mobTemplate.mAppearancePath = "data/creatures/Bloodseeker_icon.png";
        mobTemplate.mIsHero = true;
        mobTemplate.mBaseHealthRegen = range_f(2.f, 3.f);
        mobTemplate.mBaseManaRegen = range_f(0.01f, 0.1f);
        mobTemplate.mMaxHealth = range_f(800, 1000);
        mobTemplate.mMaxMana = range_f(100, 250);
        mobTemplate.mMoveSpeed = range_f(300, 350);
        mobTemplate.mMinDamage = range_f(50,80);
        mobTemplate.mDamageRange = range_f(20,50);
        mobTemplate.mArmor = range_f(1, 3);
        mobTemplate.mXpGain = range_f(100, 150);
        mobTemplate.mVisibility = 5;
        mobTemplate.mNumberToSpawn = range_i(1);
        mobTemplate.mBaseLevel = 10;
        mobTemplate.mRange = 0;
        mobTemplate.mName = "Bloodseeker";
        mobTemplate.mFaction = GameObject::Faction::EvilForeigner;
        mobTemplate.mPossibleActions.push_back(AIActions::attack);
        outTable.push_back(mobTemplate);
    }
    {
        MobTemplate mobTemplate;
        mobTemplate.mAppearancePath = "data/creatures/Doom_icon.png";
        mobTemplate.mIsHero = true;
        mobTemplate.mBaseHealthRegen = range_f(2.f, 3.f);
        mobTemplate.mBaseManaRegen = range_f(0.01f, 0.1f);
        mobTemplate.mMaxHealth = range_f(800, 1000);
        mobTemplate.mMaxMana = range_f(100, 250);
        mobTemplate.mMoveSpeed = range_f(300, 350);
        mobTemplate.mMinDamage = range_f(50,80);
        mobTemplate.mDamageRange = range_f(20,50);
        mobTemplate.mArmor = range_f(1, 3);
        mobTemplate.mXpGain = range_f(100, 150);
        mobTemplate.mVisibility = 5;
        mobTemplate.mNumberToSpawn = range_i(1);
        mobTemplate.mBaseLevel = 10;
        mobTemplate.mName = "Doom";
        mobTemplate.mRange = 0;
        mobTemplate.mFaction = GameObject::Faction::EvilForeigner;
        mobTemplate.mPossibleActions.push_back(AIActions::attack);
        outTable.push_back(mobTemplate);
    }
    {
        MobTemplate mobTemplate;
        mobTemplate.mAppearancePath = "data/creatures/Meepo_icon.png";
        mobTemplate.mIsHero = true;
        mobTemplate.mBaseHealthRegen = range_f(2.f, 3.f);
        mobTemplate.mBaseManaRegen = range_f(0.01f, 0.1f);
        mobTemplate.mMaxHealth = range_f(800, 1000);
        mobTemplate.mMaxMana = range_f(100, 250);
        mobTemplate.mMoveSpeed = range_f(300, 350);
        mobTemplate.mMinDamage = range_f(50,80);
        mobTemplate.mDamageRange = range_f(20,50);
        mobTemplate.mArmor = range_f(1, 3);
        mobTemplate.mXpGain = range_f(100, 150);
        mobTemplate.mVisibility = 5;
        mobTemplate.mNumberToSpawn = range_i(1, 4);
        mobTemplate.mBaseLevel = 10;
        mobTemplate.mRange = 0;
        mobTemplate.mName = "Meepo";
        mobTemplate.mFaction = GameObject::Faction::EvilForeigner;
        mobTemplate.mPossibleActions.push_back(AIActions::attack);
        outTable.push_back(mobTemplate);
    }
    {
        MobTemplate mobTemplate;
        mobTemplate.mAppearancePath = "data/creatures/Melee_creeps.png";
        mobTemplate.mIsHero = false;
        mobTemplate.mBaseHealthRegen = range_f(2.f, 3.f);
        mobTemplate.mBaseManaRegen = range_f(0, 0);
        mobTemplate.mMaxHealth = range_f(300, 300);
        mobTemplate.mMaxMana = range_f(0, 0);
        mobTemplate.mMoveSpeed = range_f(300, 300);
        mobTemplate.mMinDamage = range_f(40,40);
        mobTemplate.mDamageRange = range_f(20,20);
        mobTemplate.mArmor = range_f(1, 3);
        mobTemplate.mXpGain = range_f(100, 150);
        mobTemplate.mVisibility = 5;
        mobTemplate.mNumberToSpawn = range_i(1);
        mobTemplate.mBaseLevel = 3;
        mobTemplate.mRange = 0;
        mobTemplate.mName = "Melee Creep";
        mobTemplate.mFaction = GameObject::Faction::EvilForeigner;
        mobTemplate.mPossibleActions.push_back(AIActions::attack);
        outTable.push_back(mobTemplate);
    }
    {
        MobTemplate mobTemplate;
        mobTemplate.mAppearancePath = "data/creatures/huskar_petit.png";
        mobTemplate.mIsHero = false;
        mobTemplate.mBaseHealthRegen = range_f(2.f, 3.f);
        mobTemplate.mBaseManaRegen = range_f(0, 0);
        mobTemplate.mMaxHealth = range_f(300, 300);
        mobTemplate.mMaxMana = range_f(0, 0);
        mobTemplate.mMoveSpeed = range_f(300, 300);
        mobTemplate.mMinDamage = range_f(40,40);
        mobTemplate.mDamageRange = range_f(20,20);
        mobTemplate.mArmor = range_f(1, 3);
        mobTemplate.mXpGain = range_f(100, 150);
        mobTemplate.mVisibility = 6;
        mobTemplate.mNumberToSpawn = range_i(1);
        mobTemplate.mBaseLevel = 3;
        mobTemplate.mRange = 4;
        mobTemplate.mName = "Huskar the ranged";
        mobTemplate.mFaction = GameObject::Faction::EvilForeigner;
        mobTemplate.mPossibleActions.push_back(AIActions::attack);
        outTable.push_back(mobTemplate);
    }

    return outTable;
}

MobTemplate GetRoshanTemplate()
{
    MobTemplate mobTemplate;
    mobTemplate.mAppearancePath = "data/creatures/rushan_petit.png";
    mobTemplate.mIsHero = true;
    mobTemplate.mBaseHealthRegen = range_f(10.f, 10.f);
    mobTemplate.mBaseManaRegen = range_f(0.f, 0.f);
    mobTemplate.mMaxHealth = range_f(2000.f, 2000.f);
    mobTemplate.mMaxMana = range_f(0.f, 0.f);
    mobTemplate.mMoveSpeed = range_f(400, 400);
    mobTemplate.mMinDamage = range_f(100.f,100.f);
    mobTemplate.mDamageRange = range_f(100.f,100.f);
    mobTemplate.mArmor = range_f(5.f, 5.f);
    mobTemplate.mXpGain = range_f(1000.f, 1000.f);
    mobTemplate.mVisibility = 5;
    mobTemplate.mNumberToSpawn = range_i(1);
    mobTemplate.mBaseLevel = 10;
    mobTemplate.mName = "Rushan";
    mobTemplate.mFaction = GameObject::Faction::EvilForeigner;
    mobTemplate.mPossibleActions.push_back(AIActions::attack);
    return mobTemplate;
}
MobTemplate GetFountain()
{
    MobTemplate mobTemplate;
    mobTemplate.mAppearancePath = "data/creatures/Fountain.png";
    mobTemplate.mIsHero = false;
    mobTemplate.mVisibility = 3;
    mobTemplate.mNumberToSpawn = range_i(1);
    mobTemplate.mBaseLevel = 1;
    mobTemplate.mName = "Fountain";
    mobTemplate.mFaction = GameObject::Faction::EvilForeigner;
    mobTemplate.mPossibleActions.push_back(AIActions::attack);
    return mobTemplate;
}


}