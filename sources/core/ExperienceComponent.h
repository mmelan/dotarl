#pragma once

#include <cstdint>

#include "Artemis/Component.h"

namespace DotaRL { namespace GameObject
{
    class ExperienceComponent : public artemis::Component
    {
    public:
        ExperienceComponent();

        int mExperience;
    };
}
}
