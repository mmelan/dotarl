#pragma once

#include "SDL_rect.h"

namespace DotaRL { namespace Random {
    int rand_range(int low, int high);
    int rand_int(int high);
    int d100();
    int d6();
}
}
