#pragma once

#include "Artemis/EntityProcessingSystem.h"
#include "Artemis/ComponentMapper.h"

#include "PositionComponent.h"
#include "AppearanceComponent.h"

namespace DotaRL
{
    namespace GameObject
    {
        class VisualMovementSystem : public artemis::EntityProcessingSystem
        {
        public:
            VisualMovementSystem();

            virtual void initialize();
            virtual void processEntity(artemis::Entity &e);

            bool MovementInProgress();
        private:
            artemis::ComponentMapper<PositionComponent> mPositionMapper;
            artemis::ComponentMapper<AppearanceComponent> mAppearanceMapper;

            bool mMovementInProgress;
        };
    }
}
