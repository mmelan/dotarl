#include "PositionSystem.h"

#include "Artemis/Entity.h"
#include "PositionComponent.h"
#include "dungeon.h"

namespace DotaRL { namespace GameObject
{
    PositionSystem::PositionSystem(Dungeon::Level* level) :
        EntityProcessingSystem(),
        mLevel(level)
    {
        addComponentType<GameObject::PositionComponent>();
    }

    void PositionSystem::initialize()
    {
        mPositionMapper.init(*world);
    }

    void PositionSystem::processEntity(artemis::Entity &e)
    {
        auto position = mPositionMapper.get(e);
    }

    void PositionSystem::removed(artemis::Entity &e)
    {
        auto position = mPositionMapper.get(e);
        switch (position->mLayer)
        {
        case PositionLayer::Creature:
            mLevel->RemoveCreatureAt(e.getId(), position->mGridPosX, position->mGridPosY);
            break;
        case PositionLayer::Item:
            mLevel->RemoveItemAt(e.getId(), position->mGridPosX, position->mGridPosY);
            break;
        default:
            break;
        }
    }

    void PositionSystem::added(artemis::Entity &e)
    {
        auto position = mPositionMapper.get(e);
    }
}
}