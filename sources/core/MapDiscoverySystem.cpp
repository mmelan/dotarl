#include "MapDiscoverySystem.h"

#include "Artemis/Entity.h"
#include "EventInterpreter.h"
#include "Renderer.h"
#include "RenderConstantes.h"
#include "SDL.h"

#include "fov.h"

namespace DotaRL { namespace GameObject
{
    //heavy copier coller
    struct point { double x, y; };

    double CCW(point a, point b, point c)
    {
        return (b.x-a.x)*(c.y-a.y) - (b.y-a.y)*(c.x-a.x);
    }

    struct line { point s, e; };

    double middle(double a, double b, double c) {
      double t;    
      if ( a > b ) {
        t = a;
        a = b;
        b = t;
      }
      if ( a <= c && c <= b ) return 1;
      return 0;
    }

    int intersect(line a, line b) {
      if ( ( CCW(a.s, a.e, b.s) * CCW(a.s, a.e, b.e) < 0 ) &&
         ( CCW(b.s, b.e, a.s) * CCW(b.s, b.e, a.e) < 0 ) ) return 1;

      if ( CCW(a.s, a.e, b.s) == 0 && middle(a.s.x, a.e.x, b.s.x) && middle(a.s.y, a.e.y, b.s.y) ) return 1;
      if ( CCW(a.s, a.e, b.e) == 0 && middle(a.s.x, a.e.x, b.e.x) && middle(a.s.y, a.e.y, b.e.y) ) return 1;
      if ( CCW(b.s, b.e, a.s) == 0 && middle(b.s.x, b.e.x, a.s.x) && middle(b.s.y, b.e.y, a.s.y) ) return 1;
      if ( CCW(b.s, b.e, a.e) == 0 && middle(b.s.x, b.e.x, a.e.x) && middle(b.s.y, b.e.y, a.e.y) ) return 1;

        return 0;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    static bool BlocksVision(Dungeon::Level* level, int x, int y)
    {
        auto f = level->GetFeature(x, y, Dungeon::Layers::Collisionnable);
        switch (f)
        {
        case Dungeon::Feature::Nothing:
        case Dungeon::Feature::Fountain:
        case Dungeon::Feature::Mountain:
        case Dungeon::Feature::DireGrass:
        case Dungeon::Feature::RadiantGrass:
            return false;
        default:
            return true;
        }
    }

    static bool TestMapOpacity(void *map, int x, int y)
    {
        auto level = static_cast<Dungeon::Level*>(map);
        return BlocksVision(level, x, y);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    static void MarkTileAsVisible(void *map, int x, int y, int dx, int dy, void *src)
    {
        auto level = static_cast<Dungeon::Level*>(map);
        level->SetVisibility(x, y);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    MapDiscoverySystem::MapDiscoverySystem()
    {
        addComponentType<MapDiscoveryComponent>();
        addComponentType<PositionComponent>();
        addComponentType<VisibilityComponent>();

        fov_settings_init(&mFovSettings);
        fov_settings_set_shape(&mFovSettings, FOV_SHAPE_CIRCLE_PRECALCULATE);
        fov_settings_set_opaque_apply(&mFovSettings, FOV_OPAQUE_APPLY);
        fov_settings_set_opacity_test_function(&mFovSettings, TestMapOpacity);
        fov_settings_set_apply_lighting_function(&mFovSettings, MarkTileAsVisible);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    MapDiscoverySystem::~MapDiscoverySystem()
    {
        fov_settings_free(&mFovSettings);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    void MapDiscoverySystem::initialize()
    {
        mMapDiscoveryMapper.init(*world);
        mPositionMapper.init(*world);
        mVisibilityMapper.init(*world);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    bool intersectWithObstacle(line ray, std::vector<std::pair<int, int> >& obstacles)
    {
        for(size_t k = 0; k < obstacles.size(); ++k)
        {
            {
                point obstacle1={obstacles[k].first,obstacles[k].second}, obstacle2={obstacles[k].first,obstacles[k].second};
                obstacle2.x += 1;
                obstacle2.y += 1;
                line lineObstacle={obstacle1,obstacle2};
                if( intersect(ray, lineObstacle))
                {
                    return true;
                    break;
                }
            }
            {
                point obstacle1={obstacles[k].first,obstacles[k].second}, obstacle2={obstacles[k].first,obstacles[k].second};
                obstacle1.x += 1;
                obstacle2.y += 1;
                line lineObstacle={obstacle1,obstacle2};
                if( intersect(ray, lineObstacle))
                {
                    return true;
                    break;
                }
            }
        }
        return false;
    }
    bool isTileIdentifiable(int x, int y, point& source, std::vector<std::pair<int, int> >& obstacles)
    {
        for(double srcitX = 0.001; srcitX < 1; srcitX += 0.998) 
        {
            for(double srcitY = 0.001; srcitY < 1; srcitY += 0.998) 
            {
                point dest={x + srcitX, y + srcitY};
                line line1={source,dest};
                if( !intersectWithObstacle(line1, obstacles) )
                    return true;
            }
        }
        return false;
    }

    void MapDiscoverySystem::processEntity(artemis::Entity &e)
    {
        PositionComponent* positionComponent = mPositionMapper.get(e);
        VisibilityComponent* visibilityComponent = mVisibilityMapper.get(e);
        DotaRL::Renderer& renderer = DotaRL::Renderer::Instance();
        SDL_Point campPos = renderer.CameraPosition();
        if((std::abs(positionComponent->mGridPosX-campPos.x/Dungeon::GetTileSizeX()) > (Window::LogicSizeX/2+visibilityComponent->mVisionRange)) ||
            (std::abs(positionComponent->mGridPosY-campPos.y/Dungeon::GetTileSizeY()) > (Window::LogicSizeX/2+visibilityComponent->mVisionRange)) )
            return;

        SDL_assert(mDungeonLvl);
        mDungeonLvl->SetVisibility(positionComponent->mGridPosX, positionComponent->mGridPosY);
        fov_circle(&mFovSettings, mDungeonLvl, nullptr, positionComponent->mGridPosX, positionComponent->mGridPosY, visibilityComponent->mVisionRange);
    }
}
}