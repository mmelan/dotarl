#pragma once

struct SDL_Surface;
#include <vector>

namespace DotaRL { 
    class Animation
    {
    public:
        Animation();
        void operator ++()
        {
            if(mAnimationList.size() <= 1)
                return;
            mAnimationState++;
            if(mAnimationState >= (int)mAnimationList.size())
                mAnimationState = 0;
            mModified = true;
        }
        void AddAnim(const char* path);
        SDL_Surface* GetSurface();
        bool Modified();
    private:
        std::vector<SDL_Surface*> mAnimationList;
        int mAnimationState;
        bool mModified;
    };
}
