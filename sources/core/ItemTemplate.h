#pragma once

#include <string>
#include <vector>
#include "Artemis/Entity.h"
#include "MobTemplate.h"
#include "Buffs.h"
#include "SkillDescription.h"

struct SDL_Surface;
namespace DotaRL { namespace GameObject {
    struct ItemState;
    struct ItemTemplate
    {
        int Id;
        float CoolDown;
        int ManaCost;
        int Rarity;
        int Amount;
        bool Stackable;
        std::string AppearancePath_Ground;
        SDL_Surface* Appearance_inventory;
        BuffId Passive;
        BuffId Active;
        std::string Name;
        SkillId ActiveSkill;
    };

    std::vector<ItemTemplate> GetItemTable();
    ItemState* Craft(int id);
    void RandomlyDropLootOrNot(artemis::World& world,  artemis::Entity& target);
}
}
