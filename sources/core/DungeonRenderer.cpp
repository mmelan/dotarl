#include "DungeonRenderer.h"

#include "SDL_assert.h"
#include "SDL_Helper.h"
#include "SDL.h"
#include "Renderer.h"
#include "RenderConstantes.h"

namespace DotaRL { namespace Dungeon
{
    LevelRenderer::LevelRenderer()
    {
        mTileTypeToSurface[Feature::HighMountain].AddAnim("data/tiles/MountainDark.png");
        mTileTypeToSurface[Feature::Mountain].AddAnim("data/tiles/MountainDark.png");
        mTileTypeToSurface[Feature::RadiantTree].AddAnim("data/tiles/TreeRadiant.png");
        mTileTypeToSurface[Feature::DireTree].AddAnim("data/tiles/TreeDire.png");
        mTileTypeToSurface[Feature::RadiantGrass].AddAnim("data/tiles/GrassLight.png");
        mTileTypeToSurface[Feature::DireGrass].AddAnim("data/tiles/GrassDire.png");
        mTileTypeToSurface[Feature::Water].AddAnim("data/tiles/Water1.png");
        mTileTypeToSurface[Feature::Water].AddAnim("data/tiles/Water2.png");
        mTileTypeToSurface[Feature::Water].AddAnim("data/tiles/Water3.png");
        mTileTypeToSurface[Feature::Fountain].AddAnim("data/tiles/Fountain.png");

        mTileTypeToSurfaceFog[Feature::HighMountain].AddAnim("data/tiles/MountainDark_F.png");
        mTileTypeToSurfaceFog[Feature::Mountain].AddAnim("data/tiles/MountainDark_F.png");
        mTileTypeToSurfaceFog[Feature::RadiantTree].AddAnim("data/tiles/TreeRadiant_F.png");
        mTileTypeToSurfaceFog[Feature::DireTree].AddAnim("data/tiles/TreeDire_F.png");
        mTileTypeToSurfaceFog[Feature::RadiantGrass].AddAnim("data/tiles/GrassLight_F.png");
        mTileTypeToSurfaceFog[Feature::DireGrass].AddAnim("data/tiles/GrassDire_F.png");
        mTileTypeToSurfaceFog[Feature::Water].AddAnim("data/tiles/Water1_F.png");
        mTileTypeToSurfaceFog[Feature::Water].AddAnim("data/tiles/Water2_F.png");
        mTileTypeToSurfaceFog[Feature::Water].AddAnim("data/tiles/Water3_F.png");
        mTileTypeToSurfaceFog[Feature::Fountain].AddAnim("data/tiles/Fountain_F.png");
    }

    void LevelRenderer::ReinitTextures(const Level& parLevel)
    {
        dungeonSizeX = parLevel.SizeX();
        mAnimationTimer.resize(dungeonSizeX * parLevel.SizeY());
        SDL_Renderer* sdlRenderer = DotaRL::Renderer::Instance().SDLRenderer();
        for(int i = 1; i < (int)Feature::Lenght; ++i)
        {
            if(mTileTypeToTexture[(Feature)i])
                SDL_DestroyTexture(mTileTypeToTexture[(Feature)i]);
            mTileTypeToTexture[(Feature)i] = SDL_CreateTextureFromSurface(sdlRenderer, mTileTypeToSurface[(Feature)i].GetSurface());
                SDL_DestroyTexture(mTileTypeToTextureFog[(Feature)i]);
            mTileTypeToTextureFog[(Feature)i] = SDL_CreateTextureFromSurface(sdlRenderer, mTileTypeToSurfaceFog[(Feature)i].GetSurface());
        }
    }

    void LevelRenderer::UpdateAnim()
    {
        static int i = 0;
        ++i;
        if(i%40 == 0)
        {
            SDL_Renderer* sdlRenderer = DotaRL::Renderer::Instance().SDLRenderer();
            for(int i = 1; i < (int)Feature::Lenght; ++i)
            {
                ++mTileTypeToSurface[(Feature)i];
                if(mTileTypeToSurface[(Feature)i].Modified())
                {
                    if(mTileTypeToTexture[(Feature)i])
                        SDL_DestroyTexture(mTileTypeToTexture[(Feature)i]);
                    mTileTypeToTexture[(Feature)i] = SDL_CreateTextureFromSurface(sdlRenderer, mTileTypeToSurface[(Feature)i].GetSurface());
                }
                ++mTileTypeToSurfaceFog[(Feature)i];
                if(mTileTypeToSurfaceFog[(Feature)i].Modified())
                {
                    if(mTileTypeToTextureFog[(Feature)i])
                        SDL_DestroyTexture(mTileTypeToTextureFog[(Feature)i]);
                    mTileTypeToTextureFog[(Feature)i] = SDL_CreateTextureFromSurface(sdlRenderer, mTileTypeToSurfaceFog[(Feature)i].GetSurface());
                }
            }
        }
    }

    void LevelRenderer::Animate(int x, int y, int amplitude)
    {
        mAnimationTimer[y*dungeonSizeX+x] = amplitude;
    }


    void LevelRenderer::Render(const Level& parLevel)
    {
        UpdateAnim();

	    SDL_Rect position;
        DotaRL::Renderer& renderer = DotaRL::Renderer::Instance();
        SDL_Point cameraPosition = renderer.CameraPosition();
        cameraPosition.x /= Dungeon::GetTileSizeX();
        cameraPosition.y /= Dungeon::GetTileSizeY();
        int nbTileInScreenX = Window::LogicSizeX / Dungeon::GetTileSizeX();
        int nbTileInScreenY = Window::LogicSizeY / Dungeon::GetTileSizeY();

        int startItX = cameraPosition.x - nbTileInScreenX/2 - 1;
        int startItY = cameraPosition.y - nbTileInScreenY/2 - 1;

        int endItX = cameraPosition.x + nbTileInScreenX/2 + 1;
        int endItY = cameraPosition.y + nbTileInScreenY/2 + 1;
        for(int i = startItX; i < endItX; ++i)
        {
            for(int j = startItY; j < endItY; ++j)
            {
                if(!parLevel.IsInMap(i, j))
                    continue;
                for(int layer = 0; layer < (int)Layers::Lenght; ++layer)
                {
                    Feature feature = parLevel.GetLastKnownFeature(i, j, (Layers)layer);
                    if(feature == Feature::Nothing)
                        continue;
                    position.x = Dungeon::GetTileSizeX() * i; //centr� sur la case
	                position.y = Dungeon::GetTileSizeY() * j - (mAnimationTimer[j*parLevel.SizeX()+i]);
                    (mAnimationTimer[j*parLevel.SizeX()+i]) = std::max((mAnimationTimer[j*parLevel.SizeX()+i]-1), 0);
                    position.w = Dungeon::GetTileSizeX();
	                position.h = Dungeon::GetTileSizeY();
                    if(parLevel.GetVisibility(i, j))
                        renderer.Render(mTileTypeToTexture[feature], nullptr, &position, true);
                    else
                        renderer.Render(mTileTypeToTextureFog[feature], nullptr, &position, true);
                }
            }
        }
    }
}}
