#include "SDL_Helper.h"

#include "SDL_Image.h"
#include "SDL_Mixer.h"
#include "VirtualFileSystem.h"

SDL_Surface* SDL_LoadImage_VFS(const char* parPath)
{
	SDL_RWops* file = VirtualFileSystem::OpenFile(parPath);
	return IMG_Load_RW(file, 1);
}

Mix_Chunk* SDL_LoadWAV_VFS(const char* parPath)
{
	SDL_RWops* file = VirtualFileSystem::OpenFile(parPath);
	return Mix_LoadWAV_RW(file, 1);
}
