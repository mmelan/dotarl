#include "PlayerInterface.h"

#include "SDL.h"
#include "SDL_assert.h"
#include "Renderer.h"
#include "VirtualFileSystem.h"
#include "RenderConstantes.h"
#include "TagList.h"
#include "StatsComponent.h"
#include "InventoryComponent.h"
#include "ExperienceComponent.h"
#include "ItemTemplate.h"
#include "ItemState.h"
#include "SDL_Helper.h"
#include "Artemis/Entity.h"
#include <string>

namespace DotaRL
{
    PlayerInterfaceRenderer::PlayerInterfaceRenderer()
    {
        SDL_RWops* file = VirtualFileSystem::OpenFile("data/font/segoepr.ttf");
        mPolice = TTF_OpenFontRW(file, 1, 10);
        if(!mPolice) {
            char error[255];
            sprintf_s(error, "TTF_OpenFontRW: %s\n", TTF_GetError());
            __debugbreak();
        }

        DotaRL::Renderer& renderer = DotaRL::Renderer::Instance();
        {
            SDL_Surface* s = SDL_LoadImage_VFS("data/target_selection.png");
            mSelectionFeedback = SDL_CreateTextureFromSurface(renderer.SDLRenderer(), s);
            mSelectionFeedbackArea = SDL_CreateTextureFromSurface(renderer.SDLRenderer(), s);
            SDL_SetTextureAlphaMod(mSelectionFeedbackArea, 220);
            SDL_SetTextureBlendMode(mSelectionFeedbackArea, SDL_BlendMode::SDL_BLENDMODE_ADD);
            SDL_FreeSurface(s);
        }
        {
            SDL_Surface* s = SDL_LoadImage_VFS("data/target_selection_error.png");
            mSelectionFeedbackError = SDL_CreateTextureFromSurface(renderer.SDLRenderer(), s);
            SDL_FreeSurface(s);
        }
    }

    PlayerInterfaceRenderer::~PlayerInterfaceRenderer()
    {
        SDL_DestroyTexture(mSelectionFeedback);
        SDL_DestroyTexture(mSelectionFeedbackError);
        SDL_DestroyTexture(mSelectionFeedbackArea);
    }

    void DrawRect(int x, int y, int w, int h, int r, int g, int b, int a)
    {
        SDL_Surface* s = SDL_CreateRGBSurface(0, w, h, 32, 0, 0, 0, 0);
        if(!s)
        {
            char error[255];
            sprintf_s(error, "Sdl errCode: %s\n", SDL_GetError());
            SDL_assert(false);
        }
        SDL_FillRect(s, NULL, SDL_MapRGBA(s->format, r, g, b, a));

        SDL_Rect position;
        position.x = x;
        position.y = y;
        position.w = w;
        position.h = h;
        DotaRL::Renderer& renderer = DotaRL::Renderer::Instance();
        SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer.SDLRenderer(), s);
        SDL_SetTextureBlendMode( texture, SDL_BLENDMODE_BLEND );
        SDL_SetTextureAlphaMod( texture, a );
        renderer.Render(texture, nullptr, &position, false);
        SDL_FreeSurface(s);
        SDL_DestroyTexture(texture);
    }

    void DrawText(const char* text, int x, int y, int w, int h, int r, int g, int b, int a, TTF_Font* police)
    {
        SDL_Color color;
        color.r = r;
        color.g = g;
        color.b = b;
        color.a = a;
        SDL_Surface* surface = TTF_RenderText_Blended(police, text, color);
        DotaRL::Renderer& renderer = DotaRL::Renderer::Instance();
        SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer.SDLRenderer(), surface); 
        SDL_Rect position;
        position.x = x;
        position.y = y;
        position.w = surface->w;
        position.h = surface->h;
        renderer.Render(texture, nullptr, &position, false);
        SDL_FreeSurface(surface);
        SDL_DestroyTexture(texture);
    }

    void PlayerInterfaceRenderer::DrawLifeBar(const PlayerInterfaceData& playerData)
    {
        if(playerData.mMaxLifePoints > 0 && playerData.mLifePoints > 0)
        {
            DrawRect(Window::LogicSizeX / 4, Window::LogicSizeY - 42, Window::LogicSizeX * playerData.mLifePoints / playerData.mMaxLifePoints / 2, 20, 255, 0, 0, 255);
        }
        char text[20];
        sprintf_s(text, "%d / %d", playerData.mLifePoints, playerData.mMaxLifePoints);
        DrawText(text, Window::LogicSizeX / 2, Window::LogicSizeY - 42, 100, 20, 0, 0, 0, 255, mPolice);
    }

    void PlayerInterfaceRenderer::DrawManaBar(const PlayerInterfaceData& playerData)
    {
        if(playerData.mMaxManaPoints > 0 && playerData.mManaPoints > 0)
        {
            DrawRect(Window::LogicSizeX / 4, Window::LogicSizeY - 20, Window::LogicSizeX * playerData.mManaPoints / playerData.mMaxManaPoints / 2, 20, 0, 0, 255, 255);
        }
        char text[20];
        sprintf_s(text, "%d / %d", playerData.mManaPoints, playerData.mMaxManaPoints);
        DrawText(text, Window::LogicSizeX / 2, Window::LogicSizeY - 20, 100, 20, 0, 0, 0, 255, mPolice);
    }

    void PlayerInterfaceRenderer::DrawLevel(const PlayerInterfaceData& playerData)
    {
        char text[50];
        if(playerData.skillUnlearn > 0)
            sprintf_s(text, "Level %d : %d point left (ctrl+number to learn)", playerData.mLevel, playerData.skillUnlearn);
        else
            sprintf_s(text, "Level %d", playerData.mLevel);
        DrawText(text, 20, Window::LogicSizeY - 85, 100, 20, 0, 0, 0, 255, mPolice);
    }

    void PlayerInterfaceRenderer::DrawStrStat(const PlayerInterfaceData& playerData)
    {
        const char* stat_pattern = "%d  %s";
        const char* main_stat_pattern = "%d [%s]";        
        char text[50];
        if (playerData.mMainStat == MainStat::Str)
            sprintf_s(text, main_stat_pattern, playerData.mStrength, "Str");
        else
            sprintf_s(text, stat_pattern, playerData.mStrength, "Str");

        DrawText(text, 20, Window::LogicSizeY - 65, 100, 20, 0, 0, 0, 255, mPolice);
    }

    void PlayerInterfaceRenderer::DrawAgiStat(const PlayerInterfaceData& playerData)
    {
        const char* stat_pattern = "%d  %s";
        const char* main_stat_pattern = "%d [%s]";        
        char text[50];
        if (playerData.mMainStat == MainStat::Agi)
            sprintf_s(text, main_stat_pattern, playerData.mAgility, "Agi");
        else
            sprintf_s(text, stat_pattern, playerData.mAgility, "Agi");
        DrawText(text, 20, Window::LogicSizeY - 45, 100, 20, 0, 0, 0, 255, mPolice);
    }

    void PlayerInterfaceRenderer::DrawIntStat(const PlayerInterfaceData& playerData)
    {
        const char* stat_pattern = "%d  %s";
        const char* main_stat_pattern = "%d [%s]";        
        char text[50];
        if (playerData.mMainStat == MainStat::Int)
            sprintf_s(text, main_stat_pattern, playerData.mIntelligence, "Int");
        else
            sprintf_s(text, stat_pattern, playerData.mIntelligence, "Int");
        DrawText(text, 20, Window::LogicSizeY - 25, 100, 20, 0, 0, 0, 255, mPolice);
    }

    void PlayerInterfaceRenderer::DrawInventory(const PlayerInterfaceData& playerData)
    {
        const int inventoryBoxStartX = Window::LogicSizeX * 3 / 4;
        const int inventoryMargin = 4;
        const int inventoryBoxWidth = Window::LogicSizeX / 4;
        const int itemSizeY = (inventoryBoxWidth - inventoryMargin*2)/6;
        const int itemSizeX = itemSizeY*1.34; //ration des icones, cherche pas
        const int inventoryBoxHeight = itemSizeY*2+inventoryMargin*2+1;
        const int inventoryBoxStartY = Window::LogicSizeY - inventoryBoxHeight;
        //DrawRect(inventoryBoxStartX, inventoryBoxStartY, inventoryBoxWidth, inventoryBoxHeight, 100, 100, 100, 255);
        DotaRL::Renderer& renderer = DotaRL::Renderer::Instance();
        for(size_t i = 0; i < 6; ++i)
        {
            int boxStartX = inventoryBoxStartX+inventoryMargin+(i%3)*itemSizeX+(i%3);
            int boxStartY = inventoryBoxStartY+inventoryMargin+(i/3)*itemSizeY+(i/3);
            DrawRect(boxStartX, boxStartY, itemSizeX, itemSizeY, 150, 150, 150, 255);
            if(playerData.mItemList.size() > i)
            {
                SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer.SDLRenderer(), playerData.mItemList[i].mSurface);
                SDL_Rect position;
                position.x = boxStartX+2;
                position.y = boxStartY+2;
                position.w = itemSizeX-4;
                position.h = itemSizeY-4;
                renderer.Render(texture, nullptr, &position, false);
                SDL_DestroyTexture(texture);
                if(playerData.mItemList[i].mCooldown > 0)
                {
                    {
                        float ratio = playerData.mItemList[i].mCooldown / playerData.mItemList[i].mMaxCoolDown;
                        DrawRect(boxStartX+2, boxStartY+2 + (1-ratio)*(itemSizeY-4), itemSizeX-4, (itemSizeY-4)*ratio, 110, 110, 110, 100);
                        char text[4];
                        sprintf_s(text, "%d", playerData.mItemList[i].mCooldown);
                        DrawText(text, boxStartX+5, boxStartY+5, itemSizeX-4, itemSizeY-4, 255, 255, 255, 255, mPolice);
                        DrawText(text, boxStartX+4, boxStartY+4, itemSizeX-4, itemSizeY-4, 0, 0, 0, 255, mPolice);
                    }
                }
                if(playerData.mItemList[i].mCharges >= 0)
                {
                    char text[4];
                    sprintf_s(text, "%d", playerData.mItemList[i].mCharges);
                    DrawText(text, boxStartX+4, boxStartY+itemSizeY-20, 20, 20, 200, 200, 200, 255, mPolice);
                }
                {
                    char text[4];
                    switch(i)
                    {
                    case 0:
                        sprintf_s(text, SDL_GetKeyName(SDL_GetKeyFromScancode(SDL_SCANCODE_Q)));
                        break;
                    case 1:
                        sprintf_s(text, SDL_GetKeyName(SDL_GetKeyFromScancode(SDL_SCANCODE_W)));
                        break;
                    case 2:
                        sprintf_s(text, SDL_GetKeyName(SDL_GetKeyFromScancode(SDL_SCANCODE_E)));
                        break;
                    case 3:
                        sprintf_s(text, SDL_GetKeyName(SDL_GetKeyFromScancode(SDL_SCANCODE_R)));
                        break;
                    case 4:
                        sprintf_s(text, SDL_GetKeyName(SDL_GetKeyFromScancode(SDL_SCANCODE_T)));
                        break;
                    case 5:
                        sprintf_s(text, SDL_GetKeyName(SDL_GetKeyFromScancode(SDL_SCANCODE_Y)));
                        break;
                    }
                    int shortcutsize = 12;
                    DrawRect(boxStartX+itemSizeX - shortcutsize-2, boxStartY+2, shortcutsize, shortcutsize, 50, 50, 50, 255);
                    DrawText(text, boxStartX+itemSizeX - shortcutsize, boxStartY-2, shortcutsize, shortcutsize, 255, 255, 255, 255, mPolice);
                }
            }
            else
                DrawRect(boxStartX+2, boxStartY+2, itemSizeX-4, itemSizeY-4, 110, 110, 110, 255);
        }
    }

    void PlayerInterfaceRenderer::DrawSkills(const PlayerInterfaceData& playerData, int64_t currentDate)
    {
        const int itemSizeY = 41;
        const int itemSizeX = 54;
        const int inventoryBoxStartY = Interface::StartY;
        const int inventoryMargin = 4;
        const int inventoryBoxStartX = Window::LogicSizeX / 4;
        DotaRL::Renderer& renderer = DotaRL::Renderer::Instance();
        for(size_t i = 0; i < 4; ++i)
        {
            int boxStartX = inventoryBoxStartX+inventoryMargin+i*itemSizeX+i;
            int boxStartY = inventoryBoxStartY+inventoryMargin;
            DrawRect(boxStartX, boxStartY, itemSizeX, itemSizeY, 150, 150, 150, 255);
            if(playerData.mSkillList.size() > i)
            {
                SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer.SDLRenderer(), playerData.mSkillList[i]->mSkillDescription->mAppearance);
                SDL_Rect position;
                position.x = boxStartX+2;
                position.y = boxStartY+2;
                position.w = itemSizeX-4;
                position.h = itemSizeY-4;
                if(playerData.mSkillList[i]->mSkillLevel == 0)
                {
                    SDL_SetTextureBlendMode( texture, SDL_BLENDMODE_BLEND );
                    SDL_SetTextureAlphaMod( texture, 50 );
                }
                renderer.Render(texture, nullptr, &position, false);
                SDL_DestroyTexture(texture);

                auto remaining_time = playerData.mSkillList[i]->mDateEndCooldown - currentDate;
                if(remaining_time > 0)
                {
                    float ratio = static_cast<float>(remaining_time) / playerData.mSkillList[i]->mSkillDescription->mCooldown[playerData.mSkillList[i]->mSkillLevel];
                    DrawRect(boxStartX+2, boxStartY+2 + (1-ratio)*(itemSizeY-4), itemSizeX-4, (itemSizeY-4)*ratio, 200, 200, 200, 100);
                    char text[4];
                    sprintf_s(text, "%d", static_cast<int>(remaining_time / 1000));
                    DrawText(text, boxStartX+itemSizeX/2-8, boxStartY+itemSizeY/2-8, 16, 16, 255, 255, 255, 255, mPolice);
                }
                {
                    char text[4];
                    sprintf_s(text, "%d", i+1);
                    int shortcutsize = 12;
                    DrawRect(boxStartX+itemSizeX - shortcutsize-2, boxStartY+2, shortcutsize, shortcutsize, 50, 50, 50, 255);
                    DrawText(text, boxStartX+itemSizeX - shortcutsize, boxStartY-2, shortcutsize, shortcutsize, 255, 255, 255, 255, mPolice);
                }
                for(int j = 0; j < playerData.mSkillList[i]->mSkillLevel; ++j)
                {
                    int lvlBoxSize = 4;
                    int lvlBoxMargin = 2;
                    DrawRect(lvlBoxMargin+boxStartX+j*(lvlBoxSize+lvlBoxMargin)-1, boxStartY+itemSizeY-lvlBoxSize-1, lvlBoxSize+2, lvlBoxSize+2, 110, 110, 110, 255);
                    char text[50];
                    sprintf_s(text, "%d", static_cast<int>(remaining_time / 1000));
                    DrawRect(lvlBoxMargin+boxStartX+j*(lvlBoxSize+lvlBoxMargin), boxStartY+itemSizeY-lvlBoxSize, lvlBoxSize, lvlBoxSize, 200, 200, 0, 255);
                }
            }
            else
            {
                DrawRect(boxStartX+2, boxStartY+2, itemSizeX-4, itemSizeY-4, 110, 110, 110, 255);
            }

        }
    }

    void PlayerInterfaceRenderer::Process(const PlayerInterfaceData& playerData, int64_t currentDate)
    {
        DrawRect(0, Interface::StartY, Window::LogicSizeX, Window::LogicSizeX-Interface::StartY, 100, 100, 100, 255);
        DrawRect(4, Interface::StartY+4, Window::LogicSizeX/4-8, Window::LogicSizeY-Interface::StartY-8, 150, 150, 150, 255);

        DrawLifeBar(playerData);
        DrawManaBar(playerData);
        DrawLevel(playerData);
        DrawStrStat(playerData);
        DrawAgiStat(playerData);
        DrawIntStat(playerData);
        DrawInventory(playerData);
        DrawSkills(playerData, currentDate);
    }

    void PlayerInterfaceRenderer::RenderTargetting(ReticleType typeReticle, int x, int y)
    {
        DotaRL::Renderer& renderer = DotaRL::Renderer::Instance();
        SDL_Rect position;
        position.x = x * Dungeon::GetTileSizeX();
        position.y = y * Dungeon::GetTileSizeY();
        position.w = Dungeon::GetTileSizeX();
        position.h = Dungeon::GetTileSizeY();
        switch (typeReticle)
        {
        case ReticleType::Valid:
            renderer.Render(mSelectionFeedback, nullptr, &position, true);
            break;
        case ReticleType::Invalid:
            renderer.Render(mSelectionFeedbackError, nullptr, &position, true);
            break;
        case ReticleType::Area:
            renderer.Render(mSelectionFeedbackArea, nullptr, &position, true);
            break;
        }
    }


    PlayerInterfaceController::PlayerInterfaceController()
    {
    }

    PlayerInterfaceController::~PlayerInterfaceController()
    {
    }
    
    void PlayerInterfaceController::Initialise(artemis::World& world)
    {
        mHealthManaMapper.init(world);
        mCoreStatsMapper.init(world);
        mInventoryMapper.init(world);
        mSkillSetMapper.init(world);
        mCombatStatsMapper.init(world);
    }

    void PlayerInterfaceController::Process(PlayerInterfaceData& playerData, artemis::World& world)
    {
        artemis::TagManager* tagManager = world.getTagManager();
        if(tagManager->isSubscribed(TagList::PLayer))
        {
            artemis::Entity& playerEntity = tagManager->getEntity(TagList::PLayer);
            auto healthManaComponent = mHealthManaMapper.get(playerEntity);
            SDL_assert(healthManaComponent);
            playerData.mLifePoints = (int)healthManaComponent->CurrentHealth;
            playerData.mMaxLifePoints = (int)healthManaComponent->MaxHealth;
            playerData.mManaPoints = (int)healthManaComponent->CurrentMana;
            playerData.mMaxManaPoints = (int)healthManaComponent->MaxMana;

            auto coreStatsComponent = mCoreStatsMapper.get(playerEntity);
            playerData.mMainStat = (MainStat)coreStatsComponent->MainStat; // crade ! mais marche
            playerData.mStrength = coreStatsComponent->Str;
            playerData.mAgility = coreStatsComponent->Agi;
            playerData.mIntelligence = coreStatsComponent->Int;

            auto inventory = mInventoryMapper.get(playerEntity);
            SDL_assert(inventory);
            playerData.mItemList.clear();
            for(size_t i = 0; i < inventory->mOwnedItemList.size(); ++i)
            {
                playerData.mItemList.push_back(itemInfo(inventory->mOwnedItemList[i]->mCooldown, 
                                                        inventory->mOwnedItemList[i]->mItemTemplate.CoolDown,
                                                        inventory->mOwnedItemList[i]->mItemTemplate.Appearance_inventory,
                                                        inventory->mOwnedItemList[i]->mItemTemplate.Stackable?inventory->mOwnedItemList[i]->mCharges:-1));
            }

            playerData.mSkillList.clear();
            auto skillSet = mSkillSetMapper.get(playerEntity);
            for(size_t i = 0; i < skillSet->mSkillSet.size(); ++i)
                playerData.mSkillList.push_back(&skillSet->mSkillSet[i]);

            auto combatStats = mCombatStatsMapper.get(playerEntity);
            playerData.mLevel = combatStats->Level;

            
            int skillLearned = 0;
            for(size_t i = 0; i < skillSet->mSkillSet.size(); ++i)
                skillLearned += skillSet->mSkillSet[i].mSkillLevel;

            playerData.skillUnlearn = combatStats->Level - skillLearned;
        }
    }
}