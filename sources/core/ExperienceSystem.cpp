#include "ExperienceSystem.h"
#include "Artemis/Entity.h"
#include "EventInterpreter.h"
#include "SDL_assert.h"
#include "TagList.h"
#include "Console.h"

namespace DotaRL { namespace GameObject
{
    ExperienceSystem::ExperienceSystem()
    {
        addComponentType<ExperienceComponent>();
        addComponentType<CombatStatsComponent>();
    }

    void ExperienceSystem::initialize()
    {
        mExperienceMapper.init(*world);
        mCombatStatsMapper.init(*world);
    }

    void ExperienceSystem::processEntity(artemis::Entity &e)
    {
        ExperienceComponent* experienceComponent = mExperienceMapper.get(e);
        CombatStatsComponent* coreStatsComponent = mCombatStatsMapper.get(e);
        int lastLevel = coreStatsComponent->Level;
        coreStatsComponent->Level = std::min(25,experienceComponent->mExperience / 100+1);
        
        auto tagManager = world->getTagManager();
        if(lastLevel != coreStatsComponent->Level && tagManager->getEntity(TagList::PLayer).getId() == e.getId())
        {
            char text[50];
            sprintf_s(text, " Level up ! %d -> %d", lastLevel, coreStatsComponent->Level);
            AddLog(text, LogSource::Gain);
        }
    }
}
}