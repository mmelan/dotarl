#pragma once

namespace DotaRL { 
namespace Dungeon {
    inline int GetTileSizeX() { return 32; }
    inline int GetTileSizeY() { return 32; }
}

namespace Layer
{
    enum Type
    {
        Items = 0,
        Mob = 1,
        Length
    };
}

namespace Camera{
    static const float MarginDist = 100;
    static const float MaxSpeed = 10;
}

namespace Window{
    static const int LogicSizeX = 1024;
    static const int LogicSizeY = 768;
}
namespace Interface
{
    static int StartY = 677;
}

}