#pragma once

#include <map>
#include <vector>
#include "dungeon.h"
#include "Animation.h"

struct SDL_Texture;
struct SDL_Surface;

namespace DotaRL { namespace Dungeon {
    class LevelRenderer
    {
    public:
        LevelRenderer();
        void ReinitTextures(const Level& parLevel);
        void Render(const Level& parLevel);

        void Animate(int x, int y, int amplitude);

    private:
        void UpdateAnim();
        std::map<Dungeon::Feature, Animation> mTileTypeToSurface;
        std::map<Dungeon::Feature, Animation> mTileTypeToSurfaceFog;
        std::map<Dungeon::Feature, SDL_Texture*> mTileTypeToTexture;
        std::map<Dungeon::Feature, SDL_Texture*> mTileTypeToTextureFog;
        std::vector<int> mAnimationTimer;
        int dungeonSizeX;
    };
}}
