#pragma once

#include "Artemis/Component.h"
#include "Drawable.h"

namespace DotaRL
{
    namespace GameObject
    {
        enum class Faction
        {
            GoodGuys,
            EvilForeigner,
            JustLeaveUsAlone
        };
        class FactionComponent : public artemis::Component
        {
        public:
            FactionComponent(Faction faction);
            Faction mFaction;
        };
    }
}
