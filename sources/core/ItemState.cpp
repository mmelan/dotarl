#include "ItemState.h"
#include "SkillSetComponent.h"
#include <map>

namespace DotaRL { namespace GameObject
{
    ItemState::ItemState(ItemTemplate itemTemplate)
        : mItemTemplate(itemTemplate)
        , mCooldown(0)
        , mCharges(1)
        , mSkill(nullptr)
    {
    }

    ItemState::~ItemState()
    {
    }

    std::map<SkillId, SkillState*> SkillToState;
    SkillState* GetItemSkillState(SkillId skillId)
    {
        std::map<SkillId, SkillState*>::iterator it = SkillToState.find(skillId);
        if(it == SkillToState.end())
        {
            SkillState* result = new SkillState(GameObject::GetSkillDescription(skillId));
            result->mSkillLevel = 1;
            SkillToState.emplace(skillId, result);
            return result;
        }
        return it->second;
    }

}}