#include "FactionComponent.h"

namespace DotaRL
{
    namespace GameObject
    {
        FactionComponent::FactionComponent(Faction faction)
            : mFaction(faction)
        {
        };
    }
}