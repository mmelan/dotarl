#include "ControleSystem.h"

#include "Artemis/Entity.h"
#include "EventInterpreter.h"
#include "DamageRules.h"
#include "ItemInfoComponent.h"
#include "EntityCreation.h"
#include "EnergySystem.h"
#include "StatsComponent.h"
#include "EntityCreation.h"
#include "SDL.h"
#include "TagList.h"
#include "Buffs.h"
#include "SkillBehavior.h"
#include "Console.h"
#include "PlayerInterface.h"

namespace DotaRL { namespace GameObject
{
    namespace {
        int64_t GetMoveCost(float moveSpeed)
        {
            return static_cast<int64_t>(300.f / moveSpeed * 1000.f);
        }

        int64_t GetAttackCost(float baseAttackTime, float IAS)
        {
            auto attackTime = baseAttackTime / (1.f + IAS/100.f);
            return static_cast<int64_t>(attackTime*1000.f);
        }

        int64_t GetWaitCost()
        {
            return 500;
        }
    }

    ControleSystem::ControleSystem(EnergySystem* energySystem)
        : mEntityPlaying(-1)
        , mEnergySystem(energySystem)
        , mTargetSelecting(false)
        , targetSelectionX(-1)
        , targetSelectionY(-1)
    {
        addComponentType<ControllableComponent>();
        addComponentType<PositionComponent>();
        addComponentType<EnergyComponent>();
        addComponentType<VisibilityComponent>();
        addComponentType<FactionComponent>();
        addComponentType<InventoryComponent>();
        addComponentType<CombatStatsComponent>();
        addComponentType<SkillSetComponent>();
        addComponentType<HealthManaComponent>();
    }

    void ControleSystem::initialize()
    {
        mControllableMapper.init(*world);
        mPositionMapper.init(*world);
        mEnergyMapper.init(*world);
        mVisibitilyMapper.init(*world);
        mFactionMapper.init(*world);
        mInventoryMapper.init(*world);
        mBuffListMapper.init(*world);
        mCombatStatsMapper.init(*world);
        mSkillSetMapper.init(*world);
        mIteminfoMapper.init(*world);
        mManaHealthMapper.init(*world);
    }

    bool ControleSystem::RestIfn(artemis::Entity &e)
    {
        HealthManaComponent* manaHealthComponent = mManaHealthMapper.get(e);
        if(manaHealthComponent->Resting)
        {
            if( manaHealthComponent->CurrentHealth >= manaHealthComponent->MaxHealth ||
                manaHealthComponent->LastHealth > manaHealthComponent->CurrentHealth)
                manaHealthComponent->Resting = false;
            else
            {
                Wait(e);
                return true;
            }
        }
        return false;
    }
        
    void ControleSystem::TryRest(artemis::Entity &e)
    {
        HealthManaComponent* manaHealthComponent = mManaHealthMapper.get(e);
        if(manaHealthComponent->LastHealth > manaHealthComponent->CurrentHealth)
            return;
        if(manaHealthComponent->CurrentHealth >= manaHealthComponent->MaxHealth)
            return;
        manaHealthComponent->Resting = true;
    }

    void ControleSystem::TryDrop(artemis::Entity &e, int index)
    {
        {
            InventoryComponent* inventoryComponent = mInventoryMapper.get(e);
            if((int)inventoryComponent->mOwnedItemList.size() > index)
            {
                ItemState* itemState = inventoryComponent->mOwnedItemList[index];
                inventoryComponent->mOwnedItemList.erase(inventoryComponent->mOwnedItemList.begin()+index);
                PositionComponent* positionComponent = mPositionMapper.get(e);
                CreateItemOnGround(*world, positionComponent->mGridPosX, positionComponent->mGridPosY, positionComponent->mLevel, itemState->mItemTemplate, itemState);
                {
                    char text[50];
                    sprintf_s(text, "Dropped : %s", itemState->mItemTemplate.Name.c_str());
                    AddLog(text, LogSource::Gain);
                }
                auto buffList = mBuffListMapper.get(e);
                for (auto b : *buffList)
                {
                    if (b->Creator == e.getId() && b->GetId() == itemState->mItemTemplate.Passive)
                    {
                        world->getEntity(b->EntityId).remove();
                    }
                }
                EnergyComponent* energyComponent = mEnergyMapper.get(e);
                energyComponent->DateNextAction += 1;
            }
        }
    }

    void ControleSystem::TryUseItem(artemis::Entity &e, int index)
    {
        InventoryComponent* inventoryComponent = mInventoryMapper.get(e);
        if((int)inventoryComponent->mOwnedItemList.size() > index)
        {
            ItemState* itemState = inventoryComponent->mOwnedItemList[index];
            if(itemState->mSkill)
            {
                if(TryLaunchSkill(*world, e, *itemState->mSkill, 0, 0, mEnergySystem->CurrentDate()))
                {
                    itemState->mCharges--;
                    if(itemState->mCharges <= 0)
                    {
                        inventoryComponent->mOwnedItemList.erase( inventoryComponent->mOwnedItemList.begin() + index);
                        delete itemState;
                    }
                    EnergyComponent* energyComponent = mEnergyMapper.get(e);
                    energyComponent->DateNextAction += 1;
                }
            }
        }
    }

    void ControleSystem::TryPickUp(artemis::Entity &e)
    {
        PositionComponent* positionComponent = mPositionMapper.get(e);
        int itemEntityId = positionComponent->mLevel->GetItem(positionComponent->mGridPosX, positionComponent->mGridPosY);
        if(itemEntityId != -1)
        {
            InventoryComponent* inventoryComponent = mInventoryMapper.get(e);
            {
                auto& item = world->getEntity(itemEntityId);
                ItemInfoComponent* itemInfoComponent = mIteminfoMapper.get(item);
                ItemState* itemState = itemInfoComponent->mItemState;
                bool mustDelete = false;
                if(itemState->mItemTemplate.Id == 11) //void stone
                {
                    for(int i = 0; i < inventoryComponent->mOwnedItemList.size(); ++i)
                    {
                        if(inventoryComponent->mOwnedItemList[i]->mItemTemplate.Id == 1) //ring of heath
                        {
                            //craft
                            delete inventoryComponent->mOwnedItemList[i];
                            inventoryComponent->mOwnedItemList.erase(inventoryComponent->mOwnedItemList.begin() + i );
                            mustDelete = true;
                            itemState = Craft(0);
                            break;
                        }
                    }
                }
                if(itemState->mItemTemplate.Id == 1) //ring of heath
                {
                    for(int i = 0; i < inventoryComponent->mOwnedItemList.size(); ++i)
                    {
                        if(inventoryComponent->mOwnedItemList[i]->mItemTemplate.Id == 11)//void stone
                        {
                            //craft
                            delete inventoryComponent->mOwnedItemList[i];
                            inventoryComponent->mOwnedItemList.erase(inventoryComponent->mOwnedItemList.begin() + i );
                            mustDelete = true;
                            itemState = Craft(0);
                            break;
                        }
                    }
                }
                if(itemState->mItemTemplate.Id == 12) //yasha
                {
                    for(int i = 0; i < inventoryComponent->mOwnedItemList.size(); ++i)
                    {
                        if(inventoryComponent->mOwnedItemList[i]->mItemTemplate.Id == 9) //sange
                        {
                            //craft
                            delete inventoryComponent->mOwnedItemList[i];
                            inventoryComponent->mOwnedItemList.erase(inventoryComponent->mOwnedItemList.begin() + i );
                            mustDelete = true;
                            itemState = Craft(1);
                            break;
                        }
                    }
                }
                if(itemState->mItemTemplate.Id == 9) //yasha
                {
                    for(int i = 0; i < inventoryComponent->mOwnedItemList.size(); ++i)
                    {
                        if(inventoryComponent->mOwnedItemList[i]->mItemTemplate.Id == 12) //sange
                        {
                            //craft
                            delete inventoryComponent->mOwnedItemList[i];
                            inventoryComponent->mOwnedItemList.erase(inventoryComponent->mOwnedItemList.begin() + i );
                            mustDelete = true;
                            itemState = Craft(1);
                            break;
                        }
                    }
                }
                bool stacked = false;
                if(itemState->mItemTemplate.Stackable)
                {
                    for(int i = 0; i < inventoryComponent->mOwnedItemList.size(); ++i)
                    {
                        if(inventoryComponent->mOwnedItemList[i]->mItemTemplate.Id == itemState->mItemTemplate.Id)
                        {
                            inventoryComponent->mOwnedItemList[i]->mCharges++;
                            stacked = true;
                            mustDelete = true;
                        }
                    }
                }
                if(inventoryComponent->mOwnedItemList.size() >= 6 && !mustDelete)
                    return;

                if(!stacked)
                    inventoryComponent->mOwnedItemList.push_back(itemState);
                {
                    char text[50];
                    sprintf_s(text, "Looted : %s", itemState->mItemTemplate.Name.c_str());
                    AddLog(text, LogSource::Gain);
                }
                auto passiveId = itemState->mItemTemplate.Passive;
                if (passiveId != BuffId::Undefined)
                {
                    auto buffEntity = AddBuffToEntity(*world, e, e, passiveId, 1, mEnergySystem->CurrentDate());
                }
                if(mustDelete)
                    delete itemInfoComponent->mItemState;

                item.remove();
                EnergyComponent* energyComponent = mEnergyMapper.get(e);
                energyComponent->DateNextAction += 1;
            }
        }
    }

    void ControleSystem::TryLevelUp(artemis::Entity &e, int index)
    {

        SkillSetComponent* skillComponent = mSkillSetMapper.get(e);
        int skillLearned = 0;
        for(size_t i = 0; i < skillComponent->mSkillSet.size(); ++i)
            skillLearned += skillComponent->mSkillSet[i].mSkillLevel;

        CombatStatsComponent* combatStatsComponent = mCombatStatsMapper.get(e);
        int skillPointLeft = combatStatsComponent->Level - skillLearned;
        if(skillPointLeft > 0 && skillComponent->mSkillSet.size() > index)
        {
            int skillLevel = ++skillComponent->mSkillSet[index].mSkillLevel;
            auto passiveId = skillComponent->mSkillSet[index].mSkillDescription->mPassiveBuff;
            if(passiveId != BuffId::Undefined)
            {
                auto buffEntity = AddBuffToEntity(*world, e, e, passiveId, skillLevel, mEnergySystem->CurrentDate());
            }
            EnergyComponent* energyComponent = mEnergyMapper.get(e);
            energyComponent->DateNextAction += 1;
        }
    }

    void ControleSystem::TryUseSkill(artemis::Entity &e, int index)
    {
        SkillSetComponent* skillComponent = mSkillSetMapper.get(e);
        if(skillComponent->mSkillSet.size() <= index)
            return;
        auto skill = skillComponent->mSkillSet[index];
        if (skill.mSkillLevel <= 0)
            return;
        switch (skill.mSkillDescription->Shape)
        {
        case SkillShape::Instant:
        case SkillShape::PBAoE:
            TryLaunchSkill(*world, e, skillComponent->mSkillSet[index], 0, 0, mEnergySystem->CurrentDate());
            break;
        case SkillShape::Ray:
        case SkillShape::Target:
        case SkillShape::GroundTarget:
            if (mTargetSelecting && mCurrentSkillTargeting == index)
            {
                if (mValidTargetPoint)
                    TryLaunchSkill(*world, e, skillComponent->mSkillSet[index], targetSelectionX, targetSelectionY, mEnergySystem->CurrentDate());
                else
                    AddLog("Invalid target", LogSource::Info);
            }
            else
            {
                mCurrentSkillTargeting = index;
                InitTargetSelection(e, skill.mSkillDescription->Range[skill.mSkillLevel], skill.mSkillDescription->Shape);
            }
            break;
        }
    }

    bool ControleSystem::TryAttack(artemis::Entity &e, int x, int y)
    {
        CombatStatsComponent* combatStatsComponent = mCombatStatsMapper.get(e);
        PositionComponent* positionComponent = mPositionMapper.get(e);
        auto targetId = positionComponent->mLevel->GetCreature(x, y);
        EnergyComponent* energyComponent = mEnergyMapper.get(e);
        if (targetId != -1)
        {
            auto& target = world->getEntity(targetId);
            FactionComponent* targetFaction = mFactionMapper.get(target);
            FactionComponent* faction = mFactionMapper.get(e);

            if(targetFaction->mFaction != faction->mFaction)
            {
                ApplyNormalAttack(*world, e, target, mEnergySystem->CurrentDate());
                energyComponent->DateNextAction += GetAttackCost(combatStatsComponent->BaseAttackTime, combatStatsComponent->IAS);
                CreateProjectile(*world, positionComponent->mGridPosX, positionComponent->mGridPosY, x, y, positionComponent->mLevel, "data/skills/ranged_veno_attack.png");
            }
            else
                return false;

        }
        return false;
    }

    bool ControleSystem::TryMoveInto(artemis::Entity &e, int dx, int dy)
    {
        PositionComponent* positionComponent = mPositionMapper.get(e);
        CombatStatsComponent* combatStatsComponent = mCombatStatsMapper.get(e);
        int x = positionComponent->mGridPosX + dx;
        int y = positionComponent->mGridPosY + dy;
        if (positionComponent->mLevel->IsMovableTile(x, y, Dungeon::EntityLayer::None))
        {
            auto targetId = positionComponent->mLevel->GetCreature(x, y);
            EnergyComponent* energyComponent = mEnergyMapper.get(e);
            if (targetId == -1) // personne, on move
            {
                positionComponent->mLevel->RemoveCreatureAt(e.getId(), positionComponent->mGridPosX, positionComponent->mGridPosY);
                positionComponent->mGridPosX = x;
                positionComponent->mGridPosY = y;
                positionComponent->mLevel->PlaceCreatureAt(e.getId(), x, y);
                energyComponent->DateNextAction += GetMoveCost(combatStatsComponent->MoveSpeed);
            }
            else // un enemy ! potentiel
            {
                SDL_assert(targetId != e.getId());
                auto& target = world->getEntity(targetId);
                FactionComponent* targetFaction = mFactionMapper.get(target);
                FactionComponent* faction = mFactionMapper.get(e);

                if(targetFaction->mFaction != faction->mFaction)
                {
                    ApplyNormalAttack(*world, e, target, mEnergySystem->CurrentDate());
                    energyComponent->DateNextAction += GetAttackCost(combatStatsComponent->BaseAttackTime, combatStatsComponent->IAS);
                    positionComponent->AttackedTile.x = x;
                    positionComponent->AttackedTile.y = y;
                }
                else if(faction->mFaction != Faction::EvilForeigner)
                {
                    PositionComponent* targetPositionComponent = mPositionMapper.get(target);
                    positionComponent->mLevel->RemoveCreatureAt(e.getId(), positionComponent->mGridPosX, positionComponent->mGridPosY);
                    positionComponent->mLevel->RemoveCreatureAt(target.getId(), targetPositionComponent->mGridPosX, targetPositionComponent->mGridPosY);
                    targetPositionComponent->mGridPosX = positionComponent->mGridPosX;
                    targetPositionComponent->mGridPosY = positionComponent->mGridPosY;
                    positionComponent->mGridPosX = x;
                    positionComponent->mGridPosY = y;
                    targetPositionComponent->mLevel->PlaceCreatureAt(e.getId(), positionComponent->mGridPosX, positionComponent->mGridPosY);
                    targetPositionComponent->mLevel->PlaceCreatureAt(target.getId(), targetPositionComponent->mGridPosX, targetPositionComponent->mGridPosY);
                }
                else
                    return false;
            }
            return true;
        }
        return false;
    }

    void ControleSystem::Wait(artemis::Entity &e)
    {
        EnergyComponent* energyComponent = mEnergyMapper.get(e);

        energyComponent->DateNextAction += GetWaitCost();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    bool ControleSystem::ProcessEntity(artemis::Entity &e)
    {
        EnergyComponent* energyComponent = mEnergyMapper.get(e);
        auto dateCurrentAction = energyComponent->DateNextAction;
        ControllableComponent* controllableComponent = mControllableMapper.get(e);
        switch(controllableComponent->mControleType)
        {
        case Control::keyboard:

            if(mTargetSelecting)
                HandleTargetSeletion(e);
            else
                KeyboardControle(e);
            break;
        case Control::IA:
            IAControle(e);
            break;
        default:
            SDL_assert(false);
        }
        return (energyComponent->DateNextAction != dateCurrentAction);
    }

    bool ControleSystem::IsPossible(AIActions action, artemis::Entity &e)
    {
        return true;
    }

    void ControleSystem::AI_Attack(artemis::Entity &e)
    {
        //choix de la target
        artemis::TagManager* tagManager = world->getTagManager();
        if(!tagManager->isSubscribed(TagList::PLayer))
            return;

        artemis::Entity& playerEntity = tagManager->getEntity(TagList::PLayer);
        PositionComponent* positionComponent = mPositionMapper.get(e);
        
        SDL_Point origin={positionComponent->mGridPosX, positionComponent->mGridPosY};
        CombatStatsComponent* combatStatsComponent = mCombatStatsMapper.get(e);
        {
            std::vector<int> targetList = positionComponent->mLevel->GetCreatureInArea(origin, combatStatsComponent->Range);
            PositionComponent* targetPositionComponent = nullptr;
            FactionComponent* faction = mFactionMapper.get(e);
            for(int i = 0; i < targetList.size(); ++i)
            {
                auto& target = world->getEntity(targetList[i]);
                FactionComponent* targetFaction = mFactionMapper.get(target);

                if(targetFaction->mFaction != faction->mFaction)
                {
                    targetPositionComponent = mPositionMapper.get(target);
                    break;
                }
            }
            if(targetPositionComponent)
            {
                 if(!TryAttack(e, targetPositionComponent->mGridPosX, targetPositionComponent->mGridPosY))
                    Wait(e);
                return;
            }
        }

        //deplacement a portee
        if(combatStatsComponent->BaseMoveSpeed <= 0)
        {
            Wait(e);
            return; 
        }
        int portee = 1; // a changer eventuellement un jour peut etre
        
        std::vector<int> targetList = positionComponent->mLevel->GetCreatureInArea(origin, portee);
        FactionComponent* faction = mFactionMapper.get(e);
        for(int i = 0; i < targetList.size(); ++i)
        {
            auto& target = world->getEntity(targetList[i]);
            FactionComponent* targetFaction = mFactionMapper.get(target);

            if(targetFaction->mFaction != faction->mFaction)
            {
                PositionComponent* targetPositionComponent = mPositionMapper.get(target);
                bool moveSuccess = (TryMoveInto(e, targetPositionComponent->mGridPosX - positionComponent->mGridPosX, targetPositionComponent->mGridPosY - positionComponent->mGridPosY));
                if(moveSuccess)
                    return;
            }
        }

        PositionComponent* playerPositionComponent = mPositionMapper.get(playerEntity);
        Dungeon::Path path = positionComponent->mLevel->SearchPath(positionComponent->mGridPosX, positionComponent->mGridPosY, playerPositionComponent->mGridPosX, playerPositionComponent->mGridPosY);

        if(path.size() > 1)
        {
            bool moveSuccess = (TryMoveInto(e, path[1].first - positionComponent->mGridPosX, path[1].second - positionComponent->mGridPosY));
            if(!moveSuccess)
                Wait(e);
        }
        else
            Wait(e);
    }

    void ControleSystem::IAControle(artemis::Entity &e)
    {
        //wait
        artemis::TagManager* tagManager = world->getTagManager();
        AIActions actionToDo = AIActions::nothing;
        if(tagManager->isSubscribed(TagList::PLayer))
        {
            artemis::Entity& playerEntity = tagManager->getEntity(TagList::PLayer);
            PositionComponent* playerPositionComponent = mPositionMapper.get(playerEntity);
            PositionComponent* positionComponent = mPositionMapper.get(e);
            int distanceX = playerPositionComponent->mGridPosX - positionComponent->mGridPosX;
            int distanceY = playerPositionComponent->mGridPosY - positionComponent->mGridPosY;
            int distanceSq = (distanceX*distanceX) + (distanceY*distanceY);
            VisibilityComponent* visibilityComponent = mVisibitilyMapper.get(e);
            if(distanceSq <= (visibilityComponent->mVisionRange*visibilityComponent->mVisionRange))
            {
                //Choix du behavior
                ControllableComponent* controllableComponent = mControllableMapper.get(e);
                for(int i = 0; i < (int)controllableComponent->mPossibleActions.size(); ++i)
                {
                    if(IsPossible(controllableComponent->mPossibleActions[i], e))
                    {
                        actionToDo = controllableComponent->mPossibleActions[i];
                        break;
                    }
                }
            }
        }
        switch(actionToDo)
        {
        case AIActions::attack:
            AI_Attack(e);
            break;
        case AIActions::nothing:
            Wait(e);
            break;
        };
    }

    void ControleSystem::ResetTargetSelection()
    {
        mTargetSelecting = false;
        mCurrentSkillTargeting = 0;
        targetSelectionX = -1;
        targetSelectionY = -1;
    }

    void ControleSystem::TryMoveTargetSelection(artemis::Entity &e, int dx, int dy)
    {
        PositionComponent* positionComponent = mPositionMapper.get(e);
        Dungeon::Path path;
        positionComponent->mLevel->FindVisibleCircle(positionComponent->mGridPosX, positionComponent->mGridPosY, mTargetRangeLimit, path);
        int x = targetSelectionX + dx;
        int y = targetSelectionY + dy;
        std::pair<int, int> pos(x, y);
        if (std::find(path.begin(), path.end(), pos) != path.end())
        {
            targetSelectionX = x;
            targetSelectionY = y;
        }

        mValidTargetPoint = true;
        switch (mTargetShape)
        {
        case SkillShape::Target:
            {
                mValidTargetPoint = false;
                auto target = positionComponent->mLevel->GetCreature(x, y);
                if (target != -1)
                {
                    auto& targetEntity = world->getEntity(target);
                    auto targetFaction = mFactionMapper.get(targetEntity);
                    auto selfFaction = mFactionMapper.get(e);

                    mValidTargetPoint =  targetFaction->mFaction != selfFaction->mFaction;
                }
            }
            break;
        case SkillShape::Ray:
            mValidTargetPoint = true;
            break;
        case SkillShape::GroundTarget:
            mValidTargetPoint = positionComponent->mLevel->IsMovableTile(x, y, Dungeon::EntityLayer::Creature);
            break;
        default:
            break;
        }
    }

    void ControleSystem::GetViableTargets(artemis::Entity& attacker, std::vector<artemis::Entity*>& targets)
    {
        PositionComponent* positionComponent = mPositionMapper.get(attacker);
        Dungeon::Path path;
        positionComponent->mLevel->FindVisibleCircle(positionComponent->mGridPosX, positionComponent->mGridPosY, mTargetRangeLimit, path);
        for (auto i : path)
        {
            auto id = positionComponent->mLevel->GetCreature(i.first, i.second);
            if (id > 0)
                targets.push_back(&world->getEntity(id));
        }
    }

    void ControleSystem::InitTargetSelection(artemis::Entity& attacker, int range, SkillShape shape)
    {
        CombatStatsComponent* combatStatsComponent = mCombatStatsMapper.get(attacker);
        if(!combatStatsComponent->Range)
            return;
        mTargetSelecting = true;
        mTargetRangeLimit = range;
        mTargetShape = shape;
        PositionComponent* positionComponent = mPositionMapper.get(attacker);
        targetSelectionX = positionComponent->mGridPosX;
        targetSelectionY = positionComponent->mGridPosY;
        if (shape != SkillShape::GroundTarget)
        {
            std::vector<artemis::Entity*> viable_targets;
            GetViableTargets(attacker, viable_targets);

            VisibilityComponent* visibilityComponent = mVisibitilyMapper.get(attacker);
            int distanceMin = INT_MAX;
            FactionComponent* faction = mFactionMapper.get(attacker);
            for(auto e : viable_targets)
            {
                auto& target = *e;
                FactionComponent* targetFaction = mFactionMapper.get(target);
                PositionComponent* targetPositionComponent = mPositionMapper.get(target);

                if(targetFaction->mFaction != faction->mFaction)
                {
                    int distance = std::max(    std::abs(targetPositionComponent->mGridPosX - positionComponent->mGridPosX),
                        std::abs(targetPositionComponent->mGridPosY - positionComponent->mGridPosY) );
                    if(distance < distanceMin)
                    {
                        targetSelectionX = targetPositionComponent->mGridPosX;
                        targetSelectionY = targetPositionComponent->mGridPosY;
                    }
                }
            }
        }
        TryMoveTargetSelection(attacker, 0, 0);
    }

    void ControleSystem::HandleTargetSeletion(artemis::Entity &e)
    {
        SDL_assert(Event::Interpreter::HasInstance());
        Event::Interpreter& eventInterpreter = Event::Interpreter::Instance();
        if (eventInterpreter.HasEvent())
        {
            auto event = eventInterpreter.PoolEvent();
            switch (event)
            {
            case Event::CANCEL:
                {
                    ResetTargetSelection();
                }
                break;
            case Event::SKIP_TURN:
                {
                    ResetTargetSelection();
                    Wait(e);
                }
                break;
            case Event::GO_DOWN_LEFT:
                TryMoveTargetSelection(e, -1, +1);
                break;
            case Event::GO_DOWN_RIGHT:
                TryMoveTargetSelection(e, +1, +1);
                break;
            case Event::GO_UP_LEFT:
                TryMoveTargetSelection(e, -1, -1);
                break;
            case Event::GO_UP_RIGHT:
                TryMoveTargetSelection(e, +1, -1);
                break;
            case Event::GO_UP:
                TryMoveTargetSelection(e,  0, -1);
                break;
            case Event::GO_DOWN:
                TryMoveTargetSelection(e,  0, +1);
                break;
            case Event::GO_LEFT:
                TryMoveTargetSelection(e, -1,  0);
                break;
            case Event::GO_RIGHT:
                TryMoveTargetSelection(e, +1,  0);
                break;
            case Event::PICK_UP:
                TryPickUp(e);
                break;

            case Event::DROP_ITEM_1:
                TryDrop(e, 0);
                break;
            case Event::DROP_ITEM_2:
                TryDrop(e, 1);
                break;
            case Event::DROP_ITEM_3:
                TryDrop(e, 2);
                break;
            case Event::DROP_ITEM_4:
                TryDrop(e, 3);
                break;
            case Event::DROP_ITEM_5:
                TryDrop(e, 4);
                break;
            case Event::DROP_ITEM_6:
                TryDrop(e, 5);
                break;

            case Event::LEARN_SKILL_1:
                TryLevelUp(e, 0);
                break;
            case Event::LEARN_SKILL_2:
                TryLevelUp(e, 1);
                break;
            case Event::LEARN_SKILL_3:
                TryLevelUp(e, 2);
                break;
            case Event::LEARN_SKILL_4:
                TryLevelUp(e, 3);
                break;

            case Event::USE_SKILL_1:
                TryUseSkill(e, 0);
                ResetTargetSelection();
                break;
            case Event::USE_SKILL_2:
                TryUseSkill(e, 1);
                ResetTargetSelection();
                break;
            case Event::USE_SKILL_3:
                TryUseSkill(e, 2);
                ResetTargetSelection();
                break;
            case Event::USE_SKILL_4:
                TryUseSkill(e, 3);
                ResetTargetSelection();
                break;
            case Event::RANGED_ATTACK:
                TryAttack(e, targetSelectionX, targetSelectionY);
                ResetTargetSelection();
                break;
            }
        }
    }

    void ControleSystem::DoRenderTargeting(PlayerInterfaceRenderer* renderer)
    {
        if (mTargetSelecting)
        {
            switch (mTargetShape)
            {
            case SkillShape::GroundTarget:
            case SkillShape::Target:
                if (mValidTargetPoint)
                    renderer->RenderTargetting(ReticleType::Valid, targetSelectionX, targetSelectionY);
                else
                    renderer->RenderTargetting(ReticleType::Invalid, targetSelectionX, targetSelectionY);
                break;
            case SkillShape::Ray:
                {
                    artemis::TagManager* tagManager = world->getTagManager();
                    artemis::Entity& playerEntity = tagManager->getEntity(TagList::PLayer);
                    PositionComponent* positionComponent = mPositionMapper.get(playerEntity);
                    Dungeon::Path path;
                    positionComponent->mLevel->FindRayLine(positionComponent->mGridPosX, positionComponent->mGridPosY, targetSelectionX, targetSelectionY, path);
                    for (auto p : path)
                    {
                        if (p.first == targetSelectionX && p.second == targetSelectionY)
                        {
                            if (mValidTargetPoint)
                                renderer->RenderTargetting(ReticleType::Valid, targetSelectionX, targetSelectionY);
                            else
                                renderer->RenderTargetting(ReticleType::Invalid, targetSelectionX, targetSelectionY);
                        }
                        else
                        {
                            renderer->RenderTargetting(ReticleType::Area, p.first, p.second);
                        }
                    }
                }
                break;
            }
        }
    }

    void ControleSystem::KeyboardControle(artemis::Entity &e)
    {
        SDL_assert(Event::Interpreter::HasInstance());
        Event::Interpreter& eventInterpreter = Event::Interpreter::Instance();
        if (eventInterpreter.HasEvent())
        {
            //tout event annule l'auto rest
            HealthManaComponent* manaHealthComponent = mManaHealthMapper.get(e);
            manaHealthComponent->Resting = false;

            auto event = eventInterpreter.PoolEvent();
            switch (event)
            {
            case Event::SKIP_TURN:
                {
                    Wait(e);
                }
                break;
            case Event::GO_DOWN_LEFT:
                TryMoveInto(e, -1, +1);
                break;
            case Event::GO_DOWN_RIGHT:
                TryMoveInto(e, +1, +1);
                break;
            case Event::GO_UP_LEFT:
                TryMoveInto(e, -1, -1);
                break;
            case Event::GO_UP_RIGHT:
                TryMoveInto(e, +1, -1);
                break;
            case Event::GO_UP:
                TryMoveInto(e,  0, -1);
                break;
            case Event::GO_DOWN:
                TryMoveInto(e,  0, +1);
                break;
            case Event::GO_LEFT:
                TryMoveInto(e, -1,  0);
                break;
            case Event::GO_RIGHT:
                TryMoveInto(e, +1,  0);
                break;
            case Event::PICK_UP:
                TryPickUp(e);
                break;
            case Event::REST:
                TryRest(e);
                break;

            case Event::DROP_ITEM_1:
                TryDrop(e, 0);
                break;
            case Event::DROP_ITEM_2:
                TryDrop(e, 1);
                break;
            case Event::DROP_ITEM_3:
                TryDrop(e, 2);
                break;
            case Event::DROP_ITEM_4:
                TryDrop(e, 3);
                break;
            case Event::DROP_ITEM_5:
                TryDrop(e, 4);
                break;
            case Event::DROP_ITEM_6:
                TryDrop(e, 5);
                break;

            case Event::USE_ITEM_1:
                TryUseItem(e, 0);
                break;
            case Event::USE_ITEM_2:
                TryUseItem(e, 1);
                break;
            case Event::USE_ITEM_3:
                TryUseItem(e, 2);
                break;
            case Event::USE_ITEM_4:
                TryUseItem(e, 3);
                break;
            case Event::USE_ITEM_5:
                TryUseItem(e, 4);
                break;
            case Event::USE_ITEM_6:
                TryUseItem(e, 5);
                break;

            case Event::LEARN_SKILL_1:
                TryLevelUp(e, 0);
                break;
            case Event::LEARN_SKILL_2:
                TryLevelUp(e, 1);
                break;
            case Event::LEARN_SKILL_3:
                TryLevelUp(e, 2);
                break;
            case Event::LEARN_SKILL_4:
                TryLevelUp(e, 3);
                break;

            case Event::USE_SKILL_1:
                TryUseSkill(e, 0);
                break;
            case Event::USE_SKILL_2:
                TryUseSkill(e, 1);
                break;
            case Event::USE_SKILL_3:
                TryUseSkill(e, 2);
                break;
            case Event::USE_SKILL_4:
                TryUseSkill(e, 3);
                break;
            case Event::RANGED_ATTACK:
                {
                    auto combatStats = mCombatStatsMapper.get(e);
                    if (combatStats->Range > 0)
                        InitTargetSelection(e, combatStats->Range, SkillShape::Target);
                    else
                        AddLog("You do not have ranged attack", LogSource::Info);
                }
                break;
            }
        }
        else
            RestIfn(e);
        HealthManaComponent* manaHealthComponent = mManaHealthMapper.get(e);
        manaHealthComponent->LastHealth = manaHealthComponent->CurrentHealth;
    }
}
}