#include "EventInterpreter.h"
#include "Console.h"

#include "SDL.h"
namespace DotaRL { namespace Event
{
    const int MaxEventBufferSize = 3;

    Interpreter::Interpreter() : TSingleton(),
        mQuitAsked(false)
    {
        mKeyItem1 = SDL_GetKeyFromScancode(SDL_SCANCODE_Q);
        mKeyItem2 = SDL_GetKeyFromScancode(SDL_SCANCODE_W);
        mKeyItem3 = SDL_GetKeyFromScancode(SDL_SCANCODE_E);
        mKeyItem4 = SDL_GetKeyFromScancode(SDL_SCANCODE_R);
        mKeyItem5 = SDL_GetKeyFromScancode(SDL_SCANCODE_T);
        mKeyItem6 = SDL_GetKeyFromScancode(SDL_SCANCODE_Y);
    }

    Interpreter::~Interpreter()
    {
    }

    void Interpreter::HandleEveyKeyItem(int index, Uint16 mod)
    {
        if (mod & KMOD_ALT)
        {
            Event::Type ev = static_cast<Event::Type>(((int)Event::DROP_ITEM_1) + index - 1);
            mEventStack.push_back(ev);
        }
        else if (mod == 0)
        {
            Event::Type ev = static_cast<Event::Type>(((int)Event::USE_ITEM_1) + index - 1);
            mEventStack.push_back(ev);
        }
    }

    void Interpreter::Process()
    {    
        SDL_Event event;

        while (SDL_PollEvent(&event))
        {
            //Boucle d'interpretation des evennement
            switch(event.type)
            {
            case SDL_QUIT:
                mQuitAsked = true;
                break;
            case SDL_KEYDOWN:
                if (event.key.keysym.sym == SDLK_F4 && (event.key.keysym.mod & KMOD_ALT))
                    mQuitAsked = true;
                else if (event.key.keysym.sym == SDLK_q && (event.key.keysym.mod & KMOD_CTRL))
                    mQuitAsked = true;
                else if (event.key.keysym.sym == mKeyItem1)
                    HandleEveyKeyItem(1, event.key.keysym.mod);
                else if (event.key.keysym.sym == mKeyItem2)
                    HandleEveyKeyItem(2, event.key.keysym.mod);
                else if (event.key.keysym.sym == mKeyItem3)
                    HandleEveyKeyItem(3, event.key.keysym.mod);
                else if (event.key.keysym.sym == mKeyItem4)
                    HandleEveyKeyItem(4, event.key.keysym.mod);
                else if (event.key.keysym.sym == mKeyItem5)
                    HandleEveyKeyItem(5, event.key.keysym.mod);
                else if (event.key.keysym.sym == mKeyItem6)
                    HandleEveyKeyItem(6, event.key.keysym.mod);
                else if (event.key.keysym.sym == mKeyItem1 && (event.key.keysym.mod & KMOD_ALT))
                    mEventStack.push_back(Event::DROP_ITEM_1);
                else if (event.key.keysym.sym == mKeyItem2 && (event.key.keysym.mod & KMOD_ALT))
                    mEventStack.push_back(Event::DROP_ITEM_2);
                else if (event.key.keysym.sym == mKeyItem3 && (event.key.keysym.mod & KMOD_ALT))
                    mEventStack.push_back(Event::DROP_ITEM_3);
                else if (event.key.keysym.sym == mKeyItem4 && (event.key.keysym.mod & KMOD_ALT))
                    mEventStack.push_back(Event::DROP_ITEM_4);
                else if (event.key.keysym.sym == mKeyItem5 && (event.key.keysym.mod & KMOD_ALT))
                    mEventStack.push_back(Event::DROP_ITEM_5);
                else if (event.key.keysym.sym == mKeyItem6 && (event.key.keysym.mod & KMOD_ALT))
                    mEventStack.push_back(Event::DROP_ITEM_6);
                else if (event.key.keysym.sym == SDLK_1 && (event.key.keysym.mod & KMOD_CTRL))
                    mEventStack.push_back(Event::LEARN_SKILL_1);
                else if (event.key.keysym.sym == SDLK_2 && (event.key.keysym.mod & KMOD_CTRL))
                    mEventStack.push_back(Event::LEARN_SKILL_2);
                else if (event.key.keysym.sym == SDLK_3 && (event.key.keysym.mod & KMOD_CTRL))
                    mEventStack.push_back(Event::LEARN_SKILL_3);
                else if (event.key.keysym.sym == SDLK_4 && (event.key.keysym.mod & KMOD_CTRL))
                    mEventStack.push_back(Event::LEARN_SKILL_4);
                else
                    HandleKeydownEvent(event.key.keysym);
                break;
            }
        }
    }

    void Interpreter::HandleKeydownEvent(SDL_Keysym& keysym)
    {
        if (mEventStack.size() >= MaxEventBufferSize)
            return;

        switch(keysym.sym)
        {
        case SDLK_KP_5:
            mEventStack.push_back(Event::SKIP_TURN);
            break;
        case SDLK_KP_4:
            mEventStack.push_back(Event::GO_LEFT);
            break;
        case SDLK_KP_6:
            mEventStack.push_back(Event::GO_RIGHT);
            break;
        case SDLK_KP_8:
        case SDLK_UP:
            mEventStack.push_back(Event::GO_UP);
            break;
        case SDLK_KP_2:
        case SDLK_DOWN:
            mEventStack.push_back(Event::GO_DOWN);
            break;
        case SDLK_KP_7:
        case SDLK_LEFT:
            mEventStack.push_back(Event::GO_UP_LEFT);
            break;
        case SDLK_KP_9:
        case SDLK_RIGHT:
            mEventStack.push_back(Event::GO_UP_RIGHT);
            break;
        case SDLK_KP_1:
            mEventStack.push_back(Event::GO_DOWN_LEFT);
            break;
        case SDLK_KP_3:
            mEventStack.push_back(Event::GO_DOWN_RIGHT);
            break;
        case SDLK_g:
            mEventStack.push_back(Event::PICK_UP);
            break;
        case SDLK_1:
            mEventStack.push_back(Event::USE_SKILL_1);
            break;
        case SDLK_2:
            mEventStack.push_back(Event::USE_SKILL_2);
            break;
        case SDLK_3:
            mEventStack.push_back(Event::USE_SKILL_3);
            break;
        case SDLK_4:
            mEventStack.push_back(Event::USE_SKILL_4);
            break;
        case SDLK_ESCAPE:
            mEventStack.push_back(Event::CANCEL);
            break;
        case SDLK_SPACE:
        case SDLK_f:
            mEventStack.push_back(Event::RANGED_ATTACK);
            break;
        case SDLK_s:
            mEventStack.push_back(Event::REST);
            break;
        case SDLK_c:
            Console::Instance().Toggle();
            break;
        }
    }

    bool Interpreter::QuitAsked() const
    {
        return mQuitAsked;
    }

    bool Interpreter::HasEvent() const
    {
        return !mEventStack.empty();
    }

    Type Interpreter::PoolEvent()
    {
        SDL_assert(!mEventStack.empty());
        Type r = mEventStack.front();
        mEventStack.pop_front();
        return r;
    }

    void Interpreter::FlushEvents()
    {
        mEventStack.clear();
    }
}
}
