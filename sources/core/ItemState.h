#pragma once
#include "ItemTemplate.h"

namespace DotaRL { namespace GameObject 
{
    struct SkillState;
    struct ItemState
    {
        ItemState(ItemTemplate itemTemplate);
        ~ItemState();

        int mCooldown;
        int mCharges;
        ItemTemplate mItemTemplate;
        SkillState* mSkill;
    };

    SkillState* GetItemSkillState(SkillId skillId);

}}
