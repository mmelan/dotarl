#pragma once

#include <cstdint>

#include "Artemis/EntityProcessingSystem.h"
#include "Artemis/ComponentMapper.h"

#include "StatsComponent.h"
#include "Buffs.h"

namespace DotaRL { namespace GameObject
{
    class EnergySystem;

    class StatsSystem : public artemis::EntityProcessingSystem
    {
    public:
        StatsSystem(EnergySystem* energySystem);

        void initialize() override;
        void processEntity(artemis::Entity &e) override;
        void end() override;
        bool checkProcessing() override;
    private:
        artemis::ComponentMapper<HealthManaComponent> mHealthManaMapper;
        artemis::ComponentMapper<CombatStatsComponent> mCombatStatsMapper;
        artemis::ComponentMapper<CoreStatsComponent> mCoreStatsMapper;
        artemis::ComponentMapper<BuffListComponent> mBuffListMapper;

        EnergySystem* mEnergySystem;
        int64_t mDateNextRegen;
    };
}
}
