#include "StartScreen.h"
#include "SDL_Assert.h"
#include "SDL_Helper.h"
#include "PlayerInterface.h"
#include "Renderer.h"
#include "RenderConstantes.h"
#include "SDL.h"
#include <vector>

namespace DotaRL
{
const float FRAME_MIN_DURATION = 1.0f / 60.0f; // 60 FPS cap
    Character StartScreen()
    {
        bool quit_asked = false;
        bool validated = false;
        int selectionIndex = 1;
        Uint64 last_tick = SDL_GetPerformanceCounter();
        Renderer& renderer = Renderer::Instance();

        SDL_Surface* titleSurface = SDL_LoadImage_VFS("data/title.png");
        int title_w = titleSurface->w;
        int title_h = titleSurface->h;
        SDL_Texture* titleTexture = SDL_CreateTextureFromSurface(renderer.SDLRenderer(), titleSurface);
        SDL_FreeSurface(titleSurface);

        SDL_Surface* secondBgSurface = SDL_LoadImage_VFS("data/screenshot.png");
        int seconBg_w = secondBgSurface->w;
        int seconBg_h = secondBgSurface->h;
        SDL_Texture* secondBgTexture = SDL_CreateTextureFromSurface(renderer.SDLRenderer(), secondBgSurface);
        SDL_FreeSurface(secondBgSurface);

        SDL_Surface* selectChar = SDL_LoadImage_VFS("data/select.png");
        int selectCharW = selectChar->w;
        int selectCharH = selectChar->h;
        SDL_Texture* selectCharTexture = SDL_CreateTextureFromSurface(renderer.SDLRenderer(), selectChar);
        SDL_SetTextureBlendMode( selectCharTexture, SDL_BLENDMODE_BLEND );
        SDL_FreeSurface(selectChar);

        SDL_Surface* bgSurface = SDL_CreateRGBSurface(0, Window::LogicSizeX, Window::LogicSizeY, 32, 0, 0, 0, 0);
        SDL_FillRect(bgSurface, NULL, SDL_MapRGBA(bgSurface->format, 255, 255, 255, 255));
        SDL_Texture* bgTexture = SDL_CreateTextureFromSurface(renderer.SDLRenderer(), bgSurface);
        SDL_SetTextureBlendMode( bgTexture, SDL_BLENDMODE_BLEND );
        SDL_SetTextureAlphaMod( bgTexture, 100 );
        SDL_FreeSurface(bgSurface);

        const int iconSize = 32;
        const int iconBgSize = 36;
        SDL_Surface* iconBgSurface = SDL_CreateRGBSurface(0, iconBgSize, iconBgSize, 32, 0, 0, 0, 0);
        SDL_FillRect(iconBgSurface, NULL, SDL_MapRGBA(iconBgSurface->format, 150, 150, 150, 255));
        SDL_Texture* iconBgTexture = SDL_CreateTextureFromSurface(renderer.SDLRenderer(), iconBgSurface);
        SDL_FreeSurface(iconBgSurface);

        const int selectionGrowSize = 4;
        SDL_Surface* iconBgSurface_selected = SDL_CreateRGBSurface(0, iconBgSize+selectionGrowSize*2, iconBgSize+selectionGrowSize*2, 32, 0, 0, 0, 0);
        SDL_FillRect(iconBgSurface_selected, NULL, SDL_MapRGBA(iconBgSurface_selected->format, 200, 200, 10, 255));
        SDL_Texture* iconBgTexture_Selected = SDL_CreateTextureFromSurface(renderer.SDLRenderer(), iconBgSurface_selected);
        SDL_SetTextureBlendMode( iconBgTexture_Selected, SDL_BLENDMODE_BLEND );
        SDL_FreeSurface(iconBgSurface_selected);

        std::vector<SDL_Texture*> iconTextureList;
        {
            SDL_Surface* s = SDL_LoadImage_VFS("data/creatures/Ursa.png");
            SDL_Texture* t = SDL_CreateTextureFromSurface(renderer.SDLRenderer(), s);
            SDL_FreeSurface(s);
            iconTextureList.push_back(t);
        }
        {
            SDL_Surface* s = SDL_LoadImage_VFS("data/creatures/Venomancer_icon.png");
            SDL_Texture* t = SDL_CreateTextureFromSurface(renderer.SDLRenderer(), s);
            SDL_FreeSurface(s);
            iconTextureList.push_back(t);
        }
        {
            SDL_Surface* s = SDL_LoadImage_VFS("data/creatures/Queen_of_Pain_icon.png");
            SDL_Texture* t = SDL_CreateTextureFromSurface(renderer.SDLRenderer(), s);
            SDL_FreeSurface(s);
            iconTextureList.push_back(t);
        }

        int alphaSelect = 240;
        int increment = 2;
        while (!quit_asked && !validated)
        {
            //cap des frames
            Uint64 current_tick = SDL_GetPerformanceCounter();
            float remaining_time;
            while ((remaining_time = static_cast<float>(current_tick - last_tick) / SDL_GetPerformanceFrequency() - FRAME_MIN_DURATION) < 0)
            {
                SDL_Delay(static_cast<uint32_t>(-remaining_time*1000.0f));
                current_tick = SDL_GetPerformanceCounter();
            }
            
            SDL_Event event;

            while (SDL_PollEvent(&event))
            {
                //Boucle d'interpretation des evennement
                switch(event.type)
                {
                case SDL_QUIT:
                    quit_asked = true;
                    break;
                case SDL_KEYDOWN:
                    if (event.key.keysym.sym == SDLK_ESCAPE)
                        quit_asked = true;
                    else if (event.key.keysym.sym == SDLK_F4 && (event.key.keysym.mod & KMOD_ALT))
                        quit_asked = true;
                    else if (event.key.keysym.sym == SDLK_q && (event.key.keysym.mod & KMOD_CTRL))
                        quit_asked = true;
                    if (event.key.keysym.sym == SDLK_RETURN || 
                        event.key.keysym.sym == SDLK_SPACE || 
                        event.key.keysym.sym == SDLK_KP_ENTER )
                    {
                        validated = true;
                    }
                    if (event.key.keysym.sym == SDLK_LEFT || event.key.keysym.sym == SDLK_KP_4)
                        selectionIndex -= 1;
                    if (event.key.keysym.sym == SDLK_RIGHT || event.key.keysym.sym == SDLK_KP_6)
                        selectionIndex += 1;
                    break;
                default:
                    break;
                };
            }
            if(selectionIndex <= 0)
                selectionIndex = (int)Character::Lenght-1;
            else if(selectionIndex >= (int)Character::Lenght)
                selectionIndex = 1;

            //boucle de rendu
            if(alphaSelect>=250)
                increment = -2;
            else if(alphaSelect<=150)
                increment = 2;
            alphaSelect += increment;
            {
	            SDL_Rect position;
                /*position.x = (Window::LogicSizeX - seconBg_w) / 2;
                position.y = (Window::LogicSizeY - seconBg_h) / 2;
                position.w = seconBg_w;
                position.h = seconBg_h;*/
                position.x = 0;
                position.y = 0;
                position.w = Window::LogicSizeX;
                position.h = Window::LogicSizeY;
                renderer.Render(secondBgTexture, nullptr, &position, false);
            }
            {
	            SDL_Rect position;
                position.x = 0;
                position.y = 0;
                position.w = Window::LogicSizeX;
                position.h = Window::LogicSizeY;
                renderer.Render(bgTexture, nullptr, &position, false);
            }
            {
	            SDL_Rect position;
                position.x = (Window::LogicSizeX - title_w) / 2;
                position.y = (Window::LogicSizeY - title_h) / 4;
                position.w = title_w;
                position.h = title_h;
                renderer.Render(titleTexture, nullptr, &position, false);
            }
            {
	            SDL_Rect position;
                position.x = (Window::LogicSizeX - title_w) / 2;
                position.y = (Window::LogicSizeY - title_h) / 4 + title_h;
                position.w = selectCharW;
                position.h = selectCharH;
                //SDL_SetTextureAlphaMod( selectCharTexture, alphaSelect );
                renderer.Render(selectCharTexture, nullptr, &position, false);
            }
            const int margin = 8;
            for(int i = 1; i  < (int)Character::Lenght; ++i)
            {
                if(i == selectionIndex)
                {
	                SDL_Rect position;
                    position.x = (Window::LogicSizeX+iconBgSize)/4 + (iconBgSize+margin)*i-2-selectionGrowSize;
                    position.y = (Window::LogicSizeY - title_h) / 4 + title_h +  selectCharH + margin - selectionGrowSize;
                    position.w = iconBgSize+2*selectionGrowSize;
                    position.h = iconBgSize+2*selectionGrowSize;
                    SDL_SetTextureAlphaMod( iconBgTexture_Selected, alphaSelect );
                    renderer.Render(iconBgTexture_Selected, nullptr, &position, false);
                }
                //else
                {
	                SDL_Rect position;
                    position.x = (Window::LogicSizeX+iconBgSize)/4 + (iconBgSize+margin)*i-2;
                    position.y = (Window::LogicSizeY - title_h) / 4 + title_h +  selectCharH + margin;
                    position.w = iconBgSize;
                    position.h = iconBgSize;
                    renderer.Render(iconBgTexture, nullptr, &position, false);
                }

                {
	                SDL_Rect position;
                    position.x = (Window::LogicSizeX+iconBgSize)/4 + (iconBgSize+margin)*i;
                    position.y = (Window::LogicSizeY - title_h) / 4 + title_h +  selectCharH + margin;
                    position.w = iconSize;
                    position.h = iconSize;
                    if(i == selectionIndex)
                        SDL_SetTextureAlphaMod( iconTextureList[i-1], alphaSelect );
                    else
                        SDL_SetTextureAlphaMod( iconTextureList[i-1], 255 );
                    renderer.Render(iconTextureList[i-1], nullptr, &position, false);
                }
            }
            renderer.Process();
        }
        
        SDL_DestroyTexture(iconBgTexture_Selected);
        SDL_DestroyTexture(iconBgTexture);
        SDL_DestroyTexture(bgTexture);
        SDL_DestroyTexture(selectCharTexture);
        SDL_DestroyTexture(titleTexture);
        SDL_DestroyTexture(secondBgTexture);
        SDL_DestroyTexture(iconBgTexture_Selected);
        for(int i = 0; i < iconTextureList.size(); ++i)
            SDL_DestroyTexture(iconTextureList[i]);
        if(quit_asked)
            return Character::None;
        return (Character)selectionIndex;
    }

}