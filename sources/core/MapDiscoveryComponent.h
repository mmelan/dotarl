#pragma once

#include "Artemis/Component.h"

namespace DotaRL
{
    namespace GameObject
    {
        class MapDiscoveryComponent : public artemis::Component
        {
        public:
            MapDiscoveryComponent();
        };
    }
}
