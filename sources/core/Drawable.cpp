#include "Drawable.h"

namespace DotaRL
{
    namespace Rendering
    {
        Drawable::Drawable()
            : mPosX(0)
            , mPosY(0)
            , mLayer(Layer::Items)
            , mTexture(nullptr)
            , mSurface(nullptr)
        {
        }
    }
}