#pragma once

#include "Artemis/EntityManager.h"
#include "MobTemplate.h"
#include "ItemTemplate.h"
#include "dungeon.h"
#include "ItemState.h"

namespace Dungeon
{
    class Level;
}

namespace DotaRL
{
    void CreateUrsa(artemis::World& world, int x, int y, Dungeon::Level* level);
    void CreateVenomancer(artemis::World& world, int x, int y, Dungeon::Level* level);
    void CreateQoP(artemis::World& world, int x, int y, Dungeon::Level* level);
    void CreatePlagueWard(artemis::World& world, int x, int y, Dungeon::Level* level, int skillLevel);
    void CreateMob(artemis::World& world,  int x, int y, Dungeon::Level* level, const MobTemplate& mobTemplate);
    void CreateItemOnGround(artemis::World& world,  int x, int y, Dungeon::Level* level, const GameObject::ItemTemplate& mobTemplate, GameObject::ItemState* itemState = NULL);
    void CreateProjectile(artemis::World& world,  int fromX, int fromY,  int toX, int toY, Dungeon::Level* level, char* imgpath);
    void CreateMobInRoom(artemis::World& world, int minx, int maxx, int miny, int maxy, std::vector<MobTemplate>& mobTable, Dungeon::Level& level, int maxLevel);
}