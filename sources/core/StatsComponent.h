#pragma once

#include "SDL_assert.h"
#include "Artemis/Component.h"

namespace DotaRL { namespace GameObject
{
    class HealthManaComponent : public artemis::Component
    {
    public:
        HealthManaComponent(float baseHealthRegen, float baseManaRegen);
        HealthManaComponent(float baseHealthRegen, float baseManaRegen, float maxHealth, float maxMana);

        int LastEntityToDamage;

        float MaxHealth;
        float MaxMana;

        float LastMaxHealth;
        float LastMaxMana;
        
        float CurrentHealth;
        float LastHealth;
        float CurrentMana;

        const float IntrinsicHealthRegen;
        const float IntrinsicManaRegen;

        float BaseHealthRegen;
        float BaseManaRegen;

        float HealthRegen;
        float ManaRegen;

        bool Resting;
    };

    class CombatStatsComponent : public artemis::Component
    {
    public:
        CombatStatsComponent(float moveSpeed, float minDamage, float maxDamage, float armor, float baseAttackTime, int range, int level);

        const float IntrinsicMoveSpeed;
        const float IntrinsicMinDamage;
        const float IntrinsicMaxDamage;
        const float IntrinsicArmor;

        int Level;

        int Range;

        float BaseMoveSpeed;
        float BaseMinDamage;
        float BaseMaxDamage;
        float BaseAttackTime;
        float BaseIAS;
        float BaseArmor;

        float MoveSpeed;
        float MinDamage;
        float MaxDamage;
        float IAS;
        float Armor;
    };

    enum class CoreMainStat { Str, Agi, Int };

    class CoreStatsComponent : public artemis::Component
    {
    public:
        CoreStatsComponent(CoreMainStat mainStat, int baseStr, float strPerLevel, int baseAgi, float agiPerLevel, int baseInt, float intPerLevel);

        CoreMainStat MainStat;

        int IntrinsicStr;
        int IntrinsicAgi;
        int IntrinsicInt;

        int BaseStr;
        int BaseAgi;
        int BaseInt;

        int Str;
        int Agi;
        int Int;

        float StrPerLevel;
        float AgiPerLevel;
        float IntPerLevel;
    };

    int ComputeCoreStatAttackBonus(const CoreStatsComponent& coreStats);
}
}
