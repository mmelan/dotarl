#ifndef RENDERER
#define RENDERER

#include "Singleton.h"
#include <map>
#include <vector>
#include "SDL_stdinc.h"
#include "SDL.h"
#include "Artemis/World.h"
#include "dungeon.h"

struct SDL_Window;
struct SDL_Renderer;
struct SDL_Texture;
namespace DotaRL
{
    class Renderer : public TSingleton<Renderer>
    {
        friend class TSingleton<Renderer>;
    public:
        void Process();
        void Initialise();
        void LoopStart();
        void LoopStart(artemis::World& world, const Dungeon::Level& dungeonLevel);

        SDL_Renderer* SDLRenderer() { return mRenderer; }
        bool ResolutionChanged() { return mReinitAll; }
        void Render(SDL_Texture * texture,
                    SDL_Rect * srcrect,
                    SDL_Rect * dstrect,
                    bool cameraRelative);
        SDL_Point CameraPosition() { return mCameraPosition; }

    protected:
        Renderer();
        ~Renderer();

    private:
	    void ApplyVideoSettings();

	    SDL_Window* mWindow;
	    SDL_Renderer* mRenderer;
        bool mReinitAll;
        SDL_Point mCameraPosition;
        bool mCameraMoving;
        bool mTeleport;
    };
}
#endif
