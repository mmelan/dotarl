#pragma once

#include <cstdint>

#include "Artemis/EntityProcessingSystem.h"
#include "Artemis/ComponentMapper.h"

#include "ExperienceComponent.h"
#include "SkillSetComponent.h"
#include "StatsComponent.h"

namespace DotaRL
{
    namespace GameObject
    {
        class ExperienceSystem : public artemis::EntityProcessingSystem
        {
        public:
            ExperienceSystem();

            void initialize() override;
            void processEntity(artemis::Entity &e) override;
        private:
            artemis::ComponentMapper<ExperienceComponent> mExperienceMapper;
            artemis::ComponentMapper<CombatStatsComponent> mCombatStatsMapper;
        };
    }
}
