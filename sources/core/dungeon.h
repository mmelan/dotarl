#pragma once

#include <cstdint>
#include <utility>
#include <vector>
#include <map>

#include "SDL_rect.h"
#include "Artemis/World.h"
#include "Artemis/EntityProcessingSystem.h"
#include "Artemis/ComponentMapper.h"

#include "fov.h"

namespace DotaRL { namespace Dungeon {
    class LevelRenderer;
    enum class Feature
    {
        Nothing, // completely unpassable
        HighMountain, // completely unpassable
        Mountain,
        RadiantTree,
        DireTree,
        RadiantGrass,
        DireGrass,
        Water,
        Fountain,
        Lenght
    };

    enum class Detail
    {
        Nothing,
        Road
    };

    enum class StartingSide
    {
        Radiant,
        Dire
    };

    bool IsMoveableFeature(Feature f);
    enum class Layers
    {
        Ground,
        Collisionnable,
        Lenght
    };

    enum class EntityLayer
    {
        None,
        Creature,
        Item
    };
    typedef std::vector< std::pair<int, int> > Path;

    class Level
    {
    public:
        Level(int sizeX, int sizeY);
        ~Level();

        void loopStart();

        std::pair<int, int> Size() const;
        int SizeX() const;
        int SizeY() const;

        Feature GetFeature(int x, int y, Layers layer) const;
        Feature GetLastKnownFeature(int x, int y, Layers layer) const;
        Detail GetDetail(int x, int y) const;

        bool GetVisibility(int x, int y) const;
        void SetVisibility(int x, int y);

        Feature& GetFeature(int x, int y, Layers layer);
        Detail& GetDetail(int x, int y);
        bool IsInMap(int x, int y) const;

        void FillFeature(Feature f, Layers layer);
        void FillFeatureRect(Feature f, int minx, int maxx, int miny, int maxy, Layers layer);
        void ReplaceFeatureRect(Feature f0, Feature f1, int minx, int maxx, int miny, int maxy, Layers layer);
        SDL_Point FindEmptyPosition(const SDL_Point& origin, EntityLayer layer = EntityLayer::None) const;
        std::vector<int> GetCreatureInArea(const SDL_Point& origin, int radius, EntityLayer layer = EntityLayer::None) const;
        bool IsMovableTile(int x, int y, EntityLayer layer = EntityLayer::None) const;
        bool IsAccessibleTile(int x, int y, EntityLayer layer) const;

        int GetCreature(int x, int y) const;
        void PlaceCreatureAt(int entity, int x, int y);
        void RemoveCreatureAt(int entity, int x, int y);

        void PlaceItemAt(int entity, int x, int y);
        void RemoveItemAt(int entity, int x, int y);
        int GetItem(int x, int y) const;

        Path SearchPath(int startX, int startY, int endX, int endY);
        void FindVisibleCircle(int x, int y, int radius, Path& result);
        void FindRayLine(int startX, int startY, int destX, int destY, Path& result);

        LevelRenderer* mLevelRenderer;
    private:
        const int mSizeX;
        const int mSizeY;

        size_t GetIndex(int x, int y) const;

        typedef std::vector<Feature> FeatureMap;
        std::map<Layers, FeatureMap> mFeatures;
        std::vector<Detail> mDetails;
        std::vector<int> mCreatures;
        std::vector<int> mItems;

        std::map<Layers, FeatureMap> mLastSeenTileState;
        std::vector<bool> mTileVisibility;

        fov_settings_type mFovSettings;
    };

    SDL_Point MakeRandomLevel(Level& level, StartingSide side, artemis::World& world);
    void DumpLevel(const Level& level);

}
}
