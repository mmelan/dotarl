#include "EntityCreation.h"

#include <random>
#include <vector>
#include <ctime>

#include "Artemis/Entity.h"
#include "Artemis/World.h"
#include "SDL_Helper.h"

#include "AppearanceComponent.h"
#include "Buffs.h"
#include "ControllableComponent.h"
#include "dungeon.h"
#include "EnergyComponent.h"
#include "PositionComponent.h"
#include "VisibilityComponent.h"
#include "MapDiscoveryComponent.h"
#include "InventoryComponent.h"
#include "FactionComponent.h"
#include "StatsComponent.h"
#include "ItemInfoComponent.h"
#include "SkillSetComponent.h"
#include "SkillDescription.h"
#include "ExperienceComponent.h"
#include "TagList.h"
#include "RandomGenerator.h"

namespace DotaRL
{
    void CreateUrsa(artemis::World& world, int x, int y, Dungeon::Level* level)
    {
        artemis::Entity & ursa = world.getEntityManager()->create();
        GameObject::AppearanceComponent* appearance = new GameObject::AppearanceComponent();
        appearance->mAppearance_Ground.mLayer = Layer::Mob;
        appearance->mAppearance_Ground.mSurface = SDL_LoadImage_VFS("data/creatures/Ursa.png");
        appearance->mName = "Ursa";

        ursa.addComponent(appearance);
        ursa.addComponent(new GameObject::ControllableComponent(GameObject::Control::keyboard));
        ursa.addComponent(new GameObject::EnergyComponent());
        ursa.addComponent(new GameObject::VisibilityComponent(5));
        ursa.addComponent(new GameObject::MapDiscoveryComponent());
        ursa.addComponent(new GameObject::HealthManaComponent(.25f,.01f));
        ursa.addComponent(new GameObject::CoreStatsComponent(GameObject::CoreMainStat::Agi, 23, 2.9f, 18, 2.1f, 16, 1.5f));
        ursa.addComponent(new GameObject::CombatStatsComponent(310.f, 27.f, 31.f, 2.95f, 1.7f, 0, 1));
        SDL_Point origin={x,y};
        SDL_Point playerpos = level->FindEmptyPosition(origin, Dungeon::EntityLayer::Creature);
        ursa.addComponent(new GameObject::PositionComponent(playerpos.x, playerpos.y, level, GameObject::PositionLayer::Creature));
        ursa.addComponent(new GameObject::BuffListComponent);
        ursa.addComponent(new GameObject::InventoryComponent);
        ursa.addComponent(new GameObject::ExperienceComponent);
        ursa.addComponent(new GameObject::FactionComponent(GameObject::Faction::GoodGuys));
        auto skillComponent = new GameObject::SkillSetComponent();
        skillComponent->mSkillSet.push_back(GameObject::SkillState(GameObject::GetSkillDescription(GameObject::SkillId::Earthshock)));
        skillComponent->mSkillSet.push_back(GameObject::SkillState(GameObject::GetSkillDescription(GameObject::SkillId::Overpower)));
        skillComponent->mSkillSet.push_back(GameObject::SkillState(GameObject::GetSkillDescription(GameObject::SkillId::Fury_Swipes)));
        skillComponent->mSkillSet.push_back(GameObject::SkillState(GameObject::GetSkillDescription(GameObject::SkillId::Enrage)));
        ursa.addComponent(skillComponent);

        ursa.refresh();
        world.getTagManager()->subscribe(TagList::PLayer, ursa);
        level->PlaceCreatureAt(ursa.getId(), playerpos.x, playerpos.y);
    }
    void CreateVenomancer(artemis::World& world, int x, int y, Dungeon::Level* level)
    {
        artemis::Entity & ursa = world.getEntityManager()->create();
        GameObject::AppearanceComponent* appearance = new GameObject::AppearanceComponent();
        appearance->mAppearance_Ground.mLayer = Layer::Mob;
        appearance->mAppearance_Ground.mSurface = SDL_LoadImage_VFS("data/creatures/Venomancer_icon.png");
        appearance->mName = "Venomancer";

        ursa.addComponent(appearance);
        ursa.addComponent(new GameObject::ControllableComponent(GameObject::Control::keyboard));
        ursa.addComponent(new GameObject::EnergyComponent());
        ursa.addComponent(new GameObject::VisibilityComponent(5));
        ursa.addComponent(new GameObject::MapDiscoveryComponent());
        ursa.addComponent(new GameObject::HealthManaComponent(.25f,.01f));
        ursa.addComponent(new GameObject::CoreStatsComponent(GameObject::CoreMainStat::Agi, 18, 1.85f, 22, 2.6f, 15, 1.75f));
        ursa.addComponent(new GameObject::CombatStatsComponent(285.f, 27.f, 31.f, -0.06f, 1.7f, 3, 1));
        SDL_Point origin={x,y};
        SDL_Point playerpos = level->FindEmptyPosition(origin, Dungeon::EntityLayer::Creature);
        ursa.addComponent(new GameObject::PositionComponent(playerpos.x, playerpos.y, level, GameObject::PositionLayer::Creature));
        ursa.addComponent(new GameObject::BuffListComponent);
        ursa.addComponent(new GameObject::InventoryComponent);
        ursa.addComponent(new GameObject::ExperienceComponent);
        ursa.addComponent(new GameObject::FactionComponent(GameObject::Faction::GoodGuys));
        auto skillComponent = new GameObject::SkillSetComponent();
        skillComponent->mSkillSet.push_back(GameObject::SkillState(GameObject::GetSkillDescription(GameObject::SkillId::VenomousGale)));
        skillComponent->mSkillSet.push_back(GameObject::SkillState(GameObject::GetSkillDescription(GameObject::SkillId::PoisonSting)));
        skillComponent->mSkillSet.push_back(GameObject::SkillState(GameObject::GetSkillDescription(GameObject::SkillId::Plague_Ward)));
        skillComponent->mSkillSet.push_back(GameObject::SkillState(GameObject::GetSkillDescription(GameObject::SkillId::PoisonNova)));
        ursa.addComponent(skillComponent);

        ursa.refresh();
        world.getTagManager()->subscribe(TagList::PLayer, ursa);
        level->PlaceCreatureAt(ursa.getId(), playerpos.x, playerpos.y);
    }
    void CreateQoP(artemis::World& world, int x, int y, Dungeon::Level* level)
    {
        artemis::Entity & ursa = world.getEntityManager()->create();
        GameObject::AppearanceComponent* appearance = new GameObject::AppearanceComponent();
        appearance->mAppearance_Ground.mLayer = Layer::Mob;
        appearance->mAppearance_Ground.mSurface = SDL_LoadImage_VFS("data/creatures/Queen_of_Pain_icon.png");
        appearance->mName = "Queen of Pain";

        ursa.addComponent(appearance);
        ursa.addComponent(new GameObject::ControllableComponent(GameObject::Control::keyboard));
        ursa.addComponent(new GameObject::EnergyComponent());
        ursa.addComponent(new GameObject::VisibilityComponent(5));
        ursa.addComponent(new GameObject::MapDiscoveryComponent());
        ursa.addComponent(new GameObject::HealthManaComponent(.25f,.01f));
        ursa.addComponent(new GameObject::CoreStatsComponent(GameObject::CoreMainStat::Int, 16, 1.7f, 18, 2.f, 24, 2.5f));
        ursa.addComponent(new GameObject::CombatStatsComponent(300.f, 25.f, 33.f, -1.05f, 1.7f, 4, 1));
        SDL_Point origin={x,y};
        SDL_Point playerpos = level->FindEmptyPosition(origin, Dungeon::EntityLayer::Creature);
        ursa.addComponent(new GameObject::PositionComponent(playerpos.x, playerpos.y, level, GameObject::PositionLayer::Creature));
        ursa.addComponent(new GameObject::BuffListComponent);
        ursa.addComponent(new GameObject::InventoryComponent);
        ursa.addComponent(new GameObject::ExperienceComponent);
        ursa.addComponent(new GameObject::FactionComponent(GameObject::Faction::GoodGuys));
        auto skillComponent = new GameObject::SkillSetComponent();
        skillComponent->mSkillSet.push_back(GameObject::SkillState(GameObject::GetSkillDescription(GameObject::SkillId::ShadowStrike)));
        skillComponent->mSkillSet.push_back(GameObject::SkillState(GameObject::GetSkillDescription(GameObject::SkillId::Blink)));
        skillComponent->mSkillSet.push_back(GameObject::SkillState(GameObject::GetSkillDescription(GameObject::SkillId::ScreamOfPain)));
        skillComponent->mSkillSet.push_back(GameObject::SkillState(GameObject::GetSkillDescription(GameObject::SkillId::SonicWave)));
        ursa.addComponent(skillComponent);

        ursa.refresh();
        world.getTagManager()->subscribe(TagList::PLayer, ursa);
        level->PlaceCreatureAt(ursa.getId(), playerpos.x, playerpos.y);
    }
    void CreatePlagueWard(artemis::World& world, int x, int y, Dungeon::Level* level, int skillLevel)
    {
        artemis::Entity & mob = world.getEntityManager()->create();
        GameObject::AppearanceComponent* appearance = new GameObject::AppearanceComponent();
        appearance->mAppearance_Ground.mLayer = Layer::Mob;
        appearance->mAppearance_Ground.mSurface = SDL_LoadImage_VFS("data/creatures/Plague_Ward.png");
        appearance->mName = "Plague Ward";

        mob.addComponent(appearance);
        auto controllabeComponent = new GameObject::ControllableComponent(GameObject::Control::IA);
        controllabeComponent->mPossibleActions.push_back(AIActions::attack);
        mob.addComponent(controllabeComponent);
        mob.addComponent(new GameObject::EnergyComponent());
        mob.addComponent(new GameObject::VisibilityComponent(5));
        mob.addComponent(new GameObject::MapDiscoveryComponent());
        static float wardHealth[] = {0.f, 75.f, 200.f, 325.f, 450.f};
        mob.addComponent(new GameObject::HealthManaComponent(0.f,0.f, wardHealth[skillLevel], 0.f));
        static float wardMinDamage[]= {0.f,  9.f, 17.f, 26.f, 34.f};
        static float wardMaxDamage[]= {0.f, 11.f, 21.f, 32.f, 42.f};
        mob.addComponent(new GameObject::CombatStatsComponent(0.f, wardMinDamage[skillLevel], wardMaxDamage[skillLevel], 0.f, 1.5f, 5, 1));
        SDL_Point origin={x,y};
        SDL_Point playerpos = level->FindEmptyPosition(origin, Dungeon::EntityLayer::Creature);
        mob.addComponent(new GameObject::PositionComponent(playerpos.x, playerpos.y, level, GameObject::PositionLayer::Creature));
        mob.addComponent(new GameObject::BuffListComponent);
        mob.addComponent(new GameObject::InventoryComponent);
        mob.addComponent(new GameObject::FactionComponent(GameObject::Faction::GoodGuys));
        auto skillComponent = new GameObject::SkillSetComponent();
        skillComponent->mSkillSet.push_back(GameObject::SkillState(GameObject::GetSkillDescription(GameObject::SkillId::PoisonSting)));
        skillComponent->mSkillSet[0].mSkillLevel = skillLevel;
        mob.addComponent(skillComponent);

        mob.refresh();
        level->PlaceCreatureAt(mob.getId(), playerpos.x, playerpos.y);
    }

    float Roll(const range<float>& parRange)
    {
        SDL_assert(parRange.min <= parRange.max);
        srand (static_cast <unsigned> (time(0)));
        return parRange.min + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(parRange.max-parRange.min)));
    }

    void CreateMob(artemis::World& world,  int x, int y, Dungeon::Level* level, const MobTemplate& mobTemplate)
    {
        artemis::Entity & creep = world.getEntityManager()->create();
        auto appearance = new GameObject::AppearanceComponent();
        appearance->mAppearance_Ground.mLayer = Layer::Mob;
        appearance->mAppearance_Ground.mSurface = SDL_LoadImage_VFS(mobTemplate.mAppearancePath.c_str());
        appearance->mName = mobTemplate.mName;
        creep.addComponent(appearance);

        auto controllabeComponent = new GameObject::ControllableComponent(GameObject::Control::IA);
        controllabeComponent->mPossibleActions = mobTemplate.mPossibleActions;
        creep.addComponent(controllabeComponent);

        creep.addComponent(new GameObject::EnergyComponent());
        creep.addComponent(new GameObject::VisibilityComponent(mobTemplate.mVisibility));

        float maxHealth = Roll(mobTemplate.mMaxHealth);
        float maxMana = Roll(mobTemplate.mMaxMana);
        creep.addComponent(new GameObject::HealthManaComponent( maxHealth, maxMana, maxHealth, maxMana));

        float minDamage = Roll(mobTemplate.mMinDamage);
        creep.addComponent(new GameObject::CombatStatsComponent( Roll(mobTemplate.mMoveSpeed),
            minDamage, 
            minDamage + Roll(mobTemplate.mDamageRange),
            Roll(mobTemplate.mArmor),
            1.7f,
            mobTemplate.mRange,
            mobTemplate.mBaseLevel
            ));
        creep.addComponent(new GameObject::FactionComponent(mobTemplate.mFaction));

        if(mobTemplate.mFaction == GameObject::Faction::GoodGuys)
            creep.addComponent(new GameObject::MapDiscoveryComponent());

        SDL_Point origin={x,y};
        SDL_Point mobPos = level->FindEmptyPosition(origin, Dungeon::EntityLayer::Creature);
        creep.addComponent(new GameObject::PositionComponent(mobPos.x, mobPos.y, level, GameObject::PositionLayer::Creature));

        creep.addComponent(new GameObject::BuffListComponent);
        creep.addComponent(new GameObject::InventoryComponent);
        creep.addComponent(new GameObject::SkillSetComponent);
        creep.refresh();
        level->PlaceCreatureAt(creep.getId(), mobPos.x, mobPos.y);
        if(mobTemplate.mName == "Rushan")
        {
            world.getTagManager()->subscribe(TagList::Rushan, creep);
            GameObject::AddBuffToEntity(world, creep, creep, GameObject::BuffId::IAmRoshan, 0, 0);
        }
    }

    void CreateMobInRoom(artemis::World& world, int minx, int maxx, int miny, int maxy, std::vector<MobTemplate>& mobTable, Dungeon::Level& level, int levelMax)
    {
        int nbMob = (maxx - minx) * (maxy - miny) / 25;
        std::vector< std::pair<int, int> > occupiedTile;
        int levelToReach = levelMax*4;
        int maxMobPerRoom = Random::rand_int(7);
        while(levelToReach > 0 && maxMobPerRoom > 0)
        {
            if(mobTable.empty()) break;
            std::pair<int, int> coord = std::make_pair(0, 0);
            bool occupied = true;
            int nb_loop = 0;
            while(occupied && nb_loop < 150)
            {
                ++nb_loop;
                coord.first = Random::rand_range(minx, maxx);
                coord.second = Random::rand_range(miny, maxy);
                std::vector< std::pair<int, int> >::iterator it = occupiedTile.begin();
                occupied = (level.GetCreature(coord.first, coord.second) != -1);
                while(!occupied && it != occupiedTile.end())
                {
                    occupied = (it->first == coord.first && it->second == coord.second);
                    ++it;
                }
            }
            if(occupied)
                return;
            std::vector<MobTemplate*> validMobList;
            for(int i = 0; i < mobTable.size(); ++i)
            {
                if(mobTable[i].mBaseLevel <= levelToReach)
                    validMobList.push_back(&mobTable[i]);
            }
            if(validMobList.empty())
                return;
            int indexMob = validMobList.size()<=1?0:Random::rand_int(validMobList.size());
            MobTemplate& mobTemplate = *validMobList[indexMob];
            if(mobTemplate.mIsHero)
            {
                std::vector<MobTemplate>::iterator it = std::find(mobTable.begin(), mobTable.end(), mobTemplate);
                SDL_assert(it != mobTable.end());
                mobTable.erase(it);
            }
            CreateMob(world,  coord.first, coord.second, &level, mobTemplate);
            maxMobPerRoom--;
            levelToReach -= mobTemplate.mBaseLevel;
        }
    }

    void CreateItemOnGround(artemis::World& world,  int x, int y, Dungeon::Level* level, const GameObject::ItemTemplate& itemTemplate, GameObject::ItemState* itemState)
    {
        artemis::Entity & item = world.getEntityManager()->create();
        GameObject::AppearanceComponent* appearance = new GameObject::AppearanceComponent();
        appearance->mAppearance_Ground.mLayer = Layer::Items;
        appearance->mAppearance_Ground.mSurface = SDL_LoadImage_VFS(itemTemplate.AppearancePath_Ground.c_str());
        item.addComponent(appearance);
        SDL_Point origin={x,y};
        SDL_Point itemDropPos = level->FindEmptyPosition(origin, Dungeon::EntityLayer::Item);

        item.addComponent(new GameObject::PositionComponent(itemDropPos.x, itemDropPos.y, level, GameObject::PositionLayer::Item));
        if(itemState)
        {
            item.addComponent(new GameObject::ItemInfoComponent(itemState));
        }
        else
        {
            GameObject::ItemState* useItemState = new GameObject::ItemState(itemTemplate);
            if(itemTemplate.ActiveSkill != GameObject::SkillId::None)
            {
                useItemState->mSkill = GetItemSkillState(itemTemplate.ActiveSkill);
            }
            item.addComponent(new GameObject::ItemInfoComponent(useItemState));
        }
        item.refresh();
        level->PlaceItemAt(item.getId(), itemDropPos.x, itemDropPos.y);
    }

    void CreateProjectile(artemis::World& world,  int fromX, int fromY,  int toX, int toY, Dungeon::Level* level, char* imgpath)
    {
        artemis::Entity & item = world.getEntityManager()->create();
        auto appearance = new GameObject::AppearanceComponent();
        appearance->mAppearance_Ground.mPosX = fromX * Dungeon::GetTileSizeX();
        appearance->mAppearance_Ground.mPosY = fromY * Dungeon::GetTileSizeY();
        appearance->mAppearance_Ground.mLayer = Layer::Mob;
        appearance->mAppearance_Ground.mSurface = SDL_LoadImage_VFS(imgpath);
        appearance->mDiesAtDestination = true;
        auto position = new GameObject::PositionComponent(toX, toY, level, GameObject::PositionLayer::Projectiles);
        position->mNextMoveIsTeleport = false;
        item.addComponent(appearance);
        item.addComponent(position);
        item.refresh();
    }
}
