#include "RandomGenerator.h"

#include "SDL_assert.h"
#include <random>
#include <ctime>

namespace DotaRL { namespace Random {
    static std::default_random_engine& random_engine()
    {
        static std::default_random_engine re((unsigned int)std::time(0));
        return re;
    }

    int rand_range(int low, int high)
    {
        SDL_assert(low < high);
        std::uniform_int_distribution<int> uid(low, high-1);
        return uid(random_engine());
    }

    int rand_int(int high)
    {
        SDL_assert(0 < high);
        std::uniform_int_distribution<int> uid(0, high-1);
        return uid(random_engine());
    }

    int d100()
    {
        static std::uniform_int_distribution<int> uid(0, 99);
        return uid(random_engine());
    }
}
}
