#pragma once

#include <cstdint>
#include <vector>

#include "Artemis/Component.h"
#include "SkillDescription.h"

namespace DotaRL{ namespace GameObject {
    struct SkillState
    {
        SkillState(const SkillDescription* skillDescription)
            : mSkillDescription(skillDescription)
            , mDateEndCooldown(0)
            , mSkillLevel(0)
        {
        }
        const SkillDescription* mSkillDescription;
        int64_t mDateEndCooldown;
        int mSkillLevel;
    };

    class SkillSetComponent : public artemis::Component
    {
    public:
        SkillSetComponent();

        std::vector<SkillState> mSkillSet;
    };

}}
