#include "StatsSystem.h"
#include "Buffs.h"
#include "PositionComponent.h"
#include "EnergySystem.h"
#include "Artemis/Entity.h"

#include "SDL_assert.h"

namespace DotaRL { namespace GameObject
{
    const auto BaseHealthForHero = 150.f;
    const auto HealthPerStr = 19.f;
    const auto ManaPerInt = 13.f;
    const auto HealthRegenPerStr = 0.03f;
    const auto ManaRegenPerInt = 0.04f;
    const auto AgiForArmorBonus = 7.f;
    const auto MinIncreaseAttackSpeed = -80.f;
    const auto MaxIncreaseAttackSpeed = 400.f;
    const auto MaxMoveSlow = .2f;
    const auto MaxMoveSpeed = 522.f;

    const int64_t HealingPeriod = 100;
    const auto HealingFactorPerPeriod = 0.1f;

    StatsSystem::StatsSystem(EnergySystem* energySystem) :
        mEnergySystem(energySystem),
        mDateNextRegen(-1)
    {
        addComponentType<HealthManaComponent>();
        addComponentType<CombatStatsComponent>();
        addComponentType<BuffListComponent>();
    }

    void StatsSystem::initialize()
    {
        mHealthManaMapper.init(*world);
        mCombatStatsMapper.init(*world);
        mCoreStatsMapper.init(*world);
        mBuffListMapper.init(*world);

        mDateNextRegen = 0;
    }

    bool StatsSystem::checkProcessing()
    {
        return mDateNextRegen <= mEnergySystem->CurrentDate();
    }

    void StatsSystem::processEntity(artemis::Entity &e)
    {
        CombatStatsComponent* combatStatsComponent = mCombatStatsMapper.get(e);
        HealthManaComponent* healthManaComponent = mHealthManaMapper.get(e);
        BuffListComponent* buffListComponent = mBuffListMapper.get(e);
        SDL_assert(combatStatsComponent);
        SDL_assert(healthManaComponent);
        SDL_assert(buffListComponent);

        // Note : BaseAttackTime est modifie directement par les buffs et on priera pour que le joueur je cumule pas Chemical Rage avec la transfo de Terrorblade

        combatStatsComponent->BaseMoveSpeed = combatStatsComponent->IntrinsicMoveSpeed;
        combatStatsComponent->BaseMinDamage = combatStatsComponent->IntrinsicMinDamage;
        combatStatsComponent->BaseMaxDamage = combatStatsComponent->IntrinsicMaxDamage;
        combatStatsComponent->BaseArmor = combatStatsComponent->IntrinsicArmor;
        // TODO : appliquer les buffs qui modifient ces stats

        CoreStatsComponent* coreStatsComponent = mCoreStatsMapper.get(e);
        if (coreStatsComponent)
        {
            coreStatsComponent->BaseStr = coreStatsComponent->IntrinsicStr + static_cast<int>((combatStatsComponent->Level-1) * coreStatsComponent->StrPerLevel);
            coreStatsComponent->BaseAgi = coreStatsComponent->IntrinsicAgi + static_cast<int>((combatStatsComponent->Level-1) * coreStatsComponent->AgiPerLevel);
            coreStatsComponent->BaseInt = coreStatsComponent->IntrinsicInt + static_cast<int>((combatStatsComponent->Level-1) * coreStatsComponent->IntPerLevel);
            // TODO : appliquer effets des buffs qui modifient les base stats pour les rares qui existent

            SDL_assert(coreStatsComponent->BaseStr >= 1);
            SDL_assert(coreStatsComponent->BaseAgi >= 1);
            SDL_assert(coreStatsComponent->BaseInt >= 1);

            coreStatsComponent->Str = coreStatsComponent->BaseStr;
            coreStatsComponent->Agi = coreStatsComponent->BaseAgi;
            coreStatsComponent->Int = coreStatsComponent->BaseInt;
            for(auto b : buffListComponent->Buffs)
                b->ApplyStatBonuses(coreStatsComponent);

            coreStatsComponent->Str = std::max(1, coreStatsComponent->Str);
            coreStatsComponent->Agi = std::max(1, coreStatsComponent->Agi);
            coreStatsComponent->Int = std::max(1, coreStatsComponent->Int);

            healthManaComponent->MaxHealth = BaseHealthForHero + HealthPerStr * coreStatsComponent->Str;
            healthManaComponent->MaxMana = ManaPerInt * coreStatsComponent->Int;
            healthManaComponent->BaseHealthRegen = healthManaComponent->IntrinsicHealthRegen + coreStatsComponent->Str * HealthRegenPerStr;
            healthManaComponent->BaseManaRegen = healthManaComponent->IntrinsicManaRegen + coreStatsComponent->Int * ManaRegenPerInt;
            // TODO : appliquer effet des buffs qui modifient la base regen (Alchemist Rage est le seul que je connaisse)

            healthManaComponent->HealthRegen = healthManaComponent->BaseHealthRegen;
            healthManaComponent->ManaRegen = healthManaComponent->BaseManaRegen;
            // TODO : appliquer effet des buffs qui modifient la regen finale

            int stat_bonus_damage = ComputeCoreStatAttackBonus(*coreStatsComponent);
            combatStatsComponent->BaseMinDamage += stat_bonus_damage;
            combatStatsComponent->BaseMaxDamage += stat_bonus_damage;
            combatStatsComponent->BaseArmor += coreStatsComponent->Agi / AgiForArmorBonus;
            combatStatsComponent->BaseIAS = static_cast<float>(coreStatsComponent->Agi);
        }

        for(auto b : buffListComponent->Buffs)
            b->ApplyBaseHealthBonuses(healthManaComponent);

        for(auto b : buffListComponent->Buffs)
            b->ApplyFinalHealthBonuses(healthManaComponent);

        healthManaComponent->MaxHealth = std::max(1.f, healthManaComponent->MaxHealth);
        healthManaComponent->MaxMana = std::max(0.f, healthManaComponent->MaxMana);

        if (healthManaComponent->LastMaxHealth != 0.f && healthManaComponent->MaxHealth != healthManaComponent->LastMaxHealth)
            healthManaComponent->CurrentHealth = healthManaComponent->CurrentHealth * healthManaComponent->MaxHealth / healthManaComponent->LastMaxHealth;
        if (healthManaComponent->LastMaxMana != 0.f && healthManaComponent->MaxMana != healthManaComponent->LastMaxMana)
            healthManaComponent->CurrentMana = healthManaComponent->CurrentMana * healthManaComponent->MaxMana / healthManaComponent->LastMaxMana;

        healthManaComponent->LastMaxHealth = healthManaComponent->MaxHealth;
        healthManaComponent->LastMaxMana = healthManaComponent->MaxMana;

        combatStatsComponent->MoveSpeed = combatStatsComponent->BaseMoveSpeed;
        combatStatsComponent->MinDamage = combatStatsComponent->BaseMinDamage;
        combatStatsComponent->MaxDamage = combatStatsComponent->BaseMaxDamage;
        combatStatsComponent->Armor = combatStatsComponent->BaseArmor;
        combatStatsComponent->IAS = combatStatsComponent->BaseIAS;

        for(auto b : buffListComponent->Buffs)
            b->ApplyGreenCombatStatBonuses(combatStatsComponent, healthManaComponent);

        // cap de l'IAS
        combatStatsComponent->IAS = std::min(MaxIncreaseAttackSpeed, std::max(MinIncreaseAttackSpeed, combatStatsComponent->IAS));

        // cap des slow
        combatStatsComponent->MoveSpeed = std::min(MaxMoveSpeed, std::max(combatStatsComponent->MoveSpeed, combatStatsComponent->BaseMoveSpeed * MaxMoveSlow));

        if (mEnergySystem->CurrentDate() >= mDateNextRegen)
        {
            if (healthManaComponent->CurrentHealth < healthManaComponent->MaxHealth)
                healthManaComponent->CurrentHealth = std::min(healthManaComponent->MaxHealth, healthManaComponent->CurrentHealth + HealingFactorPerPeriod*healthManaComponent->HealthRegen);

            if (healthManaComponent->CurrentMana < healthManaComponent->MaxMana)
                healthManaComponent->CurrentMana = std::min(healthManaComponent->MaxMana, healthManaComponent->CurrentMana + HealingFactorPerPeriod*healthManaComponent->ManaRegen);
        }
    }

    void StatsSystem::end()
    {
        if (mEnergySystem->CurrentDate() >= mDateNextRegen)
            mDateNextRegen += HealingPeriod;
        SDL_assert(mEnergySystem->CurrentDate() < mDateNextRegen);
    }
}
}
