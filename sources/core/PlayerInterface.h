#pragma once

#include <map>
#include <vector>
#include "SDL_ttf.h"
#include "Artemis/World.h"

#include "StatsComponent.h"
#include "SkillSetComponent.h"
#include "InventoryComponent.h"
#include "SkillSetComponent.h"

struct SDL_Texture;
struct SDL_Surface;
struct ItemState;
namespace DotaRL
{
    enum class MainStat
    {
        Str,
        Agi,
        Int
    };

    struct itemInfo
    {
        itemInfo(int cooldown, int maxCoolDown, SDL_Surface* surface, int charges)
            : mCooldown(cooldown)
            , mMaxCoolDown(maxCoolDown)
            , mSurface(surface)
            , mCharges(charges)
        {
        }
        int mCooldown;
        int mMaxCoolDown;
        int mCharges;
        SDL_Surface* mSurface;
    };
    struct PlayerInterfaceData
    {
        int mLifePoints;
        int mMaxLifePoints;
        int mManaPoints;
        int mMaxManaPoints;
        std::vector<GameObject::SkillState*> mSkillList;
        MainStat mMainStat;
        std::vector<itemInfo> mItemList;
        int mAgility;
        int mIntelligence;
        int mStrength;
        int mLevel;
        int skillUnlearn;
    };

    void DrawText(const char* text, int x, int y, int w, int h, int r, int g, int b, int a, TTF_Font* police);
    void DrawRect(int x, int y, int w, int h, int r, int g, int b, int a);

    enum class ReticleType
    {
        Valid,
        Invalid,
        Area
    };

    class PlayerInterfaceRenderer
    {
    public:
        PlayerInterfaceRenderer();
        ~PlayerInterfaceRenderer();

        void Process(const PlayerInterfaceData& playerData, int64_t currentDate);
        void RenderTargetting(ReticleType typeReticle, int x, int y);

    private:
        void DrawLifeBar(const PlayerInterfaceData& playerData);
        void DrawManaBar(const PlayerInterfaceData& playerData);
        void DrawLevel(const PlayerInterfaceData& playerData);
        void DrawStrStat(const PlayerInterfaceData& playerData);
        void DrawAgiStat(const PlayerInterfaceData& playerData);
        void DrawIntStat(const PlayerInterfaceData& playerData);
        void DrawInventory(const PlayerInterfaceData& playerData);
        void DrawSkills(const PlayerInterfaceData& playerData, int64_t currentDate);
        TTF_Font* mPolice;
        SDL_Texture* mSelectionFeedback;
        SDL_Texture* mSelectionFeedbackError;
        SDL_Texture* mSelectionFeedbackArea;
    };

    class PlayerInterfaceController
    {
    public:
        PlayerInterfaceController();
        ~PlayerInterfaceController();

        void Process(PlayerInterfaceData& playerData, artemis::World& world);
        void Initialise(artemis::World& world);

    private:
        artemis::ComponentMapper<GameObject::HealthManaComponent> mHealthManaMapper;
        artemis::ComponentMapper<GameObject::CoreStatsComponent> mCoreStatsMapper;
        artemis::ComponentMapper<GameObject::InventoryComponent> mInventoryMapper;
        artemis::ComponentMapper<GameObject::SkillSetComponent> mSkillSetMapper;
        artemis::ComponentMapper<GameObject::CombatStatsComponent> mCombatStatsMapper;
    };

    class InventoryRenderer
    {
    public:
        InventoryRenderer();
        ~InventoryRenderer();

        void Process(const PlayerInterfaceData& playerData, int64_t currentDate);
        void RenderTargetting(ReticleType typeReticle, int x, int y);

    private:
        TTF_Font* mPolice;
    };
}
