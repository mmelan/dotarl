#pragma once

template<class TYPE>
class TSingleton
{
public:
    static bool HasInstance();
    static void CreateInstance();
    static TYPE& Instance();
    static void DestroyInstance();

protected:
    TSingleton();
    ~TSingleton();

private:
    static TYPE* FInstance;
};

template<class TYPE>
TYPE* TSingleton<TYPE>::FInstance = nullptr;
template<class TYPE>
TSingleton<TYPE>::TSingleton()
{
}

template<class TYPE>
TSingleton<TYPE>::~TSingleton()
{
}

template<class TYPE>
bool TSingleton<TYPE>::HasInstance()
{
    return FInstance != NULL;
}

template<class TYPE>
void TSingleton<TYPE>::CreateInstance()
{
    FInstance = new TYPE();
}

template<class TYPE>
void TSingleton<TYPE>::DestroyInstance()
{
    delete FInstance;
    FInstance =  NULL;
}
    
template<class TYPE>
TYPE& TSingleton<TYPE>::Instance()
{
    return *FInstance;
}
