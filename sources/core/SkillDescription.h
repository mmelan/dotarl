#pragma once

#include <vector>
#include "Buffs.h"
#include "DamageRules.h"
struct SDL_Surface;
namespace DotaRL { namespace GameObject 
{
    enum class SkillShape
    {
        Instant,
        PBAoE,
        GroundTarget,
        Target,
        Ray
    };
    enum class SkillId
    {
        None,
        Earthshock,
        Overpower,
        Fury_Swipes,
        Enrage,

        Plague_Ward,
        PoisonNova,
        PoisonSting,
        VenomousGale,

        ShadowStrike,
        Blink,
        ScreamOfPain,
        SonicWave,
        
        HealingSalve,
        Clarity,
        TPScoll
    };
    struct Damage
    {
        Damage() :
            Amount(0),
            Type(DamageType::Magic)
        {}
        Damage(int a)
            : Amount(a)
            , Type(DamageType::Magic)
        {}
        Damage(int a, DamageType t)
            : Amount(a)
            , Type(t)
        {}
        int Amount;
        DamageType Type;
    };
    struct LevelRequirement
    {
        int Rank1;
        int Rank2;
        int Rank3;
        int Rank4;
    };
    template<class T> struct SkillLevelDependentStatTemplate
    {
        SkillLevelDependentStatTemplate()
        {
            Stat.resize(4);
        }
        SkillLevelDependentStatTemplate(T r1)
        {
            Stat.clear();
            Stat.push_back(r1);
            Stat.push_back(r1);
            Stat.push_back(r1);
            Stat.push_back(r1);
        }
        SkillLevelDependentStatTemplate(T r1, T r2, T r3)
        {
            Stat.clear();
            Stat.push_back(r1);
            Stat.push_back(r2);
            Stat.push_back(r3);
        }
        SkillLevelDependentStatTemplate(T r1, T r2, T r3, T r4)
        {
            Stat.clear();
            Stat.push_back(r1);
            Stat.push_back(r2);
            Stat.push_back(r3);
            Stat.push_back(r4);
        }
        T operator[](int index) const
        {
            return Stat[index-1];
        }

        std::vector<T> Stat;
    };
    typedef SkillLevelDependentStatTemplate<int> SkillLevelDependentStat;
    typedef SkillLevelDependentStatTemplate<Damage> SkillLevelDependentDamage;
    struct SkillDescription
    {
        SkillDescription(SkillId skillId);
        ~SkillDescription();

        SkillId mSkillId;
        SkillLevelDependentStat mCooldown;
        int CastTime;
        SkillShape Shape;
        SkillLevelDependentStat Range;
        SkillLevelDependentStat mManaCost;
        SkillLevelDependentDamage mDamages;
        LevelRequirement mLevelRequirement;
        bool isActivable;
        BuffId mPassiveBuff;
        BuffId mActiveBuff;
        SDL_Surface* mAppearance;
    };

    const SkillDescription* GetSkillDescription(SkillId skillId);
}}
