#pragma once

#include "dungeon.h"

#include "Artemis/Component.h"

namespace DotaRL { namespace GameObject
{
    enum class PositionLayer
    {
        Creature,
        Item,
        Projectiles
    };

    class PositionComponent : public artemis::Component
    {
    public:
        PositionComponent(int gridPosX, int gridPosY, Dungeon::Level* level, PositionLayer layer);

        int mGridPosX;
        int mGridPosY;
        Dungeon::Level* mLevel;
        bool mNextMoveIsTeleport;
        PositionLayer mLayer;
        SDL_Point AttackedTile;
    };
}
}
