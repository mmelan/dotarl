#pragma once

#include "Artemis/EntityProcessingSystem.h"
#include "Artemis/ComponentMapper.h"
#include "ControllableComponent.h"
#include "PositionComponent.h"
#include "EnergyComponent.h"
#include "VisibilityComponent.h"
#include "FactionComponent.h"
#include "SkillSetComponent.h"
#include "ItemInfoComponent.h"
#include "InventoryComponent.h"
#include "StatsComponent.h"
#include "dungeon.h"

namespace DotaRL {
    class PlayerInterfaceRenderer;
}

namespace DotaRL { namespace GameObject
{
    class BuffListComponent;
    class EnergySystem;

    class ControleSystem : public artemis::EntityProcessingSystem
    {

    public:
        ControleSystem(EnergySystem* energySystem);

        virtual void initialize();
        virtual void processEntity(artemis::Entity &e) override {}
        bool ProcessEntity(artemis::Entity &e);

        void DoRenderTargeting(PlayerInterfaceRenderer* renderer);

        int mEntityPlaying;
        int targetSelectionX;
        int targetSelectionY;

    private:
        void KeyboardControle(artemis::Entity &e);
        void IAControle(artemis::Entity &e);
        bool TryMoveInto(artemis::Entity &e, int dx, int dy);
        void Wait(artemis::Entity &e);
        void TryPickUp(artemis::Entity &e);
        void TryDrop(artemis::Entity &e, int index);
        void TryLevelUp(artemis::Entity &e, int index);
        void TryUseSkill(artemis::Entity &e, int index);
        bool TryAttack(artemis::Entity &e, int x, int y);
        void TryUseItem(artemis::Entity &e, int index);
        void TryRest(artemis::Entity &e);

        bool RestIfn(artemis::Entity &e);

        bool IsPossible(AIActions action, artemis::Entity &e);
        void AI_Attack(artemis::Entity &e);

        void HandleTargetSeletion(artemis::Entity &e);
        void TryMoveTargetSelection(artemis::Entity &e, int dx, int dy);
        void InitTargetSelection(artemis::Entity& e, int range, SkillShape shape);
        void ResetTargetSelection();
        void GetViableTargets(artemis::Entity& attacker, std::vector<artemis::Entity*>& targets);
        bool mTargetSelecting;
        bool mValidTargetPoint;
        int mTargetRangeLimit;
        int mCurrentSkillTargeting;
        SkillShape mTargetShape;

        EnergySystem* mEnergySystem;
        artemis::ComponentMapper<ControllableComponent> mControllableMapper;
        artemis::ComponentMapper<PositionComponent> mPositionMapper;
        artemis::ComponentMapper<EnergyComponent> mEnergyMapper;
        artemis::ComponentMapper<VisibilityComponent> mVisibitilyMapper;
        artemis::ComponentMapper<FactionComponent> mFactionMapper;
        artemis::ComponentMapper<InventoryComponent> mInventoryMapper;
        artemis::ComponentMapper<BuffListComponent> mBuffListMapper;
        artemis::ComponentMapper<CombatStatsComponent> mCombatStatsMapper;
        artemis::ComponentMapper<SkillSetComponent> mSkillSetMapper;
        artemis::ComponentMapper<ItemInfoComponent> mIteminfoMapper;
        artemis::ComponentMapper<HealthManaComponent> mManaHealthMapper;
    };
}
}
