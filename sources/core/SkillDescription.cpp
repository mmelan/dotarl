#include "SkillDescription.h"
#include <map>
#include "SDL_assert.h"
#include "SDL_Helper.h"

namespace DotaRL { namespace GameObject
{
    const LevelRequirement& NormalSkill()
    {
        static struct LevelRequirement r = {1, 3, 5, 7};
        return r;
    }
    const LevelRequirement& UltimateSkill()
    {
        static struct LevelRequirement r = {6, 11, 16, 26};
        return r;
    }
    SkillDescription::SkillDescription(SkillId skillId)
        : mSkillId(skillId)
        , mCooldown(0)
        , isActivable(false)
        , mAppearance(nullptr)
        , mPassiveBuff(BuffId::Undefined)
    {
    }
    SkillDescription::~SkillDescription()
    {
    }

    std::map<SkillId, SkillDescription> GetSkillMap()
    {
        std::map<SkillId, SkillDescription> result;
        {
            SkillDescription skill(SkillId::Earthshock);
            skill.mCooldown = 6000;
            skill.isActivable = true;
            skill.Shape = SkillShape::PBAoE;
            skill.Range = 1;
            skill.CastTime = 300;
            skill.mAppearance = SDL_LoadImage_VFS("data/skills/Earthshock_icon.png");;
            skill.mManaCost = 75;
            skill.mDamages = SkillLevelDependentDamage(90, 140, 190, 240);
            skill.mActiveBuff = BuffId::EarthshockDebuff;
            skill.mLevelRequirement = NormalSkill();
            result.emplace(skill.mSkillId, skill);
        }
        {
            SkillDescription skill(SkillId::Overpower);
            skill.mCooldown = 10000;
            skill.CastTime = 300;
            skill.isActivable = true;
            skill.Shape = SkillShape::Instant;
            skill.mManaCost = SkillLevelDependentStat(45, 55, 65, 75);
            skill.mAppearance = SDL_LoadImage_VFS("data/skills/Overpower_icon.png");;
            skill.mLevelRequirement = NormalSkill();
            skill.mActiveBuff = BuffId::OverpowerBuff;
            result.emplace(skill.mSkillId, skill);
        }
        {
            SkillDescription skill(SkillId::Fury_Swipes);
            skill.mCooldown = 0;
            skill.isActivable = false;
            skill.mAppearance = SDL_LoadImage_VFS("data/skills/Fury_Swipes_icon.png");;
            skill.mPassiveBuff = BuffId::FurySwipesPassive;
            skill.mLevelRequirement = NormalSkill();
            result.emplace(skill.mSkillId, skill);
        }
        {
            SkillDescription skill(SkillId::Enrage);
            skill.mCooldown = 25000;
            skill.CastTime = 300;
            skill.isActivable = true;
            skill.Shape = SkillShape::Instant;
            skill.mManaCost = 0;
            skill.mActiveBuff = BuffId::Enrage;
            skill.mAppearance = SDL_LoadImage_VFS("data/skills/Enrage_icon.png");;
            skill.mLevelRequirement = UltimateSkill();
            result.emplace(skill.mSkillId, skill);
        }
        //Venomancer
        {
            SkillDescription skill(SkillId::VenomousGale);
            skill.mCooldown = 22000;
            skill.CastTime = 0;
            skill.isActivable = true;
            skill.Shape = SkillShape::Ray;
            skill.Range = 5;
            skill.mManaCost = 125;
            skill.mDamages = SkillLevelDependentDamage(25, 50, 75, 100);
            skill.mActiveBuff = BuffId::VenomousGale;
            skill.mAppearance = SDL_LoadImage_VFS("data/skills/venomousgale.png");;
            skill.mLevelRequirement = NormalSkill();
            result.emplace(skill.mSkillId, skill);
        }
        {
            SkillDescription skill(SkillId::PoisonSting);
            skill.mCooldown = 0;
            skill.CastTime = 0;
            skill.isActivable = false;
            skill.mPassiveBuff = BuffId::PoisonStingPassive;
            skill.mAppearance = SDL_LoadImage_VFS("data/skills/poison-sting.png");;
            skill.mLevelRequirement = NormalSkill();
            result.emplace(skill.mSkillId, skill);
        }
        {
            SkillDescription skill(SkillId::Plague_Ward);
            skill.mCooldown = 5000;
            skill.CastTime = 0;
            skill.mManaCost = 20;
            skill.isActivable = true;
            skill.Shape = SkillShape::GroundTarget;
            skill.Range = 4;
            skill.mAppearance = SDL_LoadImage_VFS("data/skills/plague-ward.png");;
            skill.mLevelRequirement = NormalSkill();
            result.emplace(skill.mSkillId, skill);
        }
        {
            SkillDescription skill(SkillId::PoisonNova);
            skill.mCooldown = SkillLevelDependentStat(140000, 120000, 100000);
            skill.CastTime = 0;
            skill.isActivable = true;
            skill.Shape = SkillShape::PBAoE;
            skill.Range = 3;
            skill.mManaCost = SkillLevelDependentStat(200, 300, 400);
            skill.mActiveBuff = BuffId::PoisonNova;
            skill.mAppearance = SDL_LoadImage_VFS("data/skills/poisonnova.png");;
            skill.mLevelRequirement = UltimateSkill();
            result.emplace(skill.mSkillId, skill);
        }
        //Queen of Pain
        {
            SkillDescription skill(SkillId::ShadowStrike);
            skill.mCooldown = SkillLevelDependentStat(20000, 16000, 12000, 8000);
            skill.CastTime = 500;
            skill.mManaCost = 110;
            skill.Shape = SkillShape::Target;
            skill.Range = 3;
            skill.isActivable = true;
            skill.mDamages = SkillLevelDependentDamage(50, 75, 100, 125);
            skill.mActiveBuff = BuffId::ShadowStrike;
            skill.mAppearance = SDL_LoadImage_VFS("data/skills/Shadow_Strike_icon.png");;
            skill.mLevelRequirement = NormalSkill();
            result.emplace(skill.mSkillId, skill);
        }
        {
            SkillDescription skill(SkillId::Blink);
            skill.mCooldown = SkillLevelDependentStat(12000, 10000, 8000, 6000);
            skill.CastTime = 500;
            skill.mManaCost = 60;
            skill.Shape = SkillShape::GroundTarget;
            skill.Range = SkillLevelDependentStat(3, 4, 5, 6);
            skill.isActivable = true;
            skill.mActiveBuff = BuffId::Undefined;
            skill.mAppearance = SDL_LoadImage_VFS("data/skills/Blink_QoP_icon.png");;
            skill.mLevelRequirement = NormalSkill();
            result.emplace(skill.mSkillId, skill);
        }
        {
            SkillDescription skill(SkillId::ScreamOfPain);
            skill.mCooldown = 7000;
            skill.CastTime = 0;
            skill.mManaCost = SkillLevelDependentStat(110, 120, 130, 140);
            skill.mDamages = SkillLevelDependentDamage(85, 165, 225, 300);
            skill.isActivable = true;
            skill.Shape = SkillShape::PBAoE;
            skill.Range = 2;
            skill.mActiveBuff = BuffId::Undefined;
            skill.mAppearance = SDL_LoadImage_VFS("data/skills/Scream_of_Pain_icon.png");;
            skill.mLevelRequirement = NormalSkill();
            result.emplace(skill.mSkillId, skill);
        }
        {
            SkillDescription skill(SkillId::SonicWave);
            skill.mCooldown = 135000;
            skill.CastTime = 500;
            skill.Shape = SkillShape::Ray;
            skill.Range = 5;
            skill.mDamages = SkillLevelDependentDamage(350, 475, 600);
            skill.mManaCost = SkillLevelDependentStat(250, 360, 500);
            skill.isActivable = true;
            skill.mActiveBuff = BuffId::Undefined;
            skill.mAppearance = SDL_LoadImage_VFS("data/skills/Sonic_Wave_icon.png");;
            skill.mLevelRequirement = UltimateSkill();
            result.emplace(skill.mSkillId, skill);
        }

        {
            SkillDescription skill(SkillId::HealingSalve);
            skill.mCooldown = 0;
            skill.CastTime = 0;
            skill.Shape = SkillShape::Instant;
            skill.Range = 0;
            skill.mManaCost = SkillLevelDependentStat(0);
            skill.isActivable = true;
            skill.mActiveBuff = BuffId::Undefined;
            skill.mAppearance = SDL_LoadImage_VFS("data/items/Healing_Salve_icon.png");;
            skill.mLevelRequirement = NormalSkill();
            result.emplace(skill.mSkillId, skill);
        }

        {
            SkillDescription skill(SkillId::Clarity);
            skill.mCooldown = 0;
            skill.CastTime = 0;
            skill.Shape = SkillShape::Instant;
            skill.Range = 0;
            skill.mManaCost = SkillLevelDependentStat(0);
            skill.isActivable = true;
            skill.mActiveBuff = BuffId::Undefined;
            skill.mAppearance = SDL_LoadImage_VFS("data/items/Clarity_icon.png");;
            skill.mLevelRequirement = NormalSkill();
            result.emplace(skill.mSkillId, skill);
        }
        return result;
    }

    const SkillDescription* GetSkillDescription(SkillId skillId)
    {
        static const std::map<SkillId, SkillDescription> skillMap = GetSkillMap();
        return &skillMap.at(skillId);
    }

}}