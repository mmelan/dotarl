#pragma once

#include "Artemis/EntityProcessingSystem.h"
#include "Artemis/ComponentMapper.h"
#include "AppearanceComponent.h"
#include "PositionComponent.h"
#include "StatsComponent.h"
#include "FactionComponent.h"
#include "dungeon.h"

struct SDL_Surface;
struct SDL_Texture;
namespace DotaRL { namespace GameObject
{
    class FeedbackSystem : public artemis::EntityProcessingSystem
    {

    public:
        FeedbackSystem();

        virtual void initialize();
        virtual void processEntity(artemis::Entity &e);
        
        void SetDungeon(Dungeon::Level* dungeonLvl) { mDungeonLvl = dungeonLvl; }
    private:
        artemis::ComponentMapper<AppearanceComponent> mAppearanceMapper;
        artemis::ComponentMapper<PositionComponent> mPositionMapper;
        artemis::ComponentMapper<HealthManaComponent> mHealthManaMapper;
        artemis::ComponentMapper<FactionComponent> mFactionMapper;
        SDL_Surface* mlifeBarSurface_GoodGuys;
        SDL_Surface* mlifeBarSurfaceEnemy_EvilForeigner;
        SDL_Surface* mlifeBarSurfaceBg;
        Dungeon::Level* mDungeonLvl;
    };
}
}
