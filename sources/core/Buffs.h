#pragma once

#include <vector>
#include <cstdint>
#include "Artemis/Component.h"
#include "Artemis/Entity.h"
#include "Artemis/EntityProcessingSystem.h"
#include "Artemis/ComponentMapper.h"

namespace DotaRL { namespace GameObject {
    class CombatStatsComponent;
    class HealthManaComponent;
    class EnergySystem;
    class BuffListComponent;
    class CoreStatsComponent;

    enum class OrbEffectId
    {
        Desolator,
        Mjollnir,
        OrbOfVenom,
        Skadi,
        LifeSteal15,
        LifeSteal20,
        LifeSteal25,
        FurySwipes
    };

    enum class BuffId // Maste mega unmaintainable enum !
    {
        Undefined,
        Enrage,
        FurySwipesDebuff,
        FurySwipesPassive,
        EarthshockDebuff,
        OverpowerBuff,

        VenomousGale,
        PoisonStingPassive,
        PoisonStingDebuff,
        PoisonNova,

        ShadowStrike,

        IAmRoshan,
        IGrowStronger,

        RingOfHealth,
        VoidStone,
        Perseverance,
        Daedalus,
        Desolator,
        DesolatorDebuff,
        Sange,
        Yasha,
        SangeYasha,
        LesserMaim,
        GreaterMaim,
        OrchidMalevolence
    };

    struct AttackRollSummary
    {
        AttackRollSummary();

        float CritBonus;
        std::vector<BuffId> BuffToPlace;
    };

    enum class BuffStackMode
    {
        Independent,
        Refresh
    };

    class Buff
    {
    public:
        Buff(BuffId id);

        virtual void ApplyGreenCombatStatBonuses(CombatStatsComponent* combatComponent, const HealthManaComponent* healthComponent);
        virtual void ApplyBaseHealthBonuses(HealthManaComponent* healthComponent);
        virtual void ApplyFinalHealthBonuses(HealthManaComponent* healthComponent);
        virtual void AttackLaunched(std::vector<OrbEffectId>& orbs, AttackRollSummary& attackRollSummary, artemis::Entity& target);
        virtual void AttackLanded(artemis::World& world, artemis::Entity& target, int64_t currentDate);
        virtual void ApplyBuffBonusDamage(int* damage, BuffListComponent* targetBuffs);
        virtual void OnPeriodicEffect(artemis::World& world);
        virtual void ApplyStatBonuses(CoreStatsComponent* coreStats);

        int Creator;
        int Carrier;
        int EntityId;
        int Duration;
        int MaxStacks;
        int Charges;
        int PeriodicEffect;
        int64_t DateNextWakeUp;
        BuffStackMode StackMode;
        BuffId GetId() const { return mId; }
    protected:
        const BuffId mId;
    };

    class BuffComponent : public artemis::Component
    {
    public:
        BuffComponent(Buff* buff, int64_t dateExpire);

        Buff* BuffInstance;
        int64_t DateExpire;
        int64_t DateNextWakeUp;
        int ChargesRemaining;
    };

    class BuffListComponent : public artemis::Component
    {
    public:
        std::vector<Buff*> Buffs;
    };

    class BuffListIterator
    {
    public:
        BuffListIterator(const BuffListComponent* buffList, size_t index) :
            mBuffList(buffList),
            mIndex(index)
        {
        }

        bool operator!= (const BuffListIterator& other) const
        {
            return mIndex != other.mIndex;
        }

        Buff* operator*() const
        {
            return mBuffList->Buffs[mIndex];
        }

        const BuffListIterator& operator++ ()
        {
            ++mIndex;
            return *this;
        }
    private:
        const BuffListComponent* mBuffList;
        size_t mIndex;
    };

    inline BuffListIterator begin(const BuffListComponent& buffList)
    {
        return BuffListIterator(&buffList, 0);
    }

    inline BuffListIterator end(const BuffListComponent& buffList)
    {
        return BuffListIterator(&buffList, buffList.Buffs.size());
    }

    class BuffSystem : public artemis::EntityProcessingSystem
    {
    public:
        BuffSystem(const EnergySystem* energySystem);
        void initialize() override;
        void processEntity(artemis::Entity& e) override;
        void removed(artemis::Entity& e) override;
    private:
        const EnergySystem* mEnergySystem;
        artemis::ComponentMapper<BuffComponent> mBuffMapper;
        artemis::ComponentMapper<BuffListComponent> mBuffListMapper;
    };

    class BuffListSystem : public artemis::EntitySystem
    {
    public:
        BuffListSystem();
        void initialize() override;
        void processEntities(artemis::ImmutableBag<artemis::Entity*> & bag) override;
        bool checkProcessing() override;
        void removed(artemis::Entity& e) override;
    private:
        artemis::ComponentMapper<BuffListComponent> mBuffListMapper;
    };

    bool EntityHasBuff(artemis::Entity& e, BuffId id);
    int AddBuffToEntity(artemis::World& world, artemis::Entity& carrier, artemis::Entity& creator, BuffId id, int level, int64_t currentDate);
    BuffId GetOrbDebuff(OrbEffectId orbId);
}
}
