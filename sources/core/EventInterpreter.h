#pragma once

#include "Singleton.h"
#include <deque>
#include "SDL_keyboard.h"
#include "SDL_stdinc.h"

namespace DotaRL { namespace Event
{
    enum Type
    {
        //GLOB_QUIT, // je ne sais pas trop le g�rer bien celui-l�, il deviens un cas particulier
        CANCEL,
        SKIP_TURN,
        GO_LEFT,
        GO_RIGHT,
        GO_UP,
        GO_DOWN,
        GO_UP_LEFT,
        GO_UP_RIGHT,
        GO_DOWN_LEFT,
        GO_DOWN_RIGHT,
        PICK_UP,
        REST,

        RANGED_ATTACK,

        USE_ITEM_1,
        USE_ITEM_2,
        USE_ITEM_3,
        USE_ITEM_4,
        USE_ITEM_5,
        USE_ITEM_6,

        DROP_ITEM_1,
        DROP_ITEM_2,
        DROP_ITEM_3,
        DROP_ITEM_4,
        DROP_ITEM_5,
        DROP_ITEM_6,
        
        USE_SKILL_1,
        USE_SKILL_2,
        USE_SKILL_3,
        USE_SKILL_4,

        LEARN_SKILL_1,
        LEARN_SKILL_2,
        LEARN_SKILL_3,
        LEARN_SKILL_4,
        Length
    };

    class Interpreter : public TSingleton<Interpreter>
    {
        friend class TSingleton<Interpreter>;
    public:
        void Process();
        bool EventTriggered(Event::Type parEventType);

        bool QuitAsked() const;
        bool HasEvent() const;
        Type PoolEvent();
        void FlushEvents();

    protected:
        Interpreter();
        ~Interpreter();
    private:
        bool mQuitAsked;
        std::deque<Type> mEventStack;

        void HandleKeydownEvent(SDL_Keysym& keysym);
        void HandleEveyKeyItem(int index, Uint16 mod);

        SDL_Keycode mKeyItem1;
        SDL_Keycode mKeyItem2;
        SDL_Keycode mKeyItem3;
        SDL_Keycode mKeyItem4;
        SDL_Keycode mKeyItem5;
        SDL_Keycode mKeyItem6;
    };
}
}
