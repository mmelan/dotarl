#pragma once

struct SDL_Surface;
struct Mix_Chunk;
SDL_Surface* SDL_LoadImage_VFS(const char* parPath);
Mix_Chunk* SDL_LoadWAV_VFS(const char* parPath);
