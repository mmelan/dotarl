#pragma once
#include "Singleton.h"
#include "SDL_ttf.h"
#include <string>
#include <vector>

struct SDL_Window;
struct SDL_Renderer;
struct SDL_Texture;
namespace DotaRL
{
    enum class LogSource
    {
        Gain,
        Death,
        Damage,
        Info
    };

    struct LogText
    {
        LogText(char* log, LogSource src) : Message(log) , Time(0), Source(src) {}
        std::string Message;
        int Time;
        LogSource Source;
    };

    class Console : public TSingleton<Console>
    {
        friend class TSingleton<Console>;
        friend void AddLog(char* log, LogSource source);
    public:
        void Process();
        void Toggle();

    protected:
        Console();
        ~Console();

    private:
        std::vector<LogText> mLogList;
        bool mFull;
	    TTF_Font* mPolice;
    };

    void AddLog(char* log, LogSource source);
}
