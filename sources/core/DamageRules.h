#pragma once

#include <cstdint>

#include "Artemis/Entity.h"

namespace DotaRL { namespace GameObject {
    enum class DamageType
    {
        Physical,
        Cleave,
        Magic,
        Pure,
        Composite,
        Universal
    };

    bool ArmorAffected[];
    bool PhysicalImmuneAffected[];
    bool MagicResistanceAffected[];
    bool MagicImmuneAffected[];

    bool IsMagicImmune(artemis::Entity& target);
    bool IsPhysicalImmune(artemis::Entity& target);
    float GetMagicVulnerability(artemis::Entity& target);
    float GetArmorVulnerability(int armor);
    float RollDamageBlock(artemis::Entity& target);

    void ApplyNormalAttack(artemis::World& world,  artemis::Entity& attacker, artemis::Entity& target, int64_t dateInitiated);
    void InflictDamage(artemis::World& world, artemis::Entity& attacker, artemis::Entity& target, float damage, DamageType type);

    void UnitTestDamageRules();
}
}
