set _SDL2DIR=%CD%\lib\SDL2-2.0.0
set SDL2DIR=%_SDL2DIR:\=/%

set _SDLIMAGEDIR=%CD%\lib\SDL2_image-2.0.0
set SDLIMAGEDIR=%_SDLIMAGEDIR:\=/%

set _SDLTTFDIR=%CD%\lib\SDL2_ttf-2.0.12
set SDLTTFDIR=%_SDLTTFDIR:\=/%

set _SDLMIXERDIR=%CD%\lib\SDL2_mixer-2.0.0
set SDLMIXERDIR=%_SDLMIXERDIR:\=/%

if not exist bin ( mkdir bin )
set CUSTOM_BINDIR=%CD%\bin\

if not exist Visual12 ( mkdir Visual12 )
cd Visual12
CMake -DSDL2_BASE_DIR=%SDL2DIR% -DSDLIMAGE_BASE_DIR=%SDLIMAGEDIR% -DSDLTTF_BASE_DIR=%SDLTTFDIR% -DSDLMIXER_BASE_DIR=%SDLMIXERDIR% -G "Visual Studio 11" ..
PAUSE

